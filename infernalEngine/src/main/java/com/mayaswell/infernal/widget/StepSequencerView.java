package com.mayaswell.infernal.widget;

import java.util.ArrayList;

import com.mayaswell.audio.Control;
import com.mayaswell.infernal.CControl;
import com.mayaswell.infernal.Pattern.Stepper;
import com.mayaswell.infernal.R;
import com.mayaswell.infernal.Pattern.Line;
import com.mayaswell.util.AbstractPatch.Action;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.AdapterView.OnItemSelectedListener;

public class StepSequencerView extends RelativeLayout {

	private StepperValuesView stepperValuesView;
	private Spinner controlSelector;
	protected ArrayAdapter<Action> controlSelectAdapter = null;
	private Line currentLine = null;
	private PopupMenu controlSelectSubMenu = null;

	public StepSequencerView(Context context) {
		super(context);
		init(context, null, -1);
	}

	public StepSequencerView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context, attrs, -1);
	}

	public StepSequencerView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		init(context, attrs, defStyleAttr);
	}

	private void init(Context context, AttributeSet attrs, int defStyleAttr) {
		LayoutInflater li = (LayoutInflater)context.getSystemService (Context.LAYOUT_INFLATER_SERVICE);
		li.inflate (R.layout.step_sequencer_view, this, true);
		stepperValuesView  = (StepperValuesView) findViewById(R.id.stepperValuesView);
		controlSelector = (Spinner) findViewById(R.id.controlSelector);
		controlSelectAdapter = new ArrayAdapter<Action>(getContext(), R.layout.mw_spinner_item);
		controlSelectAdapter.setDropDownViewResource(R.layout.mw_spinner_dropdown_item);
		if (controlSelector != null) {
			controlSelector.setAdapter(controlSelectAdapter);
			controlSelector.setOnLongClickListener(new OnLongClickListener() {
				@Override
				public boolean onLongClick(View v) {
					return true;
				}
			});
			controlSelector.setOnItemSelectedListener(new OnItemSelectedListener()  {	
				@Override
				public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
					Action b = controlSelectAdapter.getItem(position);
					if (currentLine != null) {
						int cbi = b.getOp(); // previously included things that are now menu ops
						if (cbi == Action.NOP) {
						} else if (cbi == Action.NEW) {
							controlSelectSubMenu.show();
						} else if (cbi == Action.DELETE) {
							int i = currentLine.deleteStepper(stepperValuesView.getStepper());
							Stepper s = currentLine.getStepper(i);
							stepperValuesView.setStepper(s);
							if (s != null) {
								setControlSelector(i);
							} else {
								controlSelector.setSelection(0);
							}
						} else {
							Stepper s = currentLine.getStepper4ctlid(cbi);
							if (s != null) {
								stepperValuesView.setStepper(s);
							}
						}
					}	
				}
	
				@Override
				public void onNothingSelected(AdapterView<?> parentView) {
				}
			});
			controlSelectSubMenu = new PopupMenu(getContext(), this);
			controlSelectSubMenu.setOnMenuItemClickListener(new OnMenuItemClickListener() {
				@Override
				public boolean onMenuItemClick(MenuItem it) {
					int ctlid = it.getItemId();
					if (currentLine != null) {
						Stepper s = currentLine.findStepper(ctlid);
						if (s == null) {
							s = currentLine.createStepper(ctlid);
						}
						stepperValuesView.setStepper(s);
						setStepperAdapterItems();
						setControlSelector(ctlid);
					}
					return true;
				}
			});
		}
	}
	
	public void setControlSubmenu(ArrayList<CControl> stepperCControls) {
		if (controlSelectSubMenu != null) {
			controlSelectSubMenu.getMenu().clear();
			for (int i=0; i<stepperCControls.size(); i++) {
				Control c = stepperCControls.get(i);
				controlSelectSubMenu.getMenu().add(Menu.NONE, c.typeCode, Menu.NONE, c.toString());
			}
		}
	}

	public void setTo(LineView lv) {
		if (lv == null) {
			currentLine = null;
			return;
		}
		currentLine = lv.getLine();
		stepperValuesView.setLine(lv);
		stepperValuesView.invalidate();
		setStepperAdapterItems();
	}
	
	/**
	 *   this just updates the display in the spinner
	 * @param cpInd
	 */
	protected void setControlSelector(int cpInd)
	{
		if (cpInd < 0 || cpInd >= currentLine.countSteppers()) {
			cpInd = 0;
		}
		for (int i=0; i<controlSelectAdapter.getCount(); i++) {
			Action p = controlSelectAdapter.getItem(i);
			if (p.getOp() == cpInd) {
				controlSelector.setSelection(i);
				break;
			}
		}
	}
	
	protected boolean setStepperAdapterItems()
	{
		if (controlSelectAdapter == null) return false;
		controlSelectAdapter.clear();
		controlSelectAdapter.add(new Action(Action.NOP, ""));
		controlSelectAdapter.add(new Action(Action.NEW, "*New*"));
		controlSelectAdapter.add(new Action(Action.DELETE, "*Delete*"));
		if (currentLine == null) return false;
		/*
		a.add(new Action(Action.CLONE, "*Dup Patch*"));
		a.add(new Action(Action.RENAME, "*Rename Patch*"));
		a.add(new Action(Action.RELOAD, "*Reload Patch*"));
		a.add(new Action(Action.SAVE, "*Save Patch*"));
		*/
		for (int i=0; i<currentLine.countSteppers(); i++) {
			Stepper p = currentLine.getStepper(i);
			controlSelectAdapter.add(new Action(p.control.typeCode, p.control.toString()));
		}
		return true;
	}

	public void scrollValView(int l, int t) {
		if (stepperValuesView != null) {
			stepperValuesView.scrollTo(l, t);
		}
	}
	

}

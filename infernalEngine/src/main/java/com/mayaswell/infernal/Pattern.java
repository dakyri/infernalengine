package com.mayaswell.infernal;

import java.util.ArrayList;

import com.mayaswell.audio.Control;
import com.mayaswell.infernal.Pattern.Stepper;
import com.mayaswell.util.NamedPatch;

public class Pattern extends NamedPatch {
	/**
	 * @class Stepper
	 *
	 */
	public class Stepper {
		protected float [] val = null;
		public CControl control = null;
		
		public Stepper(CControl c, int l) {
			control = c;
			setLength(l);
		}
		
		public void setLength(int size) {
			float [] ov = val;
			val = new float[size];
			int n = (ov == null? 0: (val.length > ov.length? ov.length: val.length));
			for (int i=0; i<n; i++) {
				val[i] = ov[i];
			}
			for (int i=n; i<val.length; i++) {
				val[i] = (control != null? control.defaultValue(): 0);
			}
		}
		
		public Stepper clone() {
			Stepper s = new Stepper(control, val.length);
			int i=0;
			for (float v: val) {
				s.val[i++] = v;
			}
			return s;
		}
		
		public float getNormalizedVal(int i) {
			if (val == null || i < 0 || i >= val.length || control == null) {
				return 0;
			}
			return control.normalize(val[i]);
		}

		public void setNormalizedVal(short i, float pny) {
			if (val == null || i < 0 || i >= val.length || control == null) {
				return;
			}
			val[i] = control.denormalize(pny);
		}

	}
	
	/**
	 * @class Line
	 *
	 */
	public abstract class Line {
		public static final int TIED = 0;
		public static final int OFF = -1;
		public static final int NORMAL_HIT = 80;

		protected int [] vel = null;
		protected int syncH;
		protected int syncL;
		protected int stepsPerBeat;
		protected ArrayList<Stepper> stepper = null;
		
		public Line(int sb, int l) {
			syncH = syncL = 0;
			stepsPerBeat = sb;
			setLength(l);
			stepper = new ArrayList<Stepper>();
		}
		
		public Line() {
			this(4, 16);
		}
		
		public int getLength() {
			return vel != null? vel.length : 0;
		}

		public void setLength(int size) {
			int [] ov = vel;
			vel = new int[size];
			int n = (ov == null? 0: (vel.length > ov.length? ov.length: vel.length));
			for (int i=0; i<n; i++) {
				vel[i] = ov[i];
			}
			for (int i=n; i<vel.length; i++) {
				vel[i] = OFF;
			}
		}

		public int framesPerStep(double framesPerBeat) {
			if (syncH > 0 && syncL > 0 && syncH != syncL) {
				framesPerBeat = (framesPerBeat * syncH) / syncL;
			}
			return (int) Math.ceil(framesPerBeat / stepsPerBeat);
		}

		public int nBeats() {
			return vel == null? 0 : (vel.length/stepsPerBeat);
		}

		public void setStep(int stepid, boolean hit) {
			if (stepid >= 0 && stepid < vel.length) {
				if (hit) {
					vel[stepid] = 80;
				} else {
					vel[stepid] = OFF;
				}
			}
		}

		public void setStep(int stepid, int v) {
			if (stepid >= 0 && stepid < vel.length) {
				vel[stepid] = v;
			}
		}

		public abstract Line clone();

		public int countSteppers() {
			return stepper != null? stepper.size(): 0;
		}
		
		public Stepper getStepper(int i) {
			return i >= 0 && i < stepper.size()? stepper.get(i): null;
		}
		
		public Stepper getStepper(Control c) {
			return getStepper4ctlid(c.typeCode);
		}

		public Stepper getStepper4ctlid(int cbi) {
			for (Stepper s: stepper) {
				if (s.control.typeCode == cbi) {
					return s;
				}
			}
			return null;
		}

		public int deleteStepper(Stepper s) {
			for (int i = 0; i<stepper.size(); i++) {
				Stepper si = stepper.get(i);
				if (si.control.typeCode == s.control.typeCode) {
					stepper.remove(i);
					if (i < stepper.size()) {
						return i;
					}
				}
			}
			return 0;
		}

		public Stepper findStepper(int itemId) {
			for (int i = 0; i<stepper.size(); i++) {
				Stepper si = stepper.get(i);
				if (si.control.typeCode == itemId) {
					return si;
				}
			}
			return null;
		}

		public Stepper createStepper(int itemId) {
			Stepper si = new Stepper(new CControl(itemId), vel.length);
			stepper.add(si);
			return si;
		}
		
		public boolean isRunningDecoupled() {
			return syncH > 0 && syncL > 0 && syncH != syncL;
		}

		public int getVel(int step) {
			return vel != null && step >= 0 && step < vel.length? vel[step]: Line.OFF;
		}
	}
	
	/**
	 * @class SynthLine
	 *
	 */
	public class SynthLine extends Line {
		public SynthLine() {
			this(4, 16);
		}
		public SynthLine(int sb, int n) {
			super(sb, n);
			setLength(n);
		}
		
		protected int [] pitch = null;
		
		public void setLength(int size) {
			super.setLength(size);
			int [] ov = pitch;
			pitch = new int[size];
			int n = (ov == null? 0: (pitch.length > ov.length? ov.length: pitch.length));
			for (int i=0; i<n; i++) {
				pitch[i] = ov[i];
			}
			for (int i=n; i<pitch.length; i++) {
				pitch[i] = 36;
			}
		}
		@Override
		public Line clone() {
			SynthLine l = new SynthLine(stepsPerBeat, vel.length);
			l.syncH = syncH;
			l.syncL = syncL;
			for (int i=0; i<vel.length; i++) {
				l.vel[i] = vel[i];
				l.pitch[i] = pitch[i];
			}
			for (Stepper s: stepper) {
				l.stepper.add(s.clone());
			}
			return l;
		}
		public int getNote(int step) {
			return pitch != null && step >= 0 && step < pitch.length? pitch[step]: -1;
		}
		
		public int getNextVel(int step) {
			if (vel == null || step < 0) return -1;
			if (step >= pitch.length-1) step = 0; else step++;
			return vel[step];
		}
	}
		
	/**
	 * @class DrumLine
	 *
	 */
	public class DrumLine extends Line {
		public DrumLine() {
			this(4, 16);
		}
		public DrumLine(int sb, int n) {
			super(sb, n);
			setLength(n);
		}
		protected int [] stutter = null;
		
		public void setLength(int size) {
			super.setLength(size);
			int [] ov = stutter;
			stutter = new int[size];
			int n = (ov == null? 0: (stutter.length > ov.length? ov.length: stutter.length));
			for (int i=0; i<n; i++) {
				stutter[i] = ov[i];
			}
			for (int i=n; i<stutter.length; i++) {
				stutter[i] = 0;
			}
		}
		@Override
		public Line clone() {
			DrumLine l = new DrumLine(stepsPerBeat, vel.length);
			l.syncH = syncH;
			l.syncL = syncL;
			for (int i=0; i<vel.length; i++) {
				l.vel[i] = vel[i];
				l.stutter[i] = stutter[i];
			}
			for (Stepper s: stepper) {
				l.stepper.add(s.clone());
			}
			return l;
		}
		
	}
	
	public int countDrumLine() {
		int i=0;
		for (Line l: row) {
			if (l instanceof DrumLine) {
				i++;
			}
		}
		return i;
	}
	
	public int countSynthLine() {
		int i=0;
		for (Line l: row) {
			if (l instanceof SynthLine) {
				i++;
			}
		}
		return i;
	}
	
	public DrumLine getDrumLine(int i) {
		for (Line l: row) {
			if (l instanceof DrumLine) {
				if (i == 0) {
					return (DrumLine) l;
				} 
				if (--i < 0) {
					break;
				}
				
			}
		}
		return null;
	}
		
	public SynthLine getSynthLine(int i) {
		for (Line l: row) {
			if (l instanceof SynthLine) {
				if (i == 0) {
					return (SynthLine) l;
				}
				if (--i < 0) {
					break;
				}
			}
		}
		return null;
	}
		
	protected ArrayList<Line> row;
	
	public int currentStep;
	public int currentCycleLoop;
	public int currentLoop;
	
	public int stepsPerBeat;
	public int nBeats;

	public Pattern() {
		this("", 4, 4);
	}
	
	public Pattern(String n, int nb, int sb) {
		row = new ArrayList<Line>();
		name = n;
		nBeats = nb;
		stepsPerBeat = sb;
		currentLoop = 0;
		currentCycleLoop = 0;
		currentStep = 0;
	}

	public int dfltNSteps() {
		return nBeats*stepsPerBeat;
	}

//	public ArrayList<Line> getRows() {
//		return row;
//	}

	public DrumLine addDrumLine() {
		return addDrumLine(stepsPerBeat, nBeats*stepsPerBeat);
	}

	public synchronized DrumLine addDrumLine(int sb, int n) {
		DrumLine l = new DrumLine(sb, n);
		row.add(l);
		return l;
	}
	
	public SynthLine addSynthLine() {
		return addSynthLine(stepsPerBeat, nBeats*stepsPerBeat);
	}

	public synchronized SynthLine addSynthLine(int sb, int n) {
		SynthLine l = new SynthLine(sb, n);
		row.add(l);
		return l;
	}
	
	public Pattern clone()
	{
		Pattern p = new Pattern(new String(name), nBeats, stepsPerBeat);
		for (Line l: row) {
			p.row.add(l.clone());
		}
		return p;
	}

}

package com.mayaswell.infernal;

import android.util.Log;

import com.mayaswell.audio.Bufferator;
import com.mayaswell.audio.Bufferator.SampleChunkInfo;
import com.mayaswell.audio.Bufferator.SampleInfo;
import com.mayaswell.audio.Sample.Direction;
import com.mayaswell.infernal.Pattern.Line;
import com.mayaswell.infernal.Pattern.Stepper;
import com.mayaswell.spacegun.MWBPPadActivity;
import com.mayaswell.spacegun.PadSample;

public class IPadSample extends PadSample {

	private static final int minStutterFrames = 10;
	private int nStutterFrames;
	private int nextStutterFrame;

	public IPadSample(MWBPPadActivity<?,?,?> a, String name, int id) {
		super(a, name, id);
	}

	/**
	 * variation on super class
	 * @param busMixPointer
	 * @param nFrames
	 * @param buffInd 
	 * @param nOutChannels
	 */
	public int playCumulative(long busMixPointer, int buffInd, int nFrames, short nOutChannels)
	{
		SampleInfo csf = currentSampleInfo;
		if (csf == null || csf.sampleChunk == null) {
//			Log.d("player", "bail with null chunk");
			return nFrames;
		}
		if (!isPlaying()) {
//			Log.d("player", "tried to play, but not really playing");
			return nFrames;
		}
		
		int nFramesOut = 0;
		int ll = ((int)state.loopLength);
		if (ll < 2) ll = 2;
		int sf = ((int)state.loopStart);
//		Log.d("player", String.format("pad %d to play %d %d", id, nFrames, nOutChannels));
		while (nFramesOut < nFrames) {
			int nFramesRemaining = nFrames-nFramesOut;
			int nIterFrames = (currentDirection >= 0)?
						( (nFramesRemaining > ll-currentLoopFrame)?(ll - currentLoopFrame): nFramesRemaining):
						( (nFramesRemaining > currentLoopFrame)?currentLoopFrame: nFramesRemaining);
//			Log.d("player", String.format("pad %d loop frame %d dir %d to make %d %d %d %d tune is %g", id, currentLoopFrame, currentDirection, nFrames, nFramesOut, nFramesRemaining, nIterFrames, state.tune));
			if (currentStutter > 1) {
				if (nIterFrames > nextStutterFrame-currentLoopFrame) {
					nIterFrames = nextStutterFrame-currentLoopFrame;
				}
			}
			if (nIterFrames > 0) {
				int currentDataFrame = currentLoopFrame + sf;
				int scid = 0;
				int scistart = 0;
				int scilength = 0;
				float [] scidata = null;
				SampleChunkInfo sci = csf.getChunkFor(currentDataFrame);
				float nextL = 0;
				float nextR = 0;
				if (sci != null) {
					scid = sci.id;
					scilength = sci.nFrames;
					scidata = sci.data;
					scistart = sci.startFrame;
					if (state.tune != 0) { // setup buffer endpoints if we're interpolating TODO or we are tuning it with lfo!!!!
						if (currentDirection >= 0) {
							SampleChunkInfo scnxt = null;
							if (sci.nextChunkStartFrame() >= sf + ll) {
								scnxt = csf.getChunkFor(sf);
							} else {
								scnxt = csf.getChunk(scid+1);
							}
							if (scnxt != null) {
								nextL = scnxt.data[0];
								if (nChannels > 1) {
									nextR = scnxt.data[1];
								}
							}
						} else { // backwards .
							SampleChunkInfo scnxt = null;
							if (sci.prevChunkEndFrame() < sf) {
								scnxt = csf.getChunkFor(sf);
							} else {
								scnxt = csf.getChunk(scid-1);
							}
							if (scnxt != null) {
								if (nChannels > 1) {
									nextL = scnxt.data[scnxt.nFrames-2];
									nextR = scnxt.data[scnxt.nFrames-1];
								} else {
									nextL = scnxt.data[scnxt.nFrames-1];
								}
							}
						}
					}
//					Log.d("player", String.format("pad %d got %d chunk %d len %d @ %d data[0] %g path %d %s", id, currentDataFrame, scid, scilength, scistart, scidata[0], csf.refCount, csf.path));
				} else {
					scid = Bufferator.chunk4Frame(currentDataFrame);
//					Log.d("player", String.format("pad %d failed to find %d chunk %d path %s", id, currentDataFrame, scid, csf.path));
				}
				int nIterOutFrames = playChunkMixer(cPadPointer, busMixPointer, buffInd, nIterFrames, nOutChannels, currentDataFrame,
						scidata, scistart, scilength, nextL, nextR, currentDirection);
				if (scid >= 0) {
					csf.requestChunk(scid, 1); // will already be loaded, but we'll make sure it's there
					if (currentDirection >= 0) {
						csf.requestChunk(scid+1, 1);
						csf.requestChunk(scid+2, 1);
					} else {
						csf.requestChunk(scid-1, 1);
						csf.requestChunk(scid-2, 1);
					}
				}
				buffInd += nOutChannels*nIterOutFrames;
				currentLoopFrame = getNextDataFrame(cPadPointer)-sf;
	 			nFramesOut += nIterOutFrames;
//				Log.d("player", String.format("pad %d played %d %d requested %d loop %d data %d end %d", id, nIterOutFrames, nFramesOut, nIterFrames, currentLoopFrame, getNextDataFrame(cPadPointer), ll));
			} else {
//				if (!isPlaying()) {
//					break;
//				}
			}
			if (currentLoopFrame >= ll) {
				if (currentDirection >= 0) {
					loopEnd();
				} else {
					currentLoopFrame = ll;
				}
			} else if (currentLoopFrame <= 0){
				if (currentDirection >= 0) {
					currentLoopFrame = 0;
				} else {
					loopEnd();
				}
			} else if (currentStutter > 1) {
				if (currentLoopFrame >= nextStutterFrame) {
					doStutter();
				}
			}
			if (!isPlaying()) {
				break;
			}
		}
		
		return nFrames;
	}

	protected void loopEnd() {
		Log.d("player", "end of the loop "+currentLoopFrame + " ... "+ state.loopDirection);
		if (state.loopCount > 0 && ++currentLoopCount >= state.loopCount) {
			stop(true);
//			return;
		}

		if (state.loopDirection == Direction.FWDBACK || state.loopDirection == Direction.BACKFWD) {
			currentDirection = -currentDirection;
		}
		if (currentDirection >= 0) {
			int lsb = Bufferator.chunk4Frame((int) state.loopStart);
			currentSampleInfo.requestChunk(lsb, 1);
			currentSampleInfo.requestChunk(lsb+1, 1);
			currentLoopFrame = 0;
		} else {
			int ll1 = (int) (state.loopLength-1);
			int lsb = Bufferator.chunk4Frame((int) (state.loopStart+ll1));
			currentSampleInfo.requestChunk(lsb, 1);
			currentSampleInfo.requestChunk(lsb-1, 1);
			currentLoopFrame = ll1;
		}
		reloopCPad(cPadPointer);
	}
	
	public boolean isActive() {
		return currentSampleInfo != null && currentSampleInfo.sampleChunk != null;
	}
	
	private Pattern.DrumLine patternLine = null;
	public int stepFrames = 0;
	public int currentStep = 0;
	private float currentVelocity = 1;
	private int currentStutter = 0;

	public void setPatternLine(Pattern.DrumLine p) {
		patternLine = p;
	}
	
	public Line getPatternLine() {
		return patternLine;
	}

	public boolean atStepEnd(double framesPerBeat) {
		if (patternLine != null) {
			return stepFrames >= patternLine.framesPerStep(framesPerBeat);
		}
		return false;
	}

	public int getNextStep() {
		int step = currentStep+1;
		if (patternLine != null && patternLine.vel != null && step >= patternLine.vel.length) {
			step = 0;
		}
		return step;
	}

	public void setStep(int step, int framesPerStep) {
//		Log.d("ipads", String.format("%d step", step));
		currentStep = step;
		stepFrames = 0;
		if (patternLine != null) {
			int v = patternLine.getVel(step);
			if (v > 0) {
				setVelocity(v);
				setStutter(patternLine.stutter[step]);
				if (currentStutter > 1) {
					nStutterFrames = (int) (framesPerStep / currentStutter);
					if (nStutterFrames < minStutterFrames) {
						nStutterFrames = minStutterFrames;
					}
					nextStutterFrame = currentLoopFrame + nStutterFrames;
				} else {
					nextStutterFrame = 0; // make sure not to check!!
				}
				Log.d("ipads", String.format("%d step fire", step));
				fire();
			}
			for (Stepper s: patternLine.stepper) {
				if (step >= 0 && step < s.val.length) {
					float sv = s.val[step];
					setValue(s.control, sv);
				}
			}
		}
	}
	
	public void setStutter(int i) {
		currentStutter  = i;
	}

	public void setVelocity(float vel) {
		currentVelocity  = vel;
		setCPadGain(cPadPointer, state.gain*currentVelocity);
	}

	public void setGain(float v)
	{
		state.gain = v;
		setCPadGain(cPadPointer, state.gain*currentVelocity);
	}
	
	private void doStutter() {
		switch(state.loopDirection) {
		case BACKWARD:
			currentLoopFrame = (int) (state.loopStart+state.loopLength-1);
			break;
		case FORWARD:
			currentLoopFrame = (int) state.loopStart;
			break;
		case FWDBACK:
			currentDirection = -currentDirection;
			break;
		case BACKFWD:
			currentDirection = -currentDirection;
			break;
		}
		nextStutterFrame = currentLoopFrame + nStutterFrames;
	}

	public int getLoopCount() {
		return currentLoopCount;
	}
	public void setLoopCount(int lc) {
		currentLoopCount = 0;
	}

}

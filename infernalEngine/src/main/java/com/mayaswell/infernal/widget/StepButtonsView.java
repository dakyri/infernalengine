package com.mayaswell.infernal.widget;

import com.mayaswell.R;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Paint.Style;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.View.MeasureSpec;
import android.widget.RelativeLayout;

public class StepButtonsView extends RelativeLayout {
	final float densityMultiplier = getContext().getResources().getDisplayMetrics().density;
	final float textScaledPx = 10 * densityMultiplier;
	private Paint textBrush = null;
	private Paint borderBrush;
	protected Rect bRect = null;

	public StepButtonsView(Context context) {
		super(context);
		setup(context, null, -1);
	}

	public StepButtonsView(Context context, AttributeSet attrs) {
		super(context, attrs);
		setup(context, attrs, -1);
	}

	public StepButtonsView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		setup(context, attrs, defStyleAttr);
	}

	private void setup(Context context, AttributeSet attrs, int defStyleAttr) {
		bRect = new Rect();
		
		borderBrush = new Paint(Paint.ANTI_ALIAS_FLAG);
		borderBrush.setColor(Color.WHITE);
		borderBrush.setStyle(Style.STROKE);
		borderBrush.setStrokeWidth(1);
		
		setPadding(1, 1, 1, 1);
	}
	
	@Override
	protected void onSizeChanged (int w, int h, int oldw, int oldh)
	{
		super.onSizeChanged(w, h, oldw, oldh);
	}
	
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		/*
		Log.d("touch", String.format("onMeaure %s %s", View.MeasureSpec.toString(widthMeasureSpec), View.MeasureSpec.toString(heightMeasureSpec)));

		int wm = MeasureSpec.getMode(widthMeasureSpec);
		int w = MeasureSpec.getSize(widthMeasureSpec);
		int hm = MeasureSpec.getMode(heightMeasureSpec);
		int h = MeasureSpec.getSize(heightMeasureSpec);

	
		if (w < getChildCount()*200) {
			w = getChildCount()*200;
		}
		setMeasuredDimension(w, h);
		*/
	}
	
	/**
	 * @param canvas
	 */
	@Override
	protected void dispatchDraw(Canvas canvas) {
		bRect.set(0, 0, getWidth(), getHeight());
		canvas.drawRect(bRect, borderBrush);
		super.dispatchDraw(canvas);
//			getLocalVisibleRect(vRect);
//			vRect.set(0, 0, getWidth(), getHeight());
//			canvas.drawText(Integer.toString(id),(vRect.right+vRect.left)/2,vRect.bottom-textScaledPx, textBrush);
//			if (lamp != null) {
//				lamp.setBounds((vRect.right+vRect.left)/2-25, vRect.top+20, (vRect.right+vRect.left)/2+25, vRect.top+35);
//				lamp.draw(canvas);
//			}
	/*		if (padSample != null) {
	//			Log.d("draw button", String.format("playing %b", padSample.isPlaying()));
				if (padSample.isPlaying()) {
					float lp = pdRect.left;
					float rp = pdRect.left+padSample.getCurrentPosition()*(pdRect.right-pdRect.left);
					float mp = (pdRect.bottom+pdRect.top)/2;
	//				canvas.drawLine(lp, pdRect.top, lp, pdRect.bottom, positionBrush);
	//				canvas.drawLine(rp, pdRect.top, rp, pdRect.bottom, positionBrush);
					canvas.drawLine(lp, mp, rp, mp, positionBrush);
				}
				PadSampleState pss = padSample.getState();
				if (pss.name != null) {
				}
			}
	*/
	}
}

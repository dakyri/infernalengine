package com.mayaswell.infernal;

import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.util.Log;

import com.mayaswell.audio.AudioMixer;
import com.mayaswell.spacegun.BusMixer;
import com.mayaswell.spacegun.PadSample;

public class IEAudioMixer extends AudioMixer {

	private InfernalPlayer infernal = null;

	public IEAudioMixer() {
		super();
	}
	
	public void addInfernal(InfernalPlayer w)
	{
		infernal  = w;
	}
	
	public boolean play()
	{
		Thread t; 
		
		if (isPlaying) {
			return false;
		}
		
		t = new Thread() {
			public void run() {
				isPlaying = true;
				
				setPriority(Thread.MAX_PRIORITY);

				AudioTrack audioTrack = new AudioTrack(AudioManager.STREAM_MUSIC, dfltSampleRate, 
							outChannelFormat, 
							AudioFormat.ENCODING_PCM_16BIT, 
						 	outBufSize, 
						 	AudioTrack.MODE_STREAM);
			
				int nFramePerBuf = outBufSize/nOutChannels;
//				float busBuffer[] = new float[outBufSize];
				short outBuffer[] = new short[outBufSize];

				audioTrack.play();

				while(isPlaying){
					BusMixer.zeroBuffers(nFramePerBuf);
					int nActiveBus = 0;
//					Log.d("player", String.format("nframe per buf %d", nFramePerBuf));
					if (infernal != null && infernal.isPlaying()) {
						nActiveBus = infernal.playCumulative(BusMixer.getNativePointer(), nFramePerBuf, (short) 2);
					}
					BusMixer.mix(nActiveBus, outBuffer, nFramePerBuf, (short)nOutChannels);
					/*
					if (exportState > 0 && exportState < 3) {
						xStashCount+=(exportEndOffset-exportStartOffset)/nOutChannels;
						Log.d("render", "stash "+nFramePerBuf +","+Integer.toString((exportEndOffset-exportStartOffset))+"," + xStashCount+","+exportState);
						short[] xBuffer = new short[exportEndOffset-exportStartOffset];
						for (int i=exportStartOffset; i<xBuffer.length; i++) {
							xBuffer[i] = outBuffer[i];
						
						}
						synchronized (exportDataQueue) {
							exportDataQueue.add(xBuffer);		
							exportStartOffset = 0;
							exportEndOffset = outBufSize;
							if (exportState == 2) {
								exportState = 3;
							}
						}
					}
					*/
					audioTrack.write(outBuffer, 0, outBufSize);
				}

				audioTrack.stop();
				audioTrack.release();
			}
		};
		
		t.start();
		return true;
	}
	
	public void stop()
	{
		isPlaying = false;
	}

	public void startInfernal(InfernalPlayer njin) {
		njin.fire();
	}

	public void stopInfernal(InfernalPlayer infernal) {
		infernal.stop(false);
	}

}

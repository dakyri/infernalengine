package com.mayaswell.infernal.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.mayaswell.fragment.MWFragment;
import com.mayaswell.infernal.InfernalEngine;
import com.mayaswell.infernal.Patch;
import com.mayaswell.infernal.Pattern;
import com.mayaswell.infernal.Pattern.Line;
import com.mayaswell.infernal.R;
import com.mayaswell.infernal.Pattern.DrumLine;
import com.mayaswell.infernal.Pattern.SynthLine;
import com.mayaswell.infernal.widget.DrumLineView;
import com.mayaswell.infernal.widget.LineView;
import com.mayaswell.infernal.widget.PatternLinesView;
import com.mayaswell.infernal.widget.StepSequencerView;
import com.mayaswell.infernal.widget.SynthLineView;
import com.mayaswell.infernal.widget.SynthStepButton;
import com.mayaswell.spacegun.PadSampleState;

public class PatternFragment extends MWFragment {

	private PatternLinesView patternLinesView = null;
	private StepSequencerView stepSequencerView = null;
	private LineView selectedLineView = null;


	public PatternFragment() {
		Log.d("patternfragment", "PatternFragment()");
	}

	@Override
	public void onAttach(Activity activity)
	{
		super.onAttach(activity);
		Log.d("patternfragment", "onAttach");
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		Log.d("patternfragment", "onCreate");
	}
	

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState)
	{
		super.onViewCreated(view, savedInstanceState);
		Log.d("patternfragment", "onViewCreated");
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		Log.d("patternfragment", "onCreateView");
		View v = inflater.inflate(R.layout.pattern_fragment_view, container, true);
		patternLinesView  = (PatternLinesView) v.findViewById(R.id.patternLinesView);
		patternLinesView.setMaxLinesViews(4);
		patternLinesView.setListener(new PatternLinesView.Listener() {
			
			@Override
			public void onSelect(LineView v) {
				InfernalEngine ie = (InfernalEngine) getActivity();
				ie.selectLineView(v);
				stepSequencerView.setTo(v);	
				selectedLineView = v;
			}
			
			@Override
			public void onPadHit(DrumLineView v, DrumLine dl, int drid, int stepid, boolean hit) {
				dl.setStep(stepid, hit);
			}

			@Override
			public void onPadHit(SynthLineView v, SynthLine dl, int syid, int stepid, int mode) {
				switch (mode) {
					case SynthStepButton.TIED:
						dl.setStep(stepid, Line.TIED);
						break;
					case SynthStepButton.ACTIVATED:
						dl.setStep(stepid, Line.NORMAL_HIT);
						break;
					case SynthStepButton.DEACTIVATED:
						dl.setStep(stepid, Line.OFF);
						break;
				}
			}

			@Override
			public void onScrollChanged(LineView v, int l, int t) {
				if (selectedLineView == v) {
					stepSequencerView.scrollValView(l, t);
				}
			}
		});
		stepSequencerView  = (StepSequencerView) v.findViewById(R.id.stepSequencerView);
		InfernalEngine ie = (InfernalEngine) getActivity();
		stepSequencerView.setControlSubmenu(ie.stepperCControls);
		return v;
	}
	
	@Override
	public void onDestroyView()
	{
		super.onDestroyView();
		Log.d("patternfragment", "onDestroyView");
	}

	public void setToPattern(Patch p, Pattern pat) {
		patternLinesView.setToPattern(p, pat);
	}

}

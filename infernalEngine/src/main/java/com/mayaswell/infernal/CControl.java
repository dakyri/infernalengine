package com.mayaswell.infernal;

public class CControl extends com.mayaswell.spacegun.CControl {

	public static final int WAVEFORM = 9;
	public static final int WAVESHAPE = 10;
	public static final int GLIDE = 11;
	public static final int FLTR_ENV_DEC_T = 12;
	public static final int FLTR_ENV_ATT_T = 13;
	public static final int FLTR_ENV_HOLD_T = 14;
	public static final int FLTR_ENV_ATTACK = 15;
	public static final int FLTR_ENV_DECAY = 16;

	public static final float MAX_FLTR_ATT_T = 4;
	public static final float MAX_FLTR_HOLD_T = 16;
	public static final float MAX_FLTR_DEC_T = 4;

	public CControl(int i)
	{
		super(i);
	}
	
	public CControl(String s)
	{
		super(string2code(s));
	}
	
	@Override
	public String fullName()
	{
		switch (typeCode) {
			case WAVEFORM: return "Waveform";
			case WAVESHAPE: return "Wave Shape";
			case GLIDE: return "Glide";
			case FLTR_ENV_DEC_T: return "Filter Dec T";
			case FLTR_ENV_ATT_T: return "Filter Att T";
			case FLTR_ENV_HOLD_T: return "Filter Hold T";
			case FLTR_ENV_ATTACK: return "Filter Attack";
			case FLTR_ENV_DECAY: return "Filter Decay";
			default:
				return super.fullName();
		}
	}
	
	@Override
	public String abbrevName()
	{
		switch (typeCode) {
			case WAVEFORM: return "Wave";
			case WAVESHAPE: return "Shape";
			case GLIDE: return "Glide";
			case FLTR_ENV_DEC_T: return "Flt Dec T";
			case FLTR_ENV_ATT_T: return "Flt Att T";
			case FLTR_ENV_HOLD_T: return "Flt Hold T";
			case FLTR_ENV_ATTACK: return "Flt Attack";
			case FLTR_ENV_DECAY: return "Flt Decay";
			default:
				return super.abbrevName();
		}
	}
	
	@Override
	public float normalize(float v)
	{
		switch (typeCode) {
			case FLTR_ENV_DEC_T:
			case FLTR_ENV_ATT_T:
			case FLTR_ENV_HOLD_T:

//			case WAVEFORM:
			case WAVESHAPE:
			case GLIDE:
			case FLTR_ENV_ATTACK:
			case FLTR_ENV_DECAY:
			default:
				return super.normalize(v);
		}
	}
	
	@Override
	public float denormalize(float v)
	{
		switch (typeCode) {
			case FLTR_ENV_DEC_T:
			case FLTR_ENV_ATT_T:
			case FLTR_ENV_HOLD_T:

//			case WAVEFORM:
			case WAVESHAPE:
			case GLIDE:
			case FLTR_ENV_ATTACK:
			case FLTR_ENV_DECAY:
			default:
				return super.denormalize(v);
		}
	}

	public static String code2string(int typeCode)
	{
		switch (typeCode) {
			case WAVEFORM: return "waveform";
			case WAVESHAPE: return "waveshape";
			case GLIDE: return "glide";
			case FLTR_ENV_DEC_T: return "fltEnvDecT";
			case FLTR_ENV_ATT_T: return "fltEnvAttT";
			case FLTR_ENV_HOLD_T: return "fltEnvHoldT";
			case FLTR_ENV_ATTACK: return "fltEnvAttAlph";
			case FLTR_ENV_DECAY: return "fltEnvDecAlph";
			default:
				return com.mayaswell.spacegun.CControl.code2string(typeCode);
		}
	}
	
	public static int string2code(String type)
	{
		if (type.equals("waveform")) {
			return WAVEFORM;
		} else if (type.equals("waveshape")) {
			return WAVESHAPE;
		} else if (type.equals("glide")) {
			return GLIDE;
		} else if (type.equals("fltEnvDecT")) {
			return FLTR_ENV_DEC_T;
		} else if (type.equals("fltEnvAttT")) {
			return FLTR_ENV_ATT_T;
		} else if (type.equals("fltEnvHoldT")) {
			return FLTR_ENV_HOLD_T;
		} else if (type.equals("fltEnvAttAlph")) {
			return FLTR_ENV_ATTACK;
		} else if (type.equals("fltEnvDecAlph")) {
			return FLTR_ENV_DECAY;
		}
		return com.mayaswell.spacegun.CControl.string2code(type);
	}
}

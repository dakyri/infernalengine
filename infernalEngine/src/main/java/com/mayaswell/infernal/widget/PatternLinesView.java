package com.mayaswell.infernal.widget;

import java.util.ArrayList;
import java.util.LinkedList;

import com.mayaswell.infernal.BasslineState;
import com.mayaswell.infernal.Patch;
import com.mayaswell.infernal.Pattern;
import com.mayaswell.infernal.R;
import com.mayaswell.infernal.Pattern.DrumLine;
import com.mayaswell.infernal.Pattern.SynthLine;
import com.mayaswell.infernal.widget.DrumLineView.Listener;
import com.mayaswell.infernal.widget.LineView.ViewMode;
import com.mayaswell.spacegun.PadSampleState;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.RelativeLayout;

public class PatternLinesView extends RelativeLayout {
	public interface Listener {
		void onSelect(LineView v);
		void onPadHit(DrumLineView v, DrumLine dl, int drid, int stepid, boolean hit);
		void onPadHit(SynthLineView v, SynthLine dl, int syid, int stepid, int mode);
		void onScrollChanged(LineView v, int l, int t);
	}

	private int newlvId = 0;
	ArrayList<DrumLineView> dlv = new ArrayList<DrumLineView>();
	ArrayList<SynthLineView> slv = new ArrayList<SynthLineView>();
	LinkedList<LineView> dq = new LinkedList<LineView>();
	
	private int lvLastId = -1;
	private int lvLastMinRowId = -1;
	private int lvLastMinColId = -1;
	private int currentMinLineRowWidth = 0;

	protected int maxLineViewShowing = -1;
	
	protected Pattern currentPattern = null;
	protected Patch currentPatch = null;

	public PatternLinesView(Context context) {
		super(context);
		init(context, null, -1);
	}

	public PatternLinesView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context, attrs, -1);
	}

	public PatternLinesView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		init(context, attrs, defStyleAttr);
	}

	private void init(Context context, AttributeSet attrs, int defStyleAttr) {
	}
	
	public synchronized void setToPattern(Patch p, Pattern pat) {
		Log.d("patternlinesview", "set to patch/pattern");
		removeAllViews();
		dq.clear();
		newlvId = 1;
		int i = 0;
		DrumLine pl;
		currentPattern = pat;
		currentPatch = p;
		int mdlv = maxLineViewShowing>0? (int)(0.6*maxLineViewShowing):-1;
		int mslv = maxLineViewShowing>0? maxLineViewShowing-mdlv:-1;
		Log.d("plv", String.format("md %d ms %d", mdlv, mslv));
		if (p != null && pat != null) {
			while ((pl=pat.getDrumLine(i)) != null) {
				PadSampleState pss = p!=null && i<p.padState.size()?p.padState.get(i):null;
				DrumLineView dlv = getDrumLineView(i, pss, pl);
				checkMaxLineViews(dlv, mdlv);
				addLineView(dlv);
				i++;
			}
			i = 0;
			SynthLine sl;
			while ((sl=pat.getSynthLine(i)) != null) {
				BasslineState pss = p!=null && i<p.synState.size()?p.synState.get(i):null;
				SynthLineView slv = getSynthLineView(i, pss, sl);
				checkMaxLineViews(slv, mdlv+mslv);
				addLineView(slv);
				i++;
			}
		}
		layoutChildren();
	}

	protected void checkMaxLineViews(LineView slv, int lim) {
		if (lim < 0) {
			return;
		}
		int i = dq.size();
		if (lim > 0 && i >= lim) {
			dq.remove(slv);
			slv.setViewMode(ViewMode.MINIMIZED, false);
		} else {
			dq.add(slv);
			slv.setViewMode(ViewMode.NORMAL, false);
		}
	}

	private void addLineView(LineView lv) {
		lv.setId(newlvId++);
		addView(lv);
	}

	protected RelativeLayout.LayoutParams makeLayoutParams(LineView lv) {
		int lvw = lv.getPreferredWidth();
		int measuredWidth = getMeasuredWidth();
		RelativeLayout.LayoutParams rlp = new RelativeLayout.LayoutParams(lvw, LayoutParams.WRAP_CONTENT);
		if (lv.viewMode == LineView.ViewMode.MINIMIZED) {
			Log.d("pattlines", lv.lineDestLabel.getText()+", "+currentMinLineRowWidth +","+ lvw +","+measuredWidth+","+getWidth());
			if (measuredWidth > 0 && currentMinLineRowWidth + lvw > measuredWidth) {
				lvLastMinRowId = lvLastMinColId;
				lvLastMinColId = -1;
				currentMinLineRowWidth = 0;
			} else {
				currentMinLineRowWidth += lvw;
			}
			if (lvLastMinColId <= 0) {
				rlp.addRule(RelativeLayout.ALIGN_PARENT_LEFT);				
			} else {
				rlp.addRule(RelativeLayout.RIGHT_OF, lvLastMinColId);				
			}
			if (lvLastMinRowId > 0) {
				rlp.addRule(RelativeLayout.BELOW, lvLastMinRowId);
			} else {
				rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP);
			}
			lvLastMinColId = lv.getId();
		} else {
			if (lvLastId > 0) {
				rlp.addRule(RelativeLayout.BELOW, lvLastId);
			} else {
				if (lvLastMinRowId > 0) {
					rlp.addRule(RelativeLayout.BELOW, lvLastMinRowId);
				} else {
					rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP);
				}
			}
			rlp.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
			rlp.bottomMargin = getContext().getResources().getDimensionPixelSize(R.dimen.lineRowMargin);
			lvLastId = lv.getId();			
		}
		return rlp;
	}

	private DrumLineView getDrumLineView(int i, PadSampleState pss, DrumLine dl) {
		DrumLineView v = null;
		for (DrumLineView pv: dlv) {
			if (pv.getParent() == null) {
				v = pv;
				break;
			}
		}
		if (v == null) {
			v = new DrumLineView(getContext());
			dlv.add(v);
		}
		v.setTo(i, pss, dl);
		v.setListener(new DrumLineView.Listener() {
			@Override
			public void onPadHit(DrumLineView v, DrumLine dl, int drid, int stepid, boolean hit) {
				if (listener != null) {
					listener.onPadHit(v, dl, drid, stepid, hit);
				}
			}

			@Override
			public void onScrollChanged(DrumLineView v, int l, int t) {
				if (listener != null) {
					listener.onScrollChanged(v, l, t);
				}
			}
		});
		return v;
	}

	private SynthLineView getSynthLineView(int i, BasslineState pss, SynthLine sl) {
		SynthLineView v = null;
		for (SynthLineView pv: slv) {
			if (pv.getParent() == null) {
				v = pv;
				break;
			}
		}
		if (v == null) {
			v = new SynthLineView(getContext());
			slv.add(v);
		}
		v.setTo(i, pss, sl);
		v.setListener(new SynthLineView.Listener() {
			@Override
			public void onPadHit(SynthLineView v, SynthLine dl, int syid, int stepid, int mode) {
				if (listener != null) {
					listener.onPadHit(v, dl, syid, stepid, mode);
				}
			}

			@Override
			public void onScrollChanged(SynthLineView v, int l, int t) {
				if (listener != null) {
					listener.onScrollChanged(v, l, t);
				}
			}
		});
		return v;
	}
	
	public boolean select(LineView v) {
		for (SynthLineView pv: slv) {
			if (pv == v) {
				pv.setSelected(true);
			} else {
				pv.setSelected(false);
			}
		}
		for (DrumLineView pv: dlv) {
			if (pv == v) {
				pv.setSelected(true);
			} else {
				pv.setSelected(false);
			}
		}
		if (v.viewMode == ViewMode.NORMAL) {
			dq.remove(v);
			dq.add(v);
		}
		if (listener != null) {
			listener.onSelect(v);
		}
		return true;
	}

	public synchronized void layoutChildren() {
		Log.d("patternlinesview", "doing layout");
		lvLastId = -1;
		lvLastMinRowId = -1;
		lvLastMinColId = -1;
		currentMinLineRowWidth  = 0;
		for (DrumLineView pv: dlv) {
			if (pv.getViewMode() == ViewMode.MINIMIZED){
				pv.setLayoutParams(makeLayoutParams(pv));
			}
		}
		for (SynthLineView pv: slv) {
			if (pv.getViewMode() == ViewMode.MINIMIZED){
				pv.setLayoutParams(makeLayoutParams(pv));
			}			
		}
		lvLastMinRowId = (lvLastMinColId>0?lvLastMinColId:lvLastMinRowId);
		for (DrumLineView pv: dlv) {
			if (pv.getViewMode() != ViewMode.MINIMIZED){
				pv.setLayoutParams(makeLayoutParams(pv));
			}
		}
		for (SynthLineView pv: slv) {
			if (pv.getViewMode() != ViewMode.MINIMIZED){
				pv.setLayoutParams(makeLayoutParams(pv));
			}			
		}
	}

	
	private Listener listener = null;
	
	public void setListener(Listener l) {
		listener = l;
	}

	public void open(LineView v) {
		if (v.viewMode == ViewMode.NORMAL) {
			dq.remove(v);
			dq.add(v);
			return;
		}
		if (maxLineViewShowing > 0) {
			if (dq.size() >= maxLineViewShowing) {
				LineView v1 = dq.remove();
				v1.setViewMode(ViewMode.MINIMIZED, false);
			}
		}
		dq.add(v);
		v.setViewMode(ViewMode.NORMAL, false);
		layoutChildren();
	}

	public void setMaxLinesViews(int i) {
		maxLineViewShowing = i;
		setToPattern(currentPatch, currentPattern);
	}
	

}

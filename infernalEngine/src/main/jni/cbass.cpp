/*
 * cbass.cpp
 *
 *  Created on: Nov 10, 2015
 *      Author: dak
 */


#include <stdlib.h>

#include "com_mayaswell_infernal_Bassline.h"

#include "CBassline.h"
#include "BusMixer.h"

const float A_E	= 2.0;
const float pow2Range = 6;

FunctionTable 		pow2Wavetable(wavetableLength);
FunctionTable 		exp_1_FalloffWavetable(wavetableLength);
FunctionTable 		logFalloffWavetable(wavetableLength);
FunctionTable 		exp2TuneWavetable(wavetableLength);

BoundedLookup		pow2_6(&pow2Wavetable, -pow2Range, pow2Range);
//BoundedLookup		exp2_tune_psi(&exp2TuneWavetable, -pad2Octs, pad2Octs);


JNIEXPORT jlong JNICALL Java_com_mayaswell_infernal_Bassline_allocCBass(JNIEnv *env, jclass obj, jint id, jint nEnv, jint nEnvTgt, jint nLfo, jint nLfoTgt)
{
	return (jlong)(new CBassline(id, nEnv, nEnvTgt, nLfo, nLfoTgt));
}

JNIEXPORT void JNICALL Java_com_mayaswell_infernal_Bassline_deleteCBass(JNIEnv *env, jclass obj, jlong padSamplePointer)
{
	if (padSamplePointer == 0) return;
	delete ((CBassline *)padSamplePointer);
}

JNIEXPORT void JNICALL Java_com_mayaswell_infernal_Bassline_setCBassBufsize(JNIEnv *env, jclass obj, jlong padSamplePointer, jint bufsize)
{
	if (padSamplePointer == 0) return;
	((CBassline *)padSamplePointer)->setBufsize(bufsize);
}

JNIEXPORT void JNICALL Java_com_mayaswell_infernal_Bassline_globalCBassInit(JNIEnv *env, jclass obj)
{
	int i;
	for(i=0;i<=wavetableLength;i++) {
		sineWavetable[i] = sin(2*M_PI*((float)i/(float)wavetableLength));
		negSawWavetable[i] = ((float)(wavetableLength-2*i)/(float)wavetableLength);
		posSawWavetable[i] = ((float)(2*i-wavetableLength)/(float)wavetableLength);
		negExpWavetable[i] = (2*exp(A_E* ((float)(wavetableLength-2*i)/(float)wavetableLength))/exp(A_E)) - 1;
		posExpWavetable[i] = (2*exp(A_E* ((float)(2*i-wavetableLength)/(float)wavetableLength))/exp(A_E)) - 1;
		squareWavetable[i] = (i<(wavetableLength/2))?1:-1;

		pow2Wavetable[i] = pow(2, pow2Range*((float)(2*i-wavetableLength))/wavetableLength);
//		exp2TuneWavetable[i] = pow(2, pad2Octs*((float)(2*i-wavetableLength))/wavetableLength);

		exp_1_FalloffWavetable[i]= 1. - exp(((float)(i-(wavetableLength/2.))/(wavetableLength/2.))*FALLOFF);
		logFalloffWavetable[i]=log((((float)i/wavetableLength)*FALLOFF+1));
	}
}

JNIEXPORT void JNICALL Java_com_mayaswell_infernal_Bassline_globalCBassCleanup(JNIEnv *env, jclass obj)
{

}


JNIEXPORT jboolean JNICALL Java_com_mayaswell_infernal_Bassline_setCBassEnv(JNIEnv *env, jclass obj, jlong basslinePointer, jint which, jbyte reset, jboolean lock,
		jfloat attackT, jfloat decayT, jfloat sustainT, jfloat sustainL, jfloat releaseT)
{
	if (basslinePointer == 0) return false;
	return ((CBassline *)basslinePointer)->setEnvelope(which, reset, lock, attackT, decayT, sustainT, sustainL, releaseT);
}


JNIEXPORT jboolean JNICALL Java_com_mayaswell_infernal_Bassline_setCBassNEnv(JNIEnv *env, jclass obj, jlong basslinePointer, jint n)
{
	if (basslinePointer == 0) return false;
	return ((CBassline *)basslinePointer)->setNEnvelope(n);
}


JNIEXPORT jboolean JNICALL Java_com_mayaswell_infernal_Bassline_setCBassNEnvTgt(JNIEnv *env, jclass obj, jlong basslinePointer, jint which, jint ntgt)
{
	if (basslinePointer == 0) return false;
	return ((CBassline *)basslinePointer)->setNEnvelopeTarget(which, ntgt);
}

JNIEXPORT jboolean JNICALL Java_com_mayaswell_infernal_Bassline_setCBassEnvTgt(JNIEnv *env, jclass obj, jlong basslinePointer, jint which, jint whicht, jint tgt, jfloat amt)
{
	if (basslinePointer == 0) return false;
	return ((CBassline *)basslinePointer)->setEnvelopeTarget(which, whicht, tgt, amt);
}

JNIEXPORT jboolean JNICALL Java_com_mayaswell_infernal_Bassline_setCBassEnvTgtAmt(JNIEnv *env, jclass obj, jlong basslinePointer, jint which, jint whicht, jfloat amt)
{
	if (basslinePointer == 0) return false;
	return ((CBassline *)basslinePointer)->setEnvelopeAmount(which, whicht, amt);
}

JNIEXPORT jboolean JNICALL Java_com_mayaswell_infernal_Bassline_setCBassEnvReset(JNIEnv *env, jclass obj, jlong basslinePointer, jint which, jbyte rst)
{
	if (basslinePointer == 0) return false;
	return ((CBassline *)basslinePointer)->setEnvelopeReset(which, rst);
}

JNIEXPORT jboolean JNICALL Java_com_mayaswell_infernal_Bassline_setCBassEnvLock(JNIEnv *env, jclass obj, jlong basslinePointer, jint which, jboolean lck)
{
	if (basslinePointer == 0) return false;
	return ((CBassline *)basslinePointer)->setEnvelopeLock(which, lck);
}

JNIEXPORT jboolean JNICALL Java_com_mayaswell_infernal_Bassline_setCBassEnvAtt(JNIEnv *env, jclass obj, jlong basslinePointer, jint which, jfloat t)
{
	if (basslinePointer == 0) return false;
	return ((CBassline *)basslinePointer)->setEnvelopeAttack(which, t);
}

JNIEXPORT jboolean JNICALL Java_com_mayaswell_infernal_Bassline_setCBassEnvDec(JNIEnv *env, jclass obj, jlong basslinePointer, jint which, jfloat t)
{
	if (basslinePointer == 0) return false;
	return ((CBassline *)basslinePointer)->setEnvelopeDecay(which, t);
}

JNIEXPORT jboolean JNICALL Java_com_mayaswell_infernal_Bassline_setCBassEnvSus(JNIEnv *env, jclass obj, jlong basslinePointer, jint which, jfloat t)
{
	if (basslinePointer == 0) return false;
	return ((CBassline *)basslinePointer)->setEnvelopeSustain(which, t);
}

JNIEXPORT jboolean JNICALL Java_com_mayaswell_infernal_Bassline_setCBassEnvSusLevel(JNIEnv *env, jclass obj, jlong basslinePointer, jint which, jfloat l)
{
	if (basslinePointer == 0) return false;
	return ((CBassline *)basslinePointer)->setEnvelopeSustainLevel(which, l);
}

JNIEXPORT jboolean JNICALL Java_com_mayaswell_infernal_Bassline_setCBassEnvRel(JNIEnv *env, jclass obj, jlong basslinePointer, jint which, jfloat t)
{
	if (basslinePointer == 0) return false;
	return ((CBassline *)basslinePointer)->setEnvelopeRelease(which, t);
}

JNIEXPORT jboolean JNICALL Java_com_mayaswell_infernal_Bassline_setCBassLFO(JNIEnv *env, jclass obj, jlong basslinePointer, jint which,
		jbyte wf, jbyte reset, jboolean lock, jfloat rate, jfloat phs)
{
	if (basslinePointer == 0) return false;
	return ((CBassline *)basslinePointer)->setLFO(which, wf, reset, lock, rate, phs);
}

JNIEXPORT jboolean JNICALL Java_com_mayaswell_infernal_Bassline_setCBassNLFO(JNIEnv *env, jclass obj, jlong basslinePointer, jint n)
{
	if (basslinePointer == 0) return false;
	return ((CBassline *)basslinePointer)->setNLFO(n);
}


JNIEXPORT jboolean JNICALL Java_com_mayaswell_infernal_Bassline_setCBassNLFOTgt(JNIEnv *env, jclass obj, jlong basslinePointer, jint which, jint ntgt)
{
	if (basslinePointer == 0) return false;
	return ((CBassline *)basslinePointer)->setNLFOTarget(which, ntgt);
}

JNIEXPORT jboolean JNICALL Java_com_mayaswell_infernal_Bassline_setCBassLFOTgt(JNIEnv *env, jclass obj, jlong basslinePointer, jint which, jint whicht, jint tgt, jfloat amt)
{
	if (basslinePointer == 0) return false;
	return ((CBassline *)basslinePointer)->setLFOTarget(which, whicht, tgt, amt);
}
JNIEXPORT jboolean JNICALL Java_com_mayaswell_infernal_Bassline_setCBassLFOTgtAmt(JNIEnv *env, jclass obj, jlong basslinePointer, jint which, jint whicht, jfloat amt)
{
	if (basslinePointer == 0) return false;
	return ((CBassline *)basslinePointer)->setLFOAmount(which, whicht, amt);
}

JNIEXPORT jboolean JNICALL Java_com_mayaswell_infernal_Bassline_setCBassLFOReset(JNIEnv *env, jclass obj, jlong basslinePointer, jint which, jbyte rst)
{
	if (basslinePointer == 0) return false;
	return ((CBassline *)basslinePointer)->setLFOReset(which, rst);
}

JNIEXPORT jboolean JNICALL Java_com_mayaswell_infernal_Bassline_setCBassLFOLock(JNIEnv *env, jclass obj, jlong basslinePointer, jint which, jboolean lck)
{
	if (basslinePointer == 0) return false;
	return ((CBassline *)basslinePointer)->setLFOLock(which, lck);
}

JNIEXPORT jboolean JNICALL Java_com_mayaswell_infernal_Bassline_setCBassLFOWave(JNIEnv *env, jclass obj, jlong basslinePointer, jint which, jbyte wf)
{
	if (basslinePointer == 0) return false;
	return ((CBassline *)basslinePointer)->setLFOWave(which, wf);
}

JNIEXPORT jboolean JNICALL Java_com_mayaswell_infernal_Bassline_setCBassLFORate(JNIEnv *env, jclass obj, jlong basslinePointer, jint which, jfloat amt)
{
	if (basslinePointer == 0) return false;
	return ((CBassline *)basslinePointer)->setLFORate(which, amt);
}

JNIEXPORT jboolean JNICALL Java_com_mayaswell_infernal_Bassline_setCBassLFOPhase(JNIEnv *env, jclass obj, jlong basslinePointer, jint which, jfloat amt)
{
	if (basslinePointer == 0) return false;
	return ((CBassline *)basslinePointer)->setLFOPhase(which, amt);
}

JNIEXPORT jboolean JNICALL Java_com_mayaswell_infernal_Bassline_setNSend(JNIEnv *env, jclass obj, jlong basslinePointer, jint n) {
	if (basslinePointer == 0) return false;
	return ((CBassline *)basslinePointer)->setNSend(n);
}


JNIEXPORT jboolean JNICALL Java_com_mayaswell_infernal_Bassline_setSend(JNIEnv *env, jclass obj, jlong basslinePointer, jlong busPointer, jfloat gain, jboolean mute) {
	if (basslinePointer == 0 || busPointer == 0) {
		return false;
	}
	return ((CBassline *)basslinePointer)->setSend((Bus *)busPointer, gain, mute);
}


JNIEXPORT jboolean JNICALL Java_com_mayaswell_infernal_Bassline_setSendGain(JNIEnv *env, jclass obj, jlong basslinePointer, jlong busPointer, jfloat gain) {
	if (basslinePointer == 0 || busPointer == 0) return false;
	return ((CBassline *)basslinePointer)->setSendGain((Bus *)busPointer, gain);
}


JNIEXPORT jboolean JNICALL Java_com_mayaswell_infernal_Bassline_setSendMute(JNIEnv *env, jclass obj, jlong basslinePointer, jlong busPointer, jboolean mute) {
	if (basslinePointer == 0 || busPointer == 0) return false;
	return ((CBassline *)basslinePointer)->setSendMute((Bus *)busPointer, mute);
}


JNIEXPORT void JNICALL Java_com_mayaswell_infernal_Bassline_noteOn(JNIEnv *env, jclass obj, jlong basslinePointer, jshort note, jshort vel) {
	if (basslinePointer == 0) return;
	((CBassline *)basslinePointer)->noteOn(note, vel);
}


JNIEXPORT void JNICALL Java_com_mayaswell_infernal_Bassline_noteOff(JNIEnv *env, jclass obj, jlong basslinePointer) {
	if (basslinePointer == 0) return;
	((CBassline *)basslinePointer)->noteOff();
}


JNIEXPORT void JNICALL Java_com_mayaswell_infernal_Bassline_modulatorReset__J(JNIEnv *env, jclass obj, jlong basslinePointer) {
	if (basslinePointer == 0) return;
	((CBassline *)basslinePointer)->resetModulators();
}


JNIEXPORT void JNICALL Java_com_mayaswell_infernal_Bassline_modulatorReset__JS(JNIEnv *env, jclass obj, jlong basslinePointer, jshort rstwh) {
	if (basslinePointer == 0) return;
	((CBassline *)basslinePointer)->resetModulators(rstwh);
}


JNIEXPORT void JNICALL Java_com_mayaswell_infernal_Bassline_setWaveform(JNIEnv *env, jclass type, jlong basslinePointer, jint wf) {
	if (basslinePointer == 0) return;
	((CBassline *)basslinePointer)->setWaveform(wf);
}

JNIEXPORT void JNICALL Java_com_mayaswell_infernal_Bassline_setWaveShape(JNIEnv *env, jclass type, jlong basslinePointer, jfloat shape)
{
	if (basslinePointer == 0) return;
	((CBassline *)basslinePointer)->setWaveShape(shape);
}

JNIEXPORT void JNICALL Java_com_mayaswell_infernal_Bassline_setGain(JNIEnv *env, jclass obj, jlong basslinePointer, jfloat g) {
	if (basslinePointer == 0) return;
	((CBassline *)basslinePointer)->setGain(g);
}


JNIEXPORT void JNICALL Java_com_mayaswell_infernal_Bassline_setPan(JNIEnv *env, jclass obj, jlong basslinePointer, jfloat p) {
	if (basslinePointer == 0) return;
	((CBassline *)basslinePointer)->setPan(p);
}


JNIEXPORT void JNICALL Java_com_mayaswell_infernal_Bassline_setTune(JNIEnv *env, jclass obj, jlong basslinePointer, jfloat t) {
	if (basslinePointer == 0) return;
	((CBassline *)basslinePointer)->setTune(t);
}

JNIEXPORT void JNICALL Java_com_mayaswell_infernal_Bassline_setGlide(JNIEnv *env, jclass obj, jlong basslinePointer, jfloat g) {
	if (basslinePointer == 0) return;
	((CBassline *)basslinePointer)->setGlide(g);
}


JNIEXPORT void JNICALL Java_com_mayaswell_infernal_Bassline_setFilterType(JNIEnv *env, jclass obj, jlong basslinePointer, jint typ) {
	if (basslinePointer == 0) return;
	((CBassline *)basslinePointer)->setFltType(typ);
}


JNIEXPORT void JNICALL Java_com_mayaswell_infernal_Bassline_setFilterFrequency(JNIEnv *env, jclass obj, jlong basslinePointer, jfloat freq) {
	if (basslinePointer == 0) return;
	((CBassline *)basslinePointer)->setFltFrequency(freq);
}


JNIEXPORT void JNICALL Java_com_mayaswell_infernal_Bassline_setFilterEnvMod(JNIEnv *env, jclass obj, jlong basslinePointer, jfloat e) {
	if (basslinePointer == 0) return;
	((CBassline *)basslinePointer)->setFltEnvMod(e);
}


JNIEXPORT void JNICALL Java_com_mayaswell_infernal_Bassline_setFilterResonance(JNIEnv *env, jclass obj, jlong basslinePointer, jfloat res) {
	if (basslinePointer == 0) return;
	((CBassline *)basslinePointer)->setFltResonance(res);
}


JNIEXPORT void JNICALL Java_com_mayaswell_infernal_Bassline_setFilter(JNIEnv *env, jclass obj, jlong basslinePointer, jint typ, jfloat f, jfloat r, jfloat e) {
	if (basslinePointer == 0) return;
	((CBassline *)basslinePointer)->setFilter(typ, f, r, e);
}


JNIEXPORT jboolean JNICALL Java_com_mayaswell_infernal_Bassline_setFilterEnvelopeAttackT(JNIEnv *env, jclass obj, jlong basslinePointer, jfloat at) {
	if (basslinePointer == 0) return (jboolean)false;
	((CBassline *)basslinePointer)->setFilterEnvAttT(at);
	return true;
}


JNIEXPORT jboolean JNICALL Java_com_mayaswell_infernal_Bassline_setFilterEnvelopeDecayT(JNIEnv *env, jclass obj, jlong basslinePointer, jfloat dt) {
	if (basslinePointer == 0) return (jboolean)false;
	((CBassline *)basslinePointer)->setFilterEnvDecT(dt);
	return true;
}


JNIEXPORT jboolean JNICALL Java_com_mayaswell_infernal_Bassline_setFilterEnvelopeHoldT(JNIEnv *env, jclass obj, jlong basslinePointer, jfloat ht) {
	if (basslinePointer == 0) return (jboolean)false;
	((CBassline *)basslinePointer)->setFilterEnvHoldT(ht);
	return true;
}


JNIEXPORT jboolean JNICALL Java_com_mayaswell_infernal_Bassline_setFilterEnvelopeAttackRate(JNIEnv *env, jclass obj, jlong basslinePointer, jfloat ar) {
	if (basslinePointer == 0) return (jboolean)false;
	((CBassline *)basslinePointer)->setFilterEnvAttack(ar);
	return true;
}


JNIEXPORT jboolean JNICALL Java_com_mayaswell_infernal_Bassline_setFilterEnvelopeDecayRate(JNIEnv *env, jclass obj, jlong basslinePointer, jfloat dr) {
	if (basslinePointer == 0) return (jboolean)false;
	((CBassline *)basslinePointer)->setFilterEnvDecay(dr);
	return true;
}


JNIEXPORT jint JNICALL Java_com_mayaswell_infernal_Bassline_activeBusCount(JNIEnv *env, jclass obj, jlong basslinePointer) {
	if (basslinePointer == 0) return (jint)0;
	return ((CBassline *)basslinePointer)->nActiveSend;
}

JNIEXPORT jint JNICALL Java_com_mayaswell_infernal_Bassline_playCBassMixer(JNIEnv *env, jclass obj, jlong basslinePointer,
		jlong mxPtr, jint buffInd, jint nIterFrames, jint nGateFrames, jshort nOutChannels) {
	if (basslinePointer == 0 || mxPtr == 0) return (jint)0;

	nIterFrames = ((CBassline *)basslinePointer)->playChunkMixer(((BusMixer *)mxPtr), buffInd, nIterFrames, nGateFrames, nOutChannels);
	return (jint)nIterFrames;

}

JNIEXPORT void JNICALL Java_com_mayaswell_infernal_Bassline_setCBassTempo(JNIEnv *env, jclass obj, jlong basslinePointer, jfloat t)
{
	if (basslinePointer == 0) return;
	((CBassline *)basslinePointer)->setTempo(t);
}

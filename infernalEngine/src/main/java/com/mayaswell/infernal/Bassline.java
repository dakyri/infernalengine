package com.mayaswell.infernal;

import java.util.ArrayList;

import android.content.Context;
import android.util.Log;
import android.widget.ArrayAdapter;

import com.mayaswell.audio.AudioPlayer;
import com.mayaswell.audio.Control;
import com.mayaswell.audio.Controllable;
import com.mayaswell.audio.ControlsAdapter;
import com.mayaswell.audio.Envelope;
import com.mayaswell.audio.ICControl;
import com.mayaswell.audio.LFO;
import com.mayaswell.audio.ControlsAdapter.ControlsFilter;
import com.mayaswell.audio.Modulator.LFWave;
import com.mayaswell.audio.Modulator.ModulationTarget;
import com.mayaswell.audio.Modulator.ResetMode;
import com.mayaswell.infernal.InfernalEngine.Global;
import com.mayaswell.infernal.Pattern.Line;
import com.mayaswell.infernal.Pattern.Stepper;
import com.mayaswell.spacegun.Bus;
import com.mayaswell.spacegun.BusMixer;
import com.mayaswell.spacegun.Send;

public class Bassline extends AudioPlayer implements Controllable, Envelope.Host, LFO.Host, Send.Host {

	static {
		System.loadLibrary("cbass");
	}

	public static final int PLAY_COMPLETE=3;

	private long cBassPointer = 0;
	public BasslineState state = new BasslineState();

	public static native void globalCBassInit();
	public static native void globalCBassCleanup();

	private static native long allocCBass(int id, int nEnv, int nEnvTgt, int nLfo, int nLfoTgt);
	private static native void deleteCBass(long ptr);
	private static native void setCBassBufsize(long ptr, int bufsize);

	private static native void setCBassTempo(long ptr, float tempo);
	private static native boolean setCBassEnv(long ptr, int which, byte reset, boolean lock, float attackT, float decayT, float sustainT, float sustainL, float releaseT);
	private static native boolean setCBassNEnv(long ptr, int n);
	private static native boolean setCBassNEnvTgt(long ptr, int i, int m);
	private static native boolean setCBassEnvTgt(long ptr, int i, int j, int cc, float amt);
	private static native boolean setCBassEnvTgtAmt(long ptr, int i, int j, float amt);
	private static native boolean setCBassEnvReset(long ptr, int i, byte rst);
	private static native boolean setCBassEnvLock(long ptr, int i, boolean lock);
	private static native boolean setCBassEnvAtt(long ptr, int i, float t);
	private static native boolean setCBassEnvDec(long ptr, int i, float t);
	private static native boolean setCBassEnvSus(long ptr, int i, float t);
	private static native boolean setCBassEnvSusLevel(long ptr, int i, float l);
	private static native boolean setCBassEnvRel(long ptr, int i, float t);
	private static native boolean setCBassLFO(long ptr, int which, byte wf, byte reset, boolean lock, float rate, float phs);
	private static native boolean setCBassNLFO(long ptr, int n);
	private static native boolean setCBassNLFOTgt(long ptr, int i, int n);
	private static native boolean setCBassLFOTgt(long ptr, int i, int j, int cc, float amt);
	private static native boolean setCBassLFOTgtAmt(long ptr, int i, int j, float amt);
	private static native boolean setCBassLFOReset(long ptr, int i, byte rst);
	private static native boolean setCBassLFOLock(long ptr, int i, boolean lock);
	private static native boolean setCBassLFOWave(long ptr, int i, byte wf);
	private static native boolean setCBassLFORate(long ptr, int i, float r);
	private static native boolean setCBassLFOPhase(long ptr, int i, float p);

	private static native boolean setNSend(long ptr, int n);
	private static native boolean setSend(long ptr, long busp, float f, boolean m);
	private static native boolean setSendGain(long ptr, long busp, float f);
	private static native boolean setSendMute(long ptr, long busp, boolean m);
	
	protected static native void noteOn(long ptr, short note, short vel);
	protected static native void noteOff(long ptr);
	protected static native void modulatorReset(long ptr);
	protected static native void modulatorReset(long ptr, short mode);
	
	protected static native void setGain(long ptr, float gain);
	protected static native void setPan(long ptr, float pan);
	protected static native void setTune(long ptr, float tune);
	protected static native void setGlide(long ptr, float glide);
	protected static native void setWaveform(long ptr, int wf);
	protected static native void setWaveShape(long ptr, float sh);

	protected static native void setFilterType(long ptr, int t);
	protected static native void setFilterFrequency(long ptr, float f);
	protected static native void setFilterEnvMod(long ptr, float f);
	protected static native void setFilterResonance(long ptr, float r);
	protected static native void setFilter(long ptr, int t, float f, float r, float e);

	protected static native boolean setFilterEnvelopeAttackT(long ptr, float t);
	protected static native boolean setFilterEnvelopeDecayT(long ptr, float t);
	protected static native boolean setFilterEnvelopeHoldT(long ptr, float t);
	protected static native boolean setFilterEnvelopeAttackRate(long ptr, float t);
	protected static native boolean setFilterEnvelopeDecayRate(long ptr, float t);
	
	protected static native int playCBassMixer(long ptr, long mxPtr, int buffInd, int nIterFrames, int nGateFrames, short nOutChannels);
	
	protected static native int activeBusCount(long ptr);

	private int nGateFrames = 0;

	private String name;

	public interface Listener {
		void playStarted(Bassline p);
		void playComplete(Bassline padSample);
	}

	public Bassline(String nm, int i) {
		name = nm;
		id = i;
		cBassPointer = allocCBass(id, CControl.maxEnv, CControl.maxEnvTgt, CControl.maxLFO, CControl.maxLFOTgt);
	}

	public void cleanup() {
		if (cBassPointer != 0) {
			deleteCBass(cBassPointer);
			cBassPointer = 0;
		}
	}

	/*
	 * 	lfo entry points
	 */
	public boolean setNLFO(int n)
	{
		return setCBassNLFO(cBassPointer, n);
	}

	public boolean setLFOList(ArrayList<LFO> lfo)
	{
		setCBassNLFO(cBassPointer, lfo.size());
		int i=0;
		for (LFO l: lfo) {
			if (l.countValidTargets() > 0) {
				setCBassLFO(cBassPointer, i, l.waveform.code(), l.trigger.code(), l.tempoLock, l.rate, l.phase);
				int j=0;
				for (ModulationTarget mt: l.target) {
					if (mt != null/* && mt.target.getTypeCode() != CControl.NOTHING*/) {
						setCBassLFOTgt(cBassPointer, i, j++, mt.target!=null?mt.target.ccontrolId():CControl.NOTHING, mt.amount);
					}
				}
				setCBassNLFOTgt(cBassPointer, i, j);
				i++;
			}
		}
		return true;
	}
	
	public boolean setLFORate(int xyci, float amt)
	{
		if (xyci >= 0 && xyci <state.lfo.size()) state.lfo.get(xyci).rate = amt;
		return setCBassLFORate(cBassPointer, xyci, amt);
	}

	public boolean setLFOPhase(int xyci, float amt)
	{
		if (xyci >= 0 && xyci <state.lfo.size()) state.lfo.get(xyci).phase = amt;
		return setCBassLFOPhase(cBassPointer, xyci, amt);
	}

	public boolean setLFOLock(int xyci, boolean amt)
	{
		if (xyci >= 0 && xyci <state.lfo.size()) state.lfo.get(xyci).tempoLock = amt;
		return setCBassLFOLock(cBassPointer, xyci, amt);
	}

	public boolean setLFOReset(int xyci, ResetMode amt)
	{
		if (xyci >= 0 && xyci <state.lfo.size()) state.lfo.get(xyci).trigger = amt;
		return setCBassLFOReset(cBassPointer, xyci, amt.code());
	}

	public boolean setLFOWave(int xyci, LFWave amt)
	{
		if (xyci >= 0 && xyci <state.lfo.size()) state.lfo.get(xyci).waveform = amt;
		return setCBassLFOWave(cBassPointer, xyci, amt.code());
	}
	
	public boolean setNLFOTarget(int xyci, int n)
	{
		return setCBassNLFOTgt(cBassPointer, xyci, n);
	}

	public boolean setLFOTarget(int xyci, int xycj, ICControl target, float amt)
	{
		if (xyci >= 0 && xyci <state.lfo.size() && xycj >= 0 && xycj <state.lfo.get(xyci).target.size()) {
//			Log.d("padsample", String.format("setlfotgt %d %d %s %g", xyci, xycj, target.abbrevName(), amt));
			state.lfo.get(xyci).target.get(xycj).target = target;
			state.lfo.get(xyci).target.get(xycj).amount = amt;
		}
		return setCBassLFOTgt(cBassPointer, xyci, xycj, target!=null?target.ccontrolId():CControl.NOTHING, amt);
	}

	public boolean setLFOTargetAmt(int xyci, int xycj, float amt)
	{
		if (xyci >= 0 && xyci <state.lfo.size() && xycj >= 0 && xycj <state.lfo.get(xyci).target.size()) {
			state.lfo.get(xyci).target.get(xycj).amount = amt;
		}
		return setCBassLFOTgtAmt(cBassPointer, xyci, xycj, amt);
	}
	
	@Override
	public int maxLFO() {
		return Global.LFO_PER_PAD;
	}
	@Override
	public int maxLFOTgt() {
		return Global.MAX_LFO_TGT;
	}
	/*
	 * 	envelope entry points
	 */
	public boolean setNEnvelope(int n)
	{
		return setCBassNEnv(cBassPointer, n);
	}

	public boolean setEnvList(ArrayList<Envelope> envelope)
	{
		setCBassNEnv(cBassPointer, envelope.size());
		int i=0;
		for (Envelope l: envelope) {
			if (l.countValidTargets() > 0) {
				setCBassEnv(cBassPointer, i++, l.trigger.code(), l.tempoLock, l.attackT, l.decayT, l.sustainT, l.sustainL, l.releaseT);
				int j=0;
				for (ModulationTarget mt: l.target) {
					if (mt != null/* && mt.target.getTypeCode() != CControl.NOTHING*/) { // need to set the NOTHING value too
						setCBassEnvTgt(cBassPointer, i, j++, mt.target!=null?mt.target.ccontrolId():CControl.NOTHING, mt.amount);
					}
				}
				setCBassNLFOTgt(cBassPointer, i, j);
				i++;
			}
		}
		return true;
	}
	
	public boolean setEnvelopeLock(int xyci, boolean amt)
	{
		if (xyci >= 0 && xyci <state.envelope.size()) state.envelope.get(xyci).tempoLock = amt;
		return setCBassEnvLock(cBassPointer, xyci, amt);
	}

	public boolean setEnvelopeReset(int xyci, ResetMode amt)
	{
		if (xyci >= 0 && xyci <state.envelope.size()) state.envelope.get(xyci).trigger = amt;
		return setCBassEnvReset(cBassPointer, xyci, amt.code());
	}

	public boolean setEnvelopeAttack(int xyci, float t)
	{
		if (xyci >= 0 && xyci <state.envelope.size()) state.envelope.get(xyci).attackT = t;
		return setCBassEnvAtt(cBassPointer, xyci, t);
	}

	public boolean setEnvelopeDecay(int xyci, float t)
	{
		if (xyci >= 0 && xyci <state.envelope.size()) state.envelope.get(xyci).decayT = t;
		return setCBassEnvDec(cBassPointer, xyci, t);
	}

	public boolean setEnvelopeSustain(int xyci, float t)
	{
		if (xyci >= 0 && xyci <state.envelope.size()) state.envelope.get(xyci).sustainT = t;
		return setCBassEnvSus(cBassPointer, xyci, t);
	}

	public boolean setEnvelopeSustainLevel(int xyci, float l)
	{
		if (xyci >= 0 && xyci <state.envelope.size()) state.envelope.get(xyci).sustainL = l;
		return setCBassEnvSusLevel(cBassPointer, xyci, l);
	}

	public boolean setEnvelopeRelease(int xyci, float t)
	{
		if (xyci >= 0 && xyci <state.envelope.size()) state.envelope.get(xyci).releaseT = t;
		return setCBassEnvRel(cBassPointer, xyci, t);
	}

	public boolean setNEnvelopeTarget(int xyci, int n)
	{
		return setCBassNEnvTgt(cBassPointer, xyci, n);
	}

	public boolean setEnvelopeTarget(int xyci, int xycj, ICControl target, float amt)
	{
		if (xyci >= 0 && xyci <state.envelope.size() && xycj >= 0 && xycj <state.envelope.get(xyci).target.size()) {
			state.envelope.get(xyci).target.get(xycj).target = target;
			state.envelope.get(xyci).target.get(xycj).amount = amt;
		}
		return setCBassEnvTgt(cBassPointer, xyci, xycj, target!=null?target.ccontrolId():CControl.NOTHING, amt);
	}

	public boolean setEnvelopeTargetAmt(int xyci, int xycj, float amt)
	{
		if (xyci >= 0 && xyci <state.envelope.size() && xycj >= 0 && xycj <state.envelope.get(xyci).target.size()) state.envelope.get(xyci).target.get(xycj).amount = amt;
		return setCBassEnvTgtAmt(cBassPointer, xyci, xycj, amt);
	}

	@Override
	public int maxEnv() {
		return Global.ENV_PER_PAD;
	}
	@Override
	public int maxEnvTgt() {
		return Global.MAX_ENV_TGT;
	}


	protected ControlsAdapter interfaceTgtAdapter = null;
	protected ControlsAdapter lfoTgtAdapter = null;
	protected ControlsAdapter envTgtAdapter = null;
	
	@Override
	public ControlsAdapter getLFOTgtAdapter() {
		return lfoTgtAdapter;
	}
	@Override
	public ControlsAdapter getEnvTgtAdapter() {
		return envTgtAdapter;
	}


	@Override
	public ControlsAdapter getInterfaceTgtAdapter() {
		return interfaceTgtAdapter;
	}

	@Override
	public ArrayList<Envelope> envs() {
		return state.envelope;
	}
	
	@Override
	public ArrayList<LFO> lfos() {
		return state.lfo;
	}
	

/*
 * controllable entry points 
 */
	@Override
	public boolean alwaysLockedTempo() {
		return true;
	}
	
	protected ControlsFilter inUseModulatorFilter = new ControlsFilter() {
		@Override
		public boolean isAvailable(Control c) {
			CControl cc = (CControl) c;
			if (cc.isEnvControl()) {
				if (state == null) return false;
				ArrayList<Envelope> el = state.envelope;
				int env = cc.env4Id();
				int param = cc.envParam4Id();
				if (env < state.envelope.size()) {
					if (el.get(env).target.size() == 0) return false;
					if (el.get(env).target.size() == 1 && el.get(env).target.get(0).target.ccontrolId() == CControl.NOTHING) return false;
					if (param >= 5) {
						if (param-5 >= el.get(env).target.size()) return false;
						if (el.get(env).target.get(param-5).target.ccontrolId() == CControl.NOTHING) return false;
					}
					return true;
				}
				return false;
			} else if (cc.isLFOControl()) {
				if (state == null) return false;
				ArrayList<LFO> ll = state.lfo;
				int lfo = cc.lfo4Id();
				int param = cc.lfoParam4Id();
				if (lfo < ll.size()) {
					if (ll.get(lfo).target.size() == 0) return false;
					if (ll.get(lfo).target.size() == 1 && ll.get(lfo).target.get(0).target.ccontrolId() == CControl.NOTHING) return false;
					if (param == 1) {
						return false;
					} else if (param >= 2) {
						if (param-2 >= ll.get(lfo).target.size()) return false;
						if (ll.get(lfo).target.get(param-2).target.ccontrolId() == CControl.NOTHING) return false;
					}
					return true;
				}
				return false;
			}
			return true;
		}
	};

	private int id;

	@Override
	public void setupControlsAdapter(Context c, int tvrid, int ddrid) {
		interfaceTgtAdapter = new ControlsAdapter(c, tvrid);
		interfaceTgtAdapter.setDropDownViewResource(ddrid);
		interfaceTgtAdapter.add(new CControl(CControl.NOTHING));
		interfaceTgtAdapter.add(new CControl(CControl.GAIN));
		interfaceTgtAdapter.add(new CControl(CControl.PAN));
		interfaceTgtAdapter.add(new CControl(CControl.TUNE));
		interfaceTgtAdapter.add(new CControl(CControl.FLT_CUTOFF));
		interfaceTgtAdapter.add(new CControl(CControl.FLT_ENVMOD));
		interfaceTgtAdapter.add(new CControl(CControl.FLT_RESONANCE));
		interfaceTgtAdapter.add(new CControl(CControl.GLIDE));
		interfaceTgtAdapter.add(new CControl(CControl.FLTR_ENV_ATT_T));
		interfaceTgtAdapter.add(new CControl(CControl.FLTR_ENV_HOLD_T));
		interfaceTgtAdapter.add(new CControl(CControl.FLTR_ENV_DEC_T));
		interfaceTgtAdapter.add(new CControl(CControl.FLTR_ENV_ATTACK));
		interfaceTgtAdapter.add(new CControl(CControl.FLTR_ENV_DECAY));
		
		for (int i=0; i<CControl.maxBus; i++) {
			interfaceTgtAdapter.add(new CControl(CControl.busSendId(i)));
		}
		
		for (int i=0; i<CControl.maxEnv; i++) {
			interfaceTgtAdapter.add(new CControl(CControl.envAttackTimeId(i)));
			interfaceTgtAdapter.add(new CControl(CControl.envDecayTimeId(i)));
			interfaceTgtAdapter.add(new CControl(CControl.envSustainTimeId(i)));
			interfaceTgtAdapter.add(new CControl(CControl.envSustainLevelId(i)));
			interfaceTgtAdapter.add(new CControl(CControl.envReleaseTimeId(i)));
			for (int j=0; j<CControl.maxEnvTgt; j++) {
				interfaceTgtAdapter.add(new CControl(CControl.envTargetDepthId(i, j)));
			}
		}
		for (int i=0; i<CControl.maxLFO; i++) {
			interfaceTgtAdapter.add(new CControl(CControl.lfoRateId(i)));
//			interfaceTgtAdapter.add(new CControl(CControl.lfoPhaseId(i)));
			for (int j=0; j<CControl.maxLFOTgt; j++) {
//				interfaceTgtAdapter.add(new CControl(CControl.lfoTargetDepthId(i, j)));
			}
		}
		interfaceTgtAdapter.setFilter(inUseModulatorFilter);
		interfaceTgtAdapter.refilter();
		
		lfoTgtAdapter = new ControlsAdapter(c, tvrid);
		lfoTgtAdapter.setDropDownViewResource(ddrid);
		lfoTgtAdapter.add(new CControl(CControl.NOTHING));
		lfoTgtAdapter.add(new CControl(CControl.GAIN));
		lfoTgtAdapter.add(new CControl(CControl.PAN));
		lfoTgtAdapter.add(new CControl(CControl.TUNE));
		lfoTgtAdapter.add(new CControl(CControl.FLT_CUTOFF));
		lfoTgtAdapter.add(new CControl(CControl.FLT_ENVMOD));
		lfoTgtAdapter.add(new CControl(CControl.FLT_RESONANCE));
		lfoTgtAdapter.add(new CControl(CControl.GLIDE));
		lfoTgtAdapter.add(new CControl(CControl.FLTR_ENV_ATT_T));
		lfoTgtAdapter.add(new CControl(CControl.FLTR_ENV_HOLD_T));
		lfoTgtAdapter.add(new CControl(CControl.FLTR_ENV_DEC_T));
		lfoTgtAdapter.add(new CControl(CControl.FLTR_ENV_ATTACK));
		lfoTgtAdapter.add(new CControl(CControl.FLTR_ENV_DECAY));
		for (int i=0; i<CControl.maxLFO; i++) {
			lfoTgtAdapter.add(new CControl(CControl.lfoRateId(i)));
//			interfaceTgtAdapter.add(new CControl(CControl.lfoPhaseId(i)));
			for (int j=0; j<CControl.maxLFOTgt; j++) {
//				lfoTgtAdapter.add(new CControl(CControl.lfoTargetDepthId(i, j)));
			}
		}
		for (int i=0; i<CControl.maxEnv; i++) {
			for (int j=0; j<CControl.maxEnvTgt; j++) {
				lfoTgtAdapter.add(new CControl(CControl.envTargetDepthId(i, j)));
			}
		}
		lfoTgtAdapter.setFilter(inUseModulatorFilter);
		lfoTgtAdapter.refilter();
		
		envTgtAdapter = new ControlsAdapter(c, tvrid);
		envTgtAdapter.setDropDownViewResource(ddrid);
		envTgtAdapter.add(new CControl(CControl.NOTHING));
		envTgtAdapter.add(new CControl(CControl.GAIN));
		envTgtAdapter.add(new CControl(CControl.PAN));
		envTgtAdapter.add(new CControl(CControl.TUNE));
		envTgtAdapter.add(new CControl(CControl.FLT_CUTOFF));
		envTgtAdapter.add(new CControl(CControl.FLT_RESONANCE));
		envTgtAdapter.add(new CControl(CControl.GLIDE));
		envTgtAdapter.add(new CControl(CControl.FLTR_ENV_ATT_T));
		envTgtAdapter.add(new CControl(CControl.FLTR_ENV_HOLD_T));
		envTgtAdapter.add(new CControl(CControl.FLTR_ENV_DEC_T));
		envTgtAdapter.add(new CControl(CControl.FLTR_ENV_ATTACK));
		envTgtAdapter.add(new CControl(CControl.FLTR_ENV_DECAY));
		for (int i=0; i<CControl.maxLFO; i++) {
			envTgtAdapter.add(new CControl(CControl.lfoRateId(i)));
			for (int j=0; j<CControl.maxLFOTgt; j++) {
				envTgtAdapter.add(new CControl(CControl.lfoTargetDepthId(i, j)));
			}
		}
		envTgtAdapter.setFilter(inUseModulatorFilter);
		envTgtAdapter.refilter();

	}

	@Override
	public int controllableId() {
		return id;
	}

	@Override
	public String controllableType() {
		return Controllable.SYNTH;
	}

	@Override
	public float floatValue(Control cc) {
		switch (cc.ccontrolId()) {
			case CControl.GAIN: return state.gain;
			case CControl.PAN: return state.pan;
			case CControl.TUNE: return state.tune;
			case CControl.GLIDE: return state.glide;
/*
			case CControl.LOOP_START:
			case CControl.LOOP_LENGTH:
*/
			case CControl.WAVESHAPE: return state.waveformShape;

			case CControl.FLT_CUTOFF: return state.filterFrequency;
			case CControl.FLT_ENVMOD: return state.filterEnvMod;
			case CControl.FLT_RESONANCE: return state.filterResonance;
			case CControl.FLTR_ENV_DEC_T: return state.filterEnvelope.decayT;
			case CControl.FLTR_ENV_ATT_T:  return state.filterEnvelope.attackT;
			case CControl.FLTR_ENV_HOLD_T: return state.filterEnvelope.holdT;
			case CControl.FLTR_ENV_ATTACK: return state.filterEnvelope.attack;
			case CControl.FLTR_ENV_DECAY: return state.filterEnvelope.decay;
			default: {
				if (cc.ccontrolId() >= CControl.envIdBase(0) && cc.ccontrolId() < CControl.envIdBase(CControl.maxEnv)) {
					int env = CControl.env4Id(cc.ccontrolId());
					int param = CControl.envParam4Id(cc.ccontrolId());
					if (param == 0) {
						if (env<state.envelope.size()) { return state.envelope.get(env).attackT; }
					} else if (param == 1) { // decay
						if (env<state.envelope.size()) {	return state.envelope.get(env).decayT; }
					} else if (param == 2) {// sustain
						if (env<state.envelope.size()) {	return state.envelope.get(env).sustainT; }
					} else if (param == 3) {// sustain level
						if (env<state.envelope.size()) {	return state.envelope.get(env).sustainL; }
					} else if (param == 4) { // release
						if (env<state.envelope.size()) {	return state.envelope.get(env).releaseT; }
					} else {// depth(param-5);
						if (env<state.envelope.size() && (param-5)<state.envelope.get(env).target.size()) {
							return state.envelope.get(env).target.get(param-5).amount;
						}
					}
				} else if (cc.ccontrolId() >= CControl.lfoIdBase(0) && cc.ccontrolId() < CControl.lfoIdBase(CControl.maxLFO)) {
					int lfo = CControl.lfo4Id(cc.ccontrolId());
					int param = CControl.lfoParam4Id(cc.ccontrolId());
					if (param == 0) { // rate
						if (lfo<state.lfo.size()) { return state.lfo.get(lfo).rate; }
					} else if (param == 1) { // phase
						if (lfo<state.lfo.size()) { return state.lfo.get(lfo).phase; }
					} else {// depth(param-2);
						if (lfo<state.lfo.size() && (param-2) < state.lfo.get(lfo).target.size()) {
							return state.lfo.get(lfo).target.get(param-2).amount;
						}
					}
				} else if (cc.ccontrolId() >= CControl.busIdBase(0) && cc.ccontrolId() < CControl.busIdBase(CControl.maxBus)) {
					int bus = CControl.bus4SendId(cc.ccontrolId());
					int param = CControl.busParam4Id(cc.ccontrolId());
					if (param == 0)	{
						Send bs = state.send4bus(bus);
						if (bs == null) {
							Bus b = BusMixer.getBus(bus);
							if (b != null) {
								bs = new Send(b, 1);
								state.addSend(bs);
								if (bus == 1) {
									bs.set(1.0f, false);
								} else {
									bs.set(0.0f, false);
								}
							}
						}
						return bs.getGain();
					}
				}
			}
		}
		return 0;
	}

	@Override
	public void setValue(Control cc, float v) {
		switch (cc.ccontrolId()) {
			case CControl.GAIN: {
				setGain(v);
				break;
			}
			case CControl.PAN: {
				setPan(v);
				break;
			}
			case CControl.TUNE: {
				setTune(v);
				break;
			}
			case CControl.WAVESHAPE: {
				setWaveShape(v);
				break;
			}
			case CControl.FLTR_ENV_DEC_T: {
				setFilterEnvelopeDecayT(v);
				break;
			}
			case CControl.FLTR_ENV_ATT_T: {
				setFilterEnvelopeAttackT(v);
				break;
			}
			case CControl.FLTR_ENV_HOLD_T: {
				setFilterEnvelopeHoldT(v);
				break;
			}
			case CControl.FLTR_ENV_ATTACK: {
				setFilterEnvelopeAttackRate(v);
				break;
			}
			case CControl.FLTR_ENV_DECAY: {
				setFilterEnvelopeDecayRate(v);
				break;
			}
			/*
			case CControl.LOOP_START:
			case CControl.LOOP_LENGTH:
			*/
			case CControl.FLT_CUTOFF: {
				setFilterFrequency(v);
				break;
			}
			case CControl.FLT_ENVMOD: {
				setFilterEnvMod(v);
				break;
			}
			case CControl.FLT_RESONANCE: {
				setFilterResonance(v);
				break;
			}
			case CControl.GLIDE: {
				setGlide(v);
				break;
			}

			default:
				if (cc.ccontrolId() >= CControl.envIdBase(0) && cc.ccontrolId() < CControl.envIdBase(CControl.maxEnv)) {
					int env = CControl.env4Id(cc.ccontrolId());
					int param = CControl.envParam4Id(cc.ccontrolId());
					if (param == 0) { setEnvelopeAttack(env, v); }
					else if (param == 1) { setEnvelopeDecay(env, v); } // decay
					else if (param == 2) { setEnvelopeSustain(env, v); } // sustain
					else if (param == 3) { setEnvelopeSustainLevel(env, v); } // sustain level
					else if (param == 4) { setEnvelopeRelease(env, v); } // sustain release
					else { setEnvelopeTargetAmt(env, param-5, v);} // depth(param-5);
				} else if (cc.ccontrolId() >= CControl.lfoIdBase(0) && cc.ccontrolId() < CControl.lfoIdBase(CControl.maxLFO)) {
					int lfo = CControl.lfo4Id(cc.ccontrolId());
					int param = CControl.lfoParam4Id(cc.ccontrolId());
					if (param == 0) { setLFORate(lfo, v); } // rate
					if (param == 1) { setLFOPhase(lfo, v); } // phase
					else { setLFOTargetAmt(lfo, param-2, v);} // depth(param-2);
				} else if (cc.ccontrolId() >= CControl.busIdBase(0) && cc.ccontrolId() < CControl.busIdBase(CControl.maxBus)) {
					int bus = CControl.bus4SendId(cc.ccontrolId());
					int param = CControl.busParam4Id(cc.ccontrolId());
					if (param == 0)	{
						Send bs = state.send4bus(bus);
						if (bs == null) {
							Bus b = BusMixer.getBus(bus);
							if (b != null) {
								bs = new Send(b, 1);
								state.addSend(bs);
								if (bus == 1) {
									bs.set(1.0f, false);
								} else {
									bs.set(0.0f, false);
								}
								setSend(bs.getBus(), v, bs.getMute());
							}
						}
						setSendGain(bs.getBus(), v);
					}
				}
		}
	}

	@Override
	public String toString()
	{
		return "Synth "+Integer.toString(id);
	}

	@Override
	public String abbrevName()
	{
		return "S"+Integer.toString(id);
	}


	public boolean isPlaying()
	{
		return playState == State.PLAYING;
	}

	public boolean isPaused()
	{
		return playState == State.PAUSED;
	}
	
	public boolean noteOn(boolean fromSync, int note, int vel)
	{
		playState = State.PLAYING;
		noteOn(cBassPointer, (short) note, (short) vel);
		if (!fromSync) {
//			currentLoopCount = 0;
		}
//		doSync();
		return true;
	}
	
	public boolean noteOff(boolean fromSync)
	{
		playState = State.STOPPED;
		noteOff(cBassPointer);
		if (!fromSync) {
//			currentLoopCount = 0;
		}
//		doSync();
		return true;
	}
	
	@Override
	public boolean stop(boolean andTriggerEvent) {
		if (andTriggerEvent && listener != null) listener.playComplete(this);
		playState = State.STOPPED;
//		currentLoopCount = 0;
		noteOff(false);
		return true;
	}

	public void setBufsize(int bufsize)
	{
		setCBassBufsize(cBassPointer, bufsize);
	}
	
	public void setTempo(float t)
	{
//		Log.d("tempo set", String.format("pad id %d ... %g tempo", id, t));
		setCBassTempo(cBassPointer, t);
	}
	
	public void setGain(float gain) {
		state.gain = gain;
		setGain(cBassPointer, gain);
	}
	
	public void setPan(float pan) {
		state.pan = pan;
		setPan(cBassPointer, pan);
	}
	
	public void setTune(float tune) {
		state.tune = tune;
		setTune(cBassPointer, tune);
	}

	public void setWaveform(OscilatorWave waveform) {
		setWaveType(waveform.getId());
	}

	public void setWaveType(int t) {
		state.waveformType = t;
		setWaveform(cBassPointer, t);
	}

	public void setWaveShape(float shape) {
		state.waveformShape = shape;
		setWaveShape(cBassPointer, shape);
	}

	public void setGlide(float g)
	{
		state.glide = g;
		setGlide(cBassPointer, g);
	}
	
	public void setFilterType(Filter flt)
	{
		state.filterType = flt.getId();
		setFilterType(cBassPointer, flt.getId());
	}
	
	public void setFilterFrequency(float f)
	{
		state.filterFrequency = f;
		setFilterFrequency(cBassPointer, f);
	}
	
	public void setFilterEnvMod(float f)
	{
		state.filterEnvMod = f;
		setFilterEnvMod(cBassPointer, f);
	}
	
	public void setFilterResonance(float r)
	{
		state.filterResonance = r;
		setFilterResonance(cBassPointer, r);
	}

	protected void setFilterParams(int t, float f, float r, float e)
	{
		state.filterType = t;
		state.filterFrequency = f;
		state.filterResonance = r;
		state.filterEnvMod = e;
		setFilter(cBassPointer, t, f, r, e);
	}

	protected void setFilterEnvelope(float fltEnvAttack, float fltEnvDecay, float fltEnvAttT, float fltEnvHoldT, float fltEnvDecT)
	{
		state.filterEnvelope.attack = fltEnvAttack;
		state.filterEnvelope.decay = fltEnvDecay;
		state.filterEnvelope.attackT = fltEnvAttT;
		state.filterEnvelope.holdT = fltEnvHoldT;
		state.filterEnvelope.decayT = fltEnvDecT;
		setFilterEnvelopeAttackRate(cBassPointer, fltEnvAttack);
		setFilterEnvelopeDecayRate(cBassPointer, fltEnvDecay);
		setFilterEnvelopeAttackT(cBassPointer, fltEnvAttT);
		setFilterEnvelopeDecayT(cBassPointer, fltEnvDecT);
		setFilterEnvelopeHoldT(cBassPointer, fltEnvHoldT);
	}

	public void setFilterEnvelopeAttackT(float t)
	{
		state.filterEnvelope.attackT = t;
		setFilterEnvelopeAttackT(cBassPointer, t);
	}
	
	public void setFilterEnvelopeHoldT(float t)
	{
		state.filterEnvelope.holdT = t;
		setFilterEnvelopeHoldT(cBassPointer, t);
	}
	
	public void setFilterEnvelopeDecayT(float t)
	{
		state.filterEnvelope.decayT = t;
		setFilterEnvelopeDecayT(cBassPointer, t);
	}
	
	public void setFilterEnvelopeAttackRate(float r)
	{
		state.filterEnvelope.attack = r;
		setFilterEnvelopeAttackRate(cBassPointer, r);
	}
	
	public void setFilterEnvelopeDecayRate(float r)
	{
		state.filterEnvelope.decay = r;
		setFilterEnvelopeDecayRate(cBassPointer, r);
	}
	
	protected Send send(Bus b)
	{
		if (b == null) return null;
		for (Send s:state.send) {
			if (s.getBus() == b) return s;
		}
		return null;
	}
	
	public boolean setNSend(int n)
	{
		while (state.send.size() > n) {
			state.send.remove(state.send.size()-1);
		}
		return setNSend(cBassPointer, n);
	}
	
	public boolean setSend(Bus b, float f, boolean m)
	{
		if (b == null) {
			Log.d("pad", "bus is null");
			return false;
		}
		Send xyci = send(b);
		if (xyci != null && f <= 0 && m) {
			state.send.remove(xyci);
		} else {
			if (xyci == null) {
				state.send.add(new Send(b, f, m));
			} else {
				xyci.setBus(b);
				xyci.setGain(f);
				xyci.setMute(m);
			}
		}
		return  setSend(cBassPointer, b.getBusPointer(), f, m);
	}

	public boolean setSendGain(Bus b, float f)
	{
		if (b == null) {
			return false;
		}

		Send xyci = send(b);
		if (f <= 0) {
			if (xyci != null) {
				if(xyci.getMute()) state.send.remove(xyci);
				else xyci.setGain(f);
			}
		} else {
			if (xyci == null) {
				state.send.add(new Send(b, f, false));
			} else {
				xyci.setBus(b);
				xyci.setGain(f);
			}
		}
		return  setSendGain(cBassPointer, b.getBusPointer(), f);
	}

	public boolean setSendMute(Bus b, boolean m)
	{
		if (b == null) {
			return false;
		}
		Send xyci = send(b);
		if (m) {
			if (xyci != null) {
				if (xyci.getGain() <= 0) state.send.remove(xyci);
				else xyci.setMute(m);
			}
		} else {
			if (xyci == null) {
				state.send.add(new Send(b, 1, m));
			} else {
				xyci.setBus(b);
				xyci.setMute(m);
			}
		}
		return  setSendMute(cBassPointer, b.getBusPointer(), m);
	}

	/**
	 * variation on super class
	 * @param busMixPointer
	 * @param nFrames
	 * @param buffInd 
	 * @param nOutChannels
	 */
	public int playCumulative(long busMixPointer, int buffInd, int nFrames, short nOutChannels)
	{
		if (!isPlaying()) {
//			Log.d("player", "tried to play, but not really playing");
			return 0;
		}
		
		int nFramesOut = 0;
		while (nFramesOut < nFrames) {
			int nIterFrames = nFrames-nFramesOut;
			/*(currentDirection >= 0)?
						( (nFramesRemaining > ll-currentLoopFrame)?(ll - currentLoopFrame): nFramesRemaining):
						( (nFramesRemaining > currentLoopFrame)?currentLoopFrame: nFramesRemaining);*/
//			Log.d("player", String.format("pad %d loop frame %d dir %d to make %d %d %d %d tune is %g", id, currentLoopFrame, currentDirection, nFrames, nFramesOut, nFramesRemaining, nIterFrames, state.tune));
			/*if (currentStutter > 1) {
				if (nIterFrames > nextStutterFrame-currentLoopFrame) {
					nIterFrames = nextStutterFrame-currentLoopFrame;
				}
			}*/
			if (nIterFrames > 0) {
				int nIterOutFrames = playCBassMixer(cBassPointer, busMixPointer, buffInd, nIterFrames, nGateFrames, nOutChannels);
				if (nIterOutFrames == 0) {
					break;
				}
				buffInd += nOutChannels*nIterOutFrames;
	 			nFramesOut += nIterOutFrames;
	 			nGateFrames -= nIterOutFrames;
				if (nGateFrames < 0) {
					nGateFrames = 0;
				}
//				Log.d("player", String.format("pad %d played %d %d requested %d step fr %d gete fr %d", id, nIterOutFrames, nFramesOut, nIterFrames, stepFrames, nGateFrames));
			} else {
				if (!isPlaying()) {
					break;
				}
			}
			/*
			if (currentLoopFrame >= ll) {
				if (currentDirection >= 0) {
					loopEnd();
				} else {
					currentLoopFrame = ll;
				}
			} else if (currentLoopFrame <= 0){
				if (currentDirection >= 0) {
					currentLoopFrame = 0;
				} else {
					loopEnd();
				}
			} else if (currentStutter > 1) {
				if (currentLoopFrame >= nextStutterFrame) {
					doStutter();
				}
			}*/
		}
		
		return nFrames;
	}
	
	public boolean isActive() {
		return true;
	}
	private Pattern.SynthLine patternLine = null;
	public int stepFrames = 0;
	public int currentStep = 0;
	
	private float currentVelocity = 1;
	private int currentNote = 0;

	public void setPatternLine(Pattern.SynthLine p) {
		patternLine = p;
	}
	
	public Line getPatternLine() {
		return patternLine;
	}

	public boolean atStepEnd(double framesPerBeat) {
		if (patternLine != null) {
			return stepFrames >= patternLine.framesPerStep(framesPerBeat);
		}
		return false;
	}

	public int getNextStep() {
		int step = currentStep+1;
		if (patternLine != null && patternLine.vel != null && step >= patternLine.vel.length) {
			step = 0;
		}
		return step;
	}

	public void setStep(int step, int framesPerStep) {
//		Log.d("ipads", String.format("%d step", step));
		currentStep = step;
		stepFrames = 0;
		if (patternLine != null) {
			int v = patternLine.getVel(step);
			int p = patternLine.getNote(step);
			if (p >= 0 && v >= 0) {
				currentNote  = p;
				int v2 = patternLine.getNextVel(step);
				nGateFrames = (v2==Line.TIED? framesPerStep: (int)(framesPerStep * (5./9.)));
// 				gateOffFrame = nt - noteDuration*(8./18.);

				if (v > 0) { /* an attack */
					currentVelocity = (float) (v/80.0);
					noteOn(false, p, v);
				} else { /* tied */
				}
			} else { /* no note this step */
				nGateFrames = 0;
			}
			Log.d("Bassline", String.format("setStep %d -> note %d/%d => %d/%g gate %d", step, p, v, currentNote, currentVelocity, nGateFrames));
			for (Stepper s: patternLine.stepper) {
				if (step >= 0 && step < s.val.length) {
					float sv = s.val[step];
					setValue(s.control, sv);
				}
			}
		}
	}


	protected int currentLoopCount = 0;
	public int getLoopCount() {
		return currentLoopCount;
	}
	public void setLoopCount(int lc) {
		currentLoopCount = 0;
	}

	@Override
	public int nActiveBus() {
		return activeBusCount(cBassPointer);
	}

	@Override
	public Send send4Bus(Bus b) {
		if (b == null || state.send == null) {
			return null;
		}
		for (Send bs: state.send) {
			if (bs.getBus() == b) {
				return bs;
			}
		}
		return null;
	}

	protected void setSendList(ArrayList<Send> send) {
		for (Send s: send) {
			if (!setSend(s.getBus(), s.getGain(), s.getMute())) {
			}
		}
	}

	public BasslineState getState() { return state; }
	/**
	 * @param pbs
	 */
	public void setState(BasslineState pbs) {
		if (pbs == null) return;
		state = pbs;
		boolean ip = isPlaying();
		setGain(pbs.gain);	// must set gain in c
		setPan(pbs.pan);
		setFilterParams(pbs.filterType, pbs.filterFrequency, pbs.filterResonance, pbs.filterEnvMod);
		setTune(pbs.tune);
		setWaveType(pbs.waveformType);
		setWaveShape(pbs.waveformShape);
		setGlide(pbs.glide);
		setFilterEnvelope(pbs.filterEnvelope.attack, pbs.filterEnvelope.decay,
				pbs.filterEnvelope.attackT, pbs.filterEnvelope.holdT, pbs.filterEnvelope.decayT);
/*
		if (ip) {

			if(!isPlayable()) {
				stop(true);
			}
		}
*/
		setLFOList(pbs.lfo);
		setEnvList(pbs.envelope);
		setSendList(pbs.send);
		if (interfaceTgtAdapter != null) interfaceTgtAdapter.refilter();
		if (lfoTgtAdapter != null) lfoTgtAdapter.refilter();
		if (envTgtAdapter != null) envTgtAdapter.refilter();
	}

	public static ArrayAdapter<Filter> filtersAdapter = null;
	public static void setupFiltersAdapter(Context c, int tvrid, int ddrid)
	{
		filtersAdapter  = new ArrayAdapter<Filter>(c, tvrid);
		filtersAdapter.setDropDownViewResource(ddrid);
		filtersAdapter.add(new Filter(Filter.UNITY, "Unity", "unity", "unity"));
		filtersAdapter.add(new Filter(Filter.SG_LOW, "SG L.Pass", "sg-lp", "SG Lo"));
		filtersAdapter.add(new Filter(Filter.SG_BAND, "SG B.Pass", "sg-bp", "SG Band"));
		filtersAdapter.add(new Filter(Filter.SG_HIGH, "SG H.Pass", "sg-hp", "SG Hi"));
		filtersAdapter.add(new Filter(Filter.MOOG1_LOW, "Moog Lo", "moog1-lp", "Moog Lo"));
		filtersAdapter.add(new Filter(Filter.MOOG1_BAND, "Moog Band", "moog1-bp", "Moog Band"));
		filtersAdapter.add(new Filter(Filter.MOOG1_HIGH, "Moog Hi", "moog1-hp", "Moog Hi"));
		filtersAdapter.add(new Filter(Filter.MOOG2_LOW, "VCF3 Lo", "moog2-lp", "VCF3 Lo"));
		filtersAdapter.add(new Filter(Filter.MOOG2_BAND, "VCF3 Band", "moog2-bp", "VCF3 Band"));
		filtersAdapter.add(new Filter(Filter.MOOG2_HIGH, "VCF3 Hi", "moog2-hp", "VCF3 Hi"));
		filtersAdapter.add(new Filter(Filter.FORMANT, "Formant", "formant", "Formant"));
	}

	private Listener listener = null;
	public void setListener(Listener l) {
		listener = l;
	}


}

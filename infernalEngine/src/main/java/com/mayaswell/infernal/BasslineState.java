package com.mayaswell.infernal;

import com.mayaswell.audio.Controllable;
import com.mayaswell.audio.Envelope;
import com.mayaswell.audio.ExpEnvelope;
import com.mayaswell.audio.LFO;
import com.mayaswell.spacegun.AudioNodeState;
import com.mayaswell.spacegun.BusAudioNodeState;
import com.mayaswell.spacegun.Send;

public class BasslineState extends BusAudioNodeState {

	public int waveformType = Controllable.OscilatorWave.SAW;
	public float waveformShape = 0;

	public int filterType = Controllable.Filter.SG_LOW;
	public float filterFrequency = 0.3f;
	public float filterEnvMod = 0;
	public float filterResonance = 0.2f;
	public ExpEnvelope filterEnvelope = new ExpEnvelope(0, 0.4f, 0, 0, 2.1f);
	public float glide = 0.25f;
	
	public BasslineState() {
		
	}
	
	public BasslineState clone() {	
		BasslineState pbs = new BasslineState();
		
		for (Envelope e: envelope) {
			pbs.envelope.add(e.clone());
		}
		
		for (LFO l: lfo) {
			pbs.lfo.add(l.clone());
		}
		
		for (Send l: send) {
			pbs.send.add(l.clone());
		}
		return pbs;
	}

}

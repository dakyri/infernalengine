/*
 * CBassline.cpp
 *
 *  Created on: Nov 10, 2015
 *      Author: dak
 */

#include "CBassline.h"

float jfa[] = {
8,		8.5333333333,	9,		9.6,	10,		10.666667, 		11.2,	12,		12.8,	13.3333333,	14,		15,
16,		17.066666667,	18,		19.2,	20,		21.333333,		22.4,	24,		25.6,	26.6666667,	28,		30,
32,		34.133333333,	36,		38.4,	40,		42.666667, 		44.8,	48,		51.2,	53.3333333,	56,		60,
64,		68.266666667,	72,		76.8,	80,		85.333333, 		89.6,	96,		102.4,	106.666667,	112,	120,
128,	136.53333333,	144,	153.6,	160,	170.666667,		179.2,	192,	204.8,	213.333333,	224,	240,
256,	273.066666667,	288,	307.2,	320,	341.333333,		358.4,	384,	409.6,	426.66667,	448,	480,
512,	546.133333333,	576,	614.4,	620,	682.6666667,	716.8,	768,	819.2,	853.333333,	896,	960,
1024,	1092.266666667,	1152,	1228.8,	1240,	1365.333333,	1433.6,	1536,	1638.4,	1706.66667, 1792,	1920,
2048,	2184.533333333,	2304,	2457.6,	2480,	2730.666667,	2867.2,	3072,	3276.8,	3413.33333,	3584,	3840,
4096,	4369.066666667,	4608,	4915.2,	4960,	5461.333333,	5724.4,	6144,	6553.6,	6826.66667,	7168,	7680,
8192,	8738.133333333,	9216,	9830.4,	9920,	10923.666667,	11448.8, 12288
};

std::vector<float> justFrequencies((float *)jfa, (float*)(jfa+127));

float mfa[] = {
8.175800,	8.661958,	  9.177025,		9.722720,	10.300863,		10.913384,	11.562327,		12.249859,	12.978274,	  13.750002,	14.567619,	15.433855,
16.351600,	17.323917,	  18.354050,	19.445439,	20.601725,		21.826767,	23.124654,		24.499718,	25.956547,	  27.500004,	29.135239,	30.867710,
32.703200,	34.647833,	  36.708101,	38.890878,	41.203450,		43.653535,	46.249309,		48.999436,	51.913094,	  55.000007,	58.270478,	61.735421,
65.406400,	69.295667,	  73.416202,	77.781756,	82.406900,		87.307069,	92.498618,		97.998872,	103.826188,	  110.000015,	116.540956,	123.470842,
130.812800,	138.591334,	  146.832403,	155.563512,	164.813800,		174.614139,	184.997236,		195.997744,	207.652376,	  220.000029,	233.081912,	246.941683,
261.625600,	277.182668,   293.664807,	311.127025,	329.627601,		349.228278,	369.994472,		391.995488,	415.304753,	  440.000058,	466.163823,	493.883367,
523.251200,	554.365335,   587.329614,	622.254050,	659.255201,		698.456556,	739.988944,		783.990976,	830.609505,	  880.000117,	932.327647,	987.766734,
1046.50240, 1108.730671,  1174.659227, 1244.508100, 1318.510403,	1396.913111, 1479.977887,	1567.981952, 1661.219011, 1760.000233, 1864.655293,	1975.533467,
2093.00480, 2217.461342,  2349.318455, 2489.016200, 2637.020805,	2793.826222, 2959.955774,	3135.963904, 3322.438021, 3520.000467, 3729.310587,	3951.066934,
4186.00960, 4434.922684,  4698.636910, 4978.032400, 5274.041610,	5587.652444, 5919.911549,	6271.927808, 6644.876043, 7040.000934, 7458.621174,	7902.133868,
8372.01920, 8869.845368,  9397.273820, 9956.064800, 10548.08322,	11175.304888, 11839.823097,	12543.855615
};
std::vector<float> midiFrequencies((float *)mfa, (float *)(mfa+127));

CBassline::CBassline(short _id, int maxEnv, int maxEnvTgt, int maxLfo, int maxLfoTgt)
		: oscMidiNote(0)
		, oscVelocity(0)
		, pitchTable(midiFrequencies)
		, enableFilterEnvelope(true)
{
	id = _id;
	buffer = NULL;
	controlFrame = 0;
	setBufsize(4096);
	cycleBuffer = new float[framesPerControlCycle];

	gain = 1;
	pan = 0;
	filterType = com_mayaswell_audio_Controllable_Filter_UNITY;
	filterFrequency = 0.5;
	filterResonance = 0;
	filterEnvMod = 0;

	for (int i=0; i<maxEnv; i++) {
		env.push_back(Envelope(maxEnvTgt, *this));
	}
	for (int i=0; i<maxLfo; i++) {
		lfo.push_back(LFO(maxLfoTgt, *this));
	}

	nActiveEnv = 0;
	nActiveLFO = 0;
	nActiveSend = 0;

	lfoOffset.fill(0); // highest number target for a cpad
	envOffset.fill(1);

	setControlRate(framesPerControlCycle/signalSampleRate);

}

CBassline::~CBassline() {
	if (buffer != NULL) {
		delete [] buffer;
	}
	delete [] cycleBuffer;
}

void
CBassline::setBufsize(int bufsize)
{
	if (buffer != NULL) {
		delete [] buffer;
	}
	buffer = new float[bufsize];
}


bool
CBassline::setEnvelopeTarget(int which, int whichn, int cc, float amt)
{
//	__android_log_print(ANDROID_LOG_DEBUG, "CBassline", "set env target %d %d %d %g", which, whichn, cc, amt);
	if (which < 0 || which >= env.size()) {
		return false;
	}
	if (whichn >= env[which].nActiveSends || whichn < 0 || whichn >= env[which].target.size()) {
		if (cc != com_mayaswell_audio_Control_NOTHING)
			return false;
	}

	int was = env[which].target[whichn].target;

	env[which].target[whichn].target = cc;
	env[which].target[whichn].amount = amt;

//	__android_log_print(ANDROID_LOG_DEBUG, "CBassline", "done set env target %d %d %d %g", which, whichn, cc, amt);
	if (was == cc) {
		return true;
	}
	if (was == com_mayaswell_spacegun_CControl_FLT_CUTOFF) {
		if (hasLFO(com_mayaswell_spacegun_CControl_FLT_CUTOFF)<0 && hasEnv(com_mayaswell_spacegun_CControl_FLT_CUTOFF)<0) {
			(filterFrequency, filterEnvMod);
			filter.setFrequency(filterSweepMax);
		}
	} else if (was == com_mayaswell_spacegun_CControl_FLT_ENVMOD) {
		/* nb env mod can affect cutofff */
		if (hasLFO(com_mayaswell_spacegun_CControl_FLT_ENVMOD)<0 && hasEnv(com_mayaswell_spacegun_CControl_FLT_ENVMOD)<0) {
			(filterFrequency, filterEnvMod);
			filter.setFrequency(filterSweepMax);
		}
	} else if (was == com_mayaswell_spacegun_CControl_FLT_RESONANCE) {
		if (hasLFO(com_mayaswell_spacegun_CControl_FLT_RESONANCE)<0 && hasEnv(com_mayaswell_spacegun_CControl_FLT_RESONANCE)<0) {
			filter.setResonance(filterResonance);
		}
/*
	} else if (was == com_mayaswell_spacegun_CControl_TUNE) {
		if (hasLFO(com_mayaswell_spacegun_CControl_TUNE)<0 && hasEnv(com_mayaswell_spacegun_CControl_TUNE)<0) {
			setTune(tune);
		}
*/
	} else if (isLFOControl(was)) {
		int lw = lfo4Id(was);
		int pw = lfoParam4Id(was);
//		__android_log_print(ANDROID_LOG_DEBUG, "CBassline", "done set env target %d %d to %d %g was %d %d %d", which, whichn, cc, amt, was, lw, pw);
		if (pw == 0) {
			if (hasLFO(was) < 0 && hasEnv(was) < 0) {
				lfo[lw].resetRate();
			}
		}
	}
	return true;
}
/**
 * I'm not expecting this to get called ... most of the lf modulation stuff will work ... fill this sometime later for completeness,
 * but ignore until then ...
 * todo
 */
inline void
CBassline::calculateControlsMono(float &l)
{
	l = lAmp = gain;
}

bool
CBassline::setLFOTarget(int which, int whichn, int cc, float amt)
{
//	__android_log_print(ANDROID_LOG_DEBUG, "CBassline", "set lfo target %d %d %d %g", which, whichn, cc, amt);
	if (which < 0 || which >= lfo.size()) {
		return false;
	}
	if (whichn < 0 || whichn >= lfo[which].target.size()) {
		if (cc != com_mayaswell_audio_Control_NOTHING) {
			return false;
		}
	}
	int was = lfo[which].target[whichn].target;

	lfo[which].target[whichn].target = cc;
	lfo[which].target[whichn].amount = amt;

//	__android_log_print(ANDROID_LOG_DEBUG, "CBassline", "done set lfo target %d %d %d %g", which, whichn, cc, amt);
	if (was == cc) {
		return true;
	}
	if (was == com_mayaswell_spacegun_CControl_FLT_CUTOFF) {
		if (hasLFO(com_mayaswell_spacegun_CControl_FLT_CUTOFF)<0 && hasEnv(com_mayaswell_spacegun_CControl_FLT_CUTOFF)<0) {
			setFilterSweepBounds(filterFrequency, filterEnvMod);
			filter.setFrequency(filterSweepMax);
		}
	} else if (was == com_mayaswell_spacegun_CControl_FLT_ENVMOD) {
		/* nb env mod can affect cutofff */
		if (hasLFO(com_mayaswell_spacegun_CControl_FLT_ENVMOD)<0 && hasEnv(com_mayaswell_spacegun_CControl_FLT_ENVMOD)<0) {
			setFilterSweepBounds(filterFrequency, filterEnvMod);
			filter.setFrequency(filterSweepMax);
		}
	} else if (was == com_mayaswell_spacegun_CControl_FLT_RESONANCE) {
		if (hasLFO(com_mayaswell_spacegun_CControl_FLT_RESONANCE)<0 && hasEnv(com_mayaswell_spacegun_CControl_FLT_RESONANCE)<0) {
			filter.setResonance(filterResonance);
		}
	} else if (was == com_mayaswell_spacegun_CControl_TUNE) {
		if (hasLFO(com_mayaswell_spacegun_CControl_TUNE)<0 && hasEnv(com_mayaswell_spacegun_CControl_TUNE)<0) {
			setTune(tune);
		}
	} else if (isLFOControl(was)) {
		int lw = lfo4Id(was);
		int pw = lfoParam4Id(was);
		if (pw == 0) {
			if (hasLFO(was) < 0 && hasEnv(was) < 0) {
				lfo[lw].resetRate();
			}
		}
	}
	return true;
}


void
CBassline::setGain(float g)
{
	gain = g;
}

void
CBassline::setPan(float p)
{
	pan = p;
}



void
CBassline::setTune(float t, float e, float l)
{
	float rt = (t/12)+e+l;
	tune = t;
	if (realTune != rt) {
		realTune = rt;
		/*
		psi = exp2_tune_psi[rt];//exp2(t/12);
		*/
	}
}

void
CBassline::setGlide(float g)
{
	// the oscilator glide factors are a function of sampleRate and updateRate,
	// such that srcLag = pow(glide,(44100./sr)*ur) destLag = 1.-srcLag
	if (glide != g) {
		srcLagFactor = pow(0.996+0.004*g,(44100./signalSampleRate)*maxFramesPerChunk);
		dstLagFactor = 1. - srcLagFactor;
		glide = g;
	}
}


bool
CBassline::setNSend(int n)
{
	nActiveSend = n;
	return true;
}

int
CBassline::countNActiveSend(int lastAdded)
{
//	__android_log_print(ANDROID_LOG_DEBUG, "CPadSample", "count send %d %d size %d", lastAdded, nActiveSend, send.size());
	if (lastAdded >= nActiveSend) {
		if (send[lastAdded].gain > 0 && !send[lastAdded].mute) {
			return lastAdded+1;
		}
	} else {
		int na = 0;
		for (int j=0; j<nActiveSend; j++) {
			if (send[j].gain > 0 && !send[j].mute) {
				na = j+1;
			}
		}
		return na;
	}
	return nActiveSend;
}

bool
CBassline::setSend(Bus *b, float g, bool m)
{
	int busid = b->id;
	while (busid >= send.size()) {
		send.push_back(Send());
	}
	send[busid].bus = b;
	send[busid].gain = g;
	send[busid].mute = m;
	nActiveSend = countNActiveSend(busid);
//	__android_log_print(ANDROID_LOG_DEBUG, "CPadSample", "set send %g %d done on %d bus %d, n %d", g, m, busid, id, nActiveSend);
	return true;
}

bool
CBassline::setSendGain(Bus *b, float g)
{
	int id = b->id;
	while (id >= send.size()) {
		send.push_back(Send());
	}
	send[id].bus = b;
	send[id].gain = g;
	nActiveSend = countNActiveSend(id);

//	__android_log_print(ANDROID_LOG_DEBUG, "CPadSample", "set send gain %g done on %d, n %d", g, id, nActiveSend);
	return true;
}

bool
CBassline::setSendMute(Bus *b, bool m)
{
	int id = b->id;
	while (id >= send.size()) {
		send.push_back(Send());
	}
	send[id].bus = b;
	send[id].mute = m;
	nActiveSend = countNActiveSend(id);
//	__android_log_print(ANDROID_LOG_DEBUG, "CPadSample", "set send gain %d done on %d, n %d", m, id, nActiveSend);
	return true;

}


void
CBassline::resetModulators()
{
	for (int i=0; i<lfo.size(); i++) {
		lfo[i].reset();
	}
	for (int i=0; i<env.size(); i++) {
		env[i].reset();
	}
}

void
CBassline::resetModulators(int modt)
{
	for (int i=0; i<lfo.size(); i++) {
		if (lfo[i].resetMode == modt) lfo[i].reset();
	}
	for (int i=0; i<env.size(); i++) {
		if (env[i].resetMode == modt) env[i].reset();
	}
}

void
CBassline::setPitchTable(std::vector<float> &pt) {
	pitchTable = pt;
}

void
CBassline::reloop() {
	resetModulators(com_mayaswell_audio_Modulator_RESET_ONLOOP);

}

void
CBassline::noteOff()
{
	oscMidiNote = -1;
}

void
CBassline::noteOn(short note, short velocity)
{
	oscMidiNote = note; /* + synthparams->oscilator.transpose*/
	oscTargetFrequency = pitchTable[note];

//	if (ampGate != AMPGATE_CLOSED || enableAmpEnv || !info->sequencerRecording)
	if (velocity > 0) {
		//if we are not tying on to the previous note...

		controlFrame = 0;
		filter.reset();

		//reset the envelopes
//		if (startNewNote || (oscVelocity == 0 && velocity != 0))
		oscVelocity = velocity;
		oscFrequency = oscTargetFrequency;
		resetModulators(com_mayaswell_audio_Modulator_RESET_ONATTACK);
/*
		if (waveform >= OSCILATOR_ADDON_BASE) {
			if (addonOsc.start_note) {
				addonOsc.start_note(addonOsc.cookie);
			}
		} else if (waveform == sample) {
			if (synthparams->oscilator.SamplePlayModeParam() != SAMPLE_PLAY_FEED) {
				sampleOscilator.StartNote(true);
			}
		} else if (waveform == triangle) {
			triangleOscilator.Phase(0);
		} else {
		}
*/
		oscilator.reset();
		filterEnvelope.reset();
/*
			if (velocity > OSC_VEL_0_GAIN) {
				filter_rezon_accent_kick = (((float)(velocity - OSC_VEL_0_GAIN))* RES_ACCENT_AMT/40.0);
//					Resonance(resonance+filter_rezon_accent_kick+resonanz_lfo_inc);
			} else {
				filter_rezon_accent_kick = 0;
//					Resonance(resonance+filter_rezon_accent_kick+resonanz_lfo_inc);
			}
*/

//		ampGate=AMPGATE_OPEN;
	}
}

/********************************************************
 * main generator hook
 ********************************************************/
inline void
CBassline::calculateControlsStereo(float &l, float &r)
{
	float p;
	float g;

	bool hasFreqMod = false;
	bool hasResMod = false;
	bool hasResEnv = false;
	bool hasPanEnv = false;
	bool hasGainEnv = false;
	hasTuneMod = false;
	std::vector<bool> hasLFORateMod(com_mayaswell_spacegun_SpaceGun_Global_LFO_PER_PAD, false);
	std::vector<bool> hasLFORateEnv(com_mayaswell_spacegun_SpaceGun_Global_LFO_PER_PAD, false);
//	__android_log_print(ANDROID_LOG_DEBUG, "CBassline", "calculateControlsStereo %d %d", nActiveLFO, nActiveEnv);

	lfoOffset.fill(0);
	envOffset.fill(1);

	for (int i=0; i<nActiveLFO; i++) {
		LFO &lf = lfo[i];

		float a = lf();
		for (int j=0; j<lf.nActiveSends; j++) {
			ModulationTarget&mt = lf.target[j];
			if (mt.target > 0 && mt.target <lfoOffset.size()) {
				float am = a*(mt.amount*envOffset[lfoTargetDepthId(i,j)]);
//				__android_log_print(ANDROID_LOG_DEBUG, "CBassline", "lfo %d %g", mt.target, am);
				if (mt.target == com_mayaswell_spacegun_CControl_FLT_RESONANCE) {
					hasResMod = true;
				} else if (mt.target == com_mayaswell_spacegun_CControl_FLT_CUTOFF
						|| mt.target == com_mayaswell_spacegun_CControl_FLT_ENVMOD) {
					hasFreqMod = true;
				} else if (mt.target == com_mayaswell_spacegun_CControl_TUNE) {
					hasTuneMod = true;
				} else if (isLFOControl(mt.target)) {
					int l = lfo4Id(mt.target);
					int p = lfoParam4Id(mt.target);
					if (p == 0) hasLFORateMod[l] = true;
//				} else if (isEnvControl(mt.target)) {
				}
				lfoOffset[mt.target] += am;

			}
		}
	}
//	__android_log_print(ANDROID_LOG_DEBUG, "CBassline", "done lfo %d %d", nActiveLFO, nActiveEnv);

	for (int i=0; i<nActiveEnv; i++) {
		Envelope & e = env[i];
		float a = e();
//		__android_log_print(ANDROID_LOG_DEBUG, "CBassline", "envelope %d %g %d", i, a, e.nActiveSends);
		for (int j=0; j<e.nActiveSends; j++) {
			ModulationTarget&mt = e.target[j];
			if (mt.target > 0 && mt.target <envOffset.size()) {
				float mta = mt.amount+lfoOffset[envTargetDepthId(i,j)];
				envOffset[mt.target] = (mt.amount < 0)?((a-1)*mta):(a*mta);
				if (mt.target == com_mayaswell_spacegun_CControl_FLT_RESONANCE) {
					hasResEnv = true;
				} else if (mt.target == com_mayaswell_spacegun_CControl_PAN) {
					hasPanEnv = true;
				} else if (mt.target == com_mayaswell_spacegun_CControl_GAIN) {
					hasGainEnv = true;
				} else if (mt.target == com_mayaswell_spacegun_CControl_FLT_CUTOFF
						|| mt.target == com_mayaswell_spacegun_CControl_FLT_ENVMOD) {
					hasFreqMod = true;
				} else if (mt.target == com_mayaswell_spacegun_CControl_TUNE) {
					envOffset[mt.target] -= mt.amount;
					hasTuneMod = true;
				} else if (isLFOControl(mt.target)) {
					int l = lfo4Id(mt.target);
					int p = lfoParam4Id(mt.target);
					if (p == 0) hasLFORateEnv[l] = true;
//				} else if (isEnvControl(mt.target)) {
				}
			}
		}
	}
//	__android_log_print(ANDROID_LOG_DEBUG, "CBassline", "done env %d %d", nActiveLFO, nActiveEnv);

	float	co = filter.frequency;
	if (hasFreqMod) { // apply lfo and recalculate the sweepMin and sweepMax
		setFilterSweepBounds(
				(filterFrequency+lfoOffset[com_mayaswell_spacegun_CControl_FLT_CUTOFF])
										* envOffset[com_mayaswell_spacegun_CControl_FLT_CUTOFF],
				(filterEnvMod+lfoOffset[com_mayaswell_spacegun_CControl_FLT_ENVMOD])
										* envOffset[com_mayaswell_spacegun_CControl_FLT_ENVMOD]);
	}
//	co = enableEnvelope?envelope.Endpoints(sweepMax,sweepMin)():sweepMax;
	if (enableFilterEnvelope) {
		co = filterEnvelope.setEndpoints(filterSweepMax, filterSweepMin)();
	} else {
		co = filterSweepMax;
	}

	if (co != filter.frequency) {
		filter.setFrequency(co);
	}

	if (hasResEnv) { // apply envelope and lfo and set resonancef
		filter.setResonance(filterResonance*envOffset[com_mayaswell_spacegun_CControl_FLT_RESONANCE]+lfoOffset[com_mayaswell_spacegun_CControl_FLT_RESONANCE]);
	} else if (hasResMod) { // apply envelope and lfo and set resonance
		filter.setResonance(filterResonance+lfoOffset[com_mayaswell_spacegun_CControl_FLT_RESONANCE]);
	}

	oscFrequency = oscFrequency * srcLagFactor + oscTargetFrequency * dstLagFactor;

	if (hasTuneMod) {
//		__android_log_print(ANDROID_LOG_DEBUG, "CBassline", "tune envelope %g %g %g", tune, envOffset[com_mayaswell_spacegun_CControl_TUNE], lfoOffset[com_mayaswell_spacegun_CControl_TUNE]);
//		setTune(tune, envOffset[com_mayaswell_spacegun_CControl_TUNE], lfoOffset[com_mayaswell_spacegun_CControl_TUNE]);
	}

	for (int i=0; i<nActiveLFO; i++) {
		if (hasLFORateMod[i] || hasLFORateEnv[i]) {
			float mod = 0;
			if (hasLFORateEnv[i]) {
				mod = LFO::logRmin * (1-envOffset[lfoRateId(i)]);
			}
			mod += lfoOffset[lfoRateId(i)];
			float emod = pow2_6[mod];//pow(2, mod);

//			__android_log_print(ANDROID_LOG_DEBUG, "CBassline", "lfo mod %g %g %g", mod, emod, pow(2, mod));
			lfo[i].modulate(emod);
		}
	}

//	__android_log_print(ANDROID_LOG_DEBUG, "CBassline", "adjusted filter %d %d", nActiveLFO, nActiveEnv);

	if (hasPanEnv) {
		p = pan*envOffset[com_mayaswell_spacegun_CControl_PAN] + lfoOffset[com_mayaswell_spacegun_CControl_PAN];
	} else {
		p = pan + lfoOffset[com_mayaswell_spacegun_CControl_PAN];
	}
	if (hasGainEnv) {
		g = gain*envOffset[com_mayaswell_spacegun_CControl_GAIN] + lfoOffset[com_mayaswell_spacegun_CControl_GAIN];
	} else {
		g = gain + lfoOffset[com_mayaswell_spacegun_CControl_GAIN];
	}
	if (p < -1) p = -1; else if (p > 1) p = 1;
	if (g < 0) g = 0; else if (g > 1) g = 1;
	r = rAmp = (g)*(1+p)/2;
	l = lAmp = (g)*(1-p)/2;
//	__android_log_print(ANDROID_LOG_DEBUG, "CBassline", "gain %g %g %g %g", gain, pan, r, l);
}


int
CBassline::playChunkMixer(BusMixer *mixer, const int buffInd, int nIterFrames, const int nGateFrames, const short nOutChannels)
{
	nIterFrames = playChunkReplacing(buffer, nIterFrames, nGateFrames, nOutChannels);

	if (send.size() == 0) {
		mixer->getBus(0)->mxn(buffer, buffInd, nIterFrames, 1);
	} else {
		for (int i=0; i<nActiveSend; i++) {
			Send &s = send[i];
			if (s.bus != NULL && s.gain > 0 && !s.mute) {
				s.bus->mxn(buffer, buffInd, nIterFrames, s.gain);
			}
		}
	}
	return nIterFrames;
}


/**
 *
 *
 * @param buff output buffer
 * @param nRequestedFrames optimal number of frames to play.
 * @param chunkData data etc for the chunk
 * @param chunkStartFrame
 * @param chunkNFrames
 * @param nextBufferStartL start of 'next' buffer
 * @param nextBufferStartR
 * @param direction >= 0 for forwards, else backwards
 */
inline int
CBassline::playChunkReplacing(float *buff, const int nRequestedFrames, int nGateFrames, const short nOutChannels)
{
	float *lSig = cycleBuffer;
	unsigned int buffI = 0;
	unsigned int nCycleFrames;
	unsigned int nControlChunkFrames;
	unsigned int nOutputFrames = 0;

	bool pitchShifting = (tune != 0 || hasTuneMod);

	__android_log_print(ANDROID_LOG_DEBUG, "CBassline", "playChunkReplacing %d requested %d gate frames", nRequestedFrames, nGateFrames);
	if (nOutChannels == 2) {
		float crAmp=rAmp;
		float clAmp=lAmp;
		nCycleFrames = (nGateFrames > 0)?nRequestedFrames:0; // we just check this egde case ... there isn't a 1:1 correspondence between requirements and data consumed
		while (nCycleFrames > 0) {
			if (controlFrame == 0) {
				calculateControlsStereo(clAmp, crAmp);
			}
			nControlChunkFrames = (controlFrame+nCycleFrames > framesPerControlCycle)? (framesPerControlCycle-controlFrame):nCycleFrames;
//			__android_log_print(ANDROID_LOG_DEBUG, "CBassline", "nControlChunkFrames %d", nControlChunkFrames);
			if (nControlChunkFrames > nGateFrames) {
				nCycleFrames = nControlChunkFrames = nGateFrames;
			}
//			__android_log_print(ANDROID_LOG_DEBUG, "CBassline", "nCycleFrames %d", nCycleFrames);
			oscilator(lSig, nControlChunkFrames);
			filter.apply(lSig, nControlChunkFrames);
//			__android_log_print(ANDROID_LOG_DEBUG, "CBassline", "applied filter %d %d %g %g %g", nControlChunkFrames, buffI, lSig[0], clAmp, crAmp);
			for (unsigned int i=0; i<nControlChunkFrames; i++, buffI+=2) {
				buff[buffI] = clAmp*lSig[i];
				buff[buffI+1] = crAmp*lSig[i];
			}
			if ((controlFrame += nControlChunkFrames) >= framesPerControlCycle) {
				controlFrame = 0; // -= framesPerControlCycle ... if it's > we have a mess where control cycle is skipped. so line above, nControlChunkFrames = .... etc
			}
			nCycleFrames -= nControlChunkFrames;
			nOutputFrames += nControlChunkFrames;
			nGateFrames -= nControlChunkFrames;
//			__android_log_print(ANDROID_LOG_DEBUG, "CBassline", "loopend %d %d %d", nCycleFrames, nOutputFrames, nGateFrames);
		}

		if (nGateFrames <= 0) {
			nCycleFrames = nRequestedFrames-nOutputFrames;
			while (nCycleFrames > 0) {
				if (controlFrame == 0) {
					calculateControlsStereo(clAmp, crAmp);
				}
				nControlChunkFrames = (controlFrame+nCycleFrames > framesPerControlCycle)? (framesPerControlCycle-controlFrame):nCycleFrames;
				memset(lSig, 0, nControlChunkFrames*sizeof(float));
				filter.apply(lSig, nControlChunkFrames);
				for (unsigned int i=0; i<nControlChunkFrames; i++, buffI+=2) {
					buff[buffI] = clAmp*lSig[i];
					buff[buffI+1] = crAmp*lSig[i];
				}
				if ((controlFrame += nControlChunkFrames) >= framesPerControlCycle) {
					controlFrame = 0;
				}
				nCycleFrames -= nControlChunkFrames;
				nOutputFrames += nControlChunkFrames;
			}
		}


	} else if (nOutChannels == 1) { // mono output ... an edge case we hope not to see
		float clAmp = lAmp;
		nCycleFrames = (nGateFrames > 0)?nRequestedFrames:0; // we just check this egde case ... there isn't a 1:1 correspondence between requirements and data consumed
		while (nCycleFrames > 0) {
			if (controlFrame == 0) {
				calculateControlsMono(clAmp);
			}
			nControlChunkFrames = (controlFrame+nCycleFrames > framesPerControlCycle)? (framesPerControlCycle-controlFrame):nCycleFrames;
			if (nControlChunkFrames > nGateFrames) {
				nCycleFrames = nControlChunkFrames = nGateFrames;
			}
			oscilator(lSig, nControlChunkFrames);
			filter.apply(lSig, nControlChunkFrames);
			for (unsigned int i=0; i<nControlChunkFrames; i++, buffI++) {
				buff[buffI] = clAmp*lSig[i];
			}
			if ((controlFrame += nControlChunkFrames) >= framesPerControlCycle) {
				controlFrame = 0;
			}
			nCycleFrames -= nControlChunkFrames;
			nOutputFrames += nControlChunkFrames;
			nGateFrames -= nControlChunkFrames;
		}
		if (nGateFrames <= 0) {
			nCycleFrames = nRequestedFrames-nOutputFrames;
			while (nCycleFrames > 0) {
				if (controlFrame == 0) {
					calculateControlsMono(clAmp);
				}
				nControlChunkFrames = (controlFrame+nCycleFrames > framesPerControlCycle)? (framesPerControlCycle-controlFrame):nCycleFrames;
				memset(lSig, 0, nControlChunkFrames*sizeof(float));
				filter.apply(lSig, nControlChunkFrames);
				for (unsigned int i=0; i<nControlChunkFrames; i++, buffI++) {
					buff[buffI] = clAmp*lSig[i];
				}
				if ((controlFrame += nControlChunkFrames) >= framesPerControlCycle) {
					controlFrame = 0;
				}
				nCycleFrames -= nControlChunkFrames;
				nOutputFrames += nControlChunkFrames;
			}
		}
	} else {
		// we would be exceedingly ambitious for a phone ;D
	}
	return nOutputFrames;
}

void
CBassline::setFltType(int t)
{
	if (t != filterType) {
		filterType = t;
		filter.setType(t);
		filter.reset();
		setFilterSweepBounds(filterFrequency, filterEnvMod);
//		__android_log_print(ANDROID_LOG_DEBUG, "CPadSample", "set filter %d freq %g %g sweep %g %g", filterType, filterFrequency, filterEnvMod, filterSweepMin, filterSweepMax);
		filter.setFrequency(filterSweepMax);
		filter.setResonance(filterResonance);
	}
}

void
CBassline::setFltFrequency(float f)
{
	if (f != filterFrequency) {
		filterFrequency = f;
		setFilterSweepBounds(filterFrequency, filterEnvMod);
		filter.setFrequency(filterSweepMax);
	}
}


void
CBassline::setFltEnvMod(float f)
{
	if (f != filterEnvMod) {
		filterEnvMod = f;
		setFilterSweepBounds(filterFrequency, filterEnvMod);
	}
}

void
CBassline::setFltResonance(float r)
{
	if (r != filterResonance) {
		filterResonance = r;
		filter.setResonance(r);
	}
}
void
CBassline::setFilter(int t, float f, float r, float e)
{
	filterType = t;
	filterResonance = r;
	filterFrequency = f;
	filterEnvMod = e;
	setFilterSweepBounds(f, e);
	filter.set(filterType, filterSweepMax, filterResonance);
}

void
CBassline::setFilterEnvAttT(float a)
{
	filterEnvelope.setAttackTime(a);
}

void
CBassline::setFilterEnvDecT(float a)
{
	filterEnvelope.setDecayTime(a);
}

void
CBassline::setFilterEnvHoldT(float a)
{
	filterEnvelope.setHoldTime(a);
}

void
CBassline::setFilterEnvAttack(float a)
{
	filterEnvelope.setAttackAlpha(a);
}

void
CBassline::setFilterEnvDecay(float a)
{
	filterEnvelope.setDecayAlpha(a);
}

void
CBassline::setFilterEnvelope(float a, float d, float at, float ht, float dt)
{

}


void
CBassline::setWaveform(int wf) {
	oscilator.setWaveform(wf);
}

void
CBassline::setWaveShape(float d) {
	oscilator.setShape(d);
}

void
CBassline::setTempo(float t)
{
	TimeSource::setTempo(t);
	for (int i=0; i<nActiveLFO; i++) {
		lfo[i].resetRate();
	}
}

package com.mayaswell.infernal.fragment;

import android.util.Log;

import com.mayaswell.audio.Controllable;
import com.mayaswell.infernal.Bassline;
import com.mayaswell.infernal.BasslineState;
import com.mayaswell.infernal.CControl;
import com.mayaswell.infernal.InfernalEngine;
import com.mayaswell.spacegun.fragment.MainControlsFragment;
import com.mayaswell.widget.InfinIntControl;
import com.mayaswell.widget.MenuControl;
import com.mayaswell.widget.MenuData;
import com.mayaswell.widget.StringControl;
import com.mayaswell.widget.TeaPot;

/**
 * Created by dak on 2/2/2016.
 */
public class SynthControlsFragment extends MainControlsFragment {
	public void setToSynth(Bassline bass) {
		if (bass == null || getView() == null) return;
		BasslineState s = bass.getState();
		/*
		if (loopCountControl != null) loopCountControl.setValue(s.loopCount);
		*/
		if (padLabelControl != null) padLabelControl.setValue(s.name);

/*
		if (padModeControl != null && mwbp.getPadModeAdapter() != null) {
			for (int i=0; i<mwbp.getPadModeAdapter().getCount(); i++) {
				MenuData<PadMode> d = mwbp.getPadModeAdapter().getItem(i);
				if (d.getMode() == s.padMode) {
					padModeControl.setSelection(i);
					break;
				}
			}
		}
		*/
		if (gainControl != null) gainControl.setValue(s.gain);
		if (panControl != null) panControl.setValue(s.pan);
		if (tuneControl != null) tuneControl.setValue(s.tune);
		if (filterEnvmodControl != null) filterEnvmodControl.setValue(s.filterEnvMod);
		if (filterCutoffControl != null) filterCutoffControl.setValue(s.filterFrequency);
		if (filterResonControl != null) filterResonControl.setValue(s.filterResonance);
		for (int i=0; i<Bassline.filtersAdapter.getCount(); i++) {
			Controllable.Filter d = Bassline.filtersAdapter.getItem(i);
			if (d.getId() == s.filterType) {
				filterSelect.setSelection(i);
				break;
			}
		}
		/*
		if (syncMasterSelect != null && bass.syncMasterAdapter != null) {
			boolean found = false;
			syncMasterSelect.setAdapter(bass.syncMasterAdapter);
			for (int i=0; i<bass.syncMasterAdapter.getCount(); i++) {
				PadSample.Info d = bass.syncMasterAdapter.getItem(i);
				if (d.padSample == s.syncMaster) {
					syncMasterSelect.setSelection(i);
					found = true;
					break;
				}
			}
			if (!found) {
				syncMasterSelect.setSelection(0);
			}
		}
		*/
	}

	protected Bassline getSelectedSynth()
	{
		if (mwbp instanceof InfernalEngine) {
			return ((InfernalEngine) mwbp).getSelectedSynth();
		}
		return null;

	}
	protected void setListeners() {
		if (padLabelControl != null) {
			padLabelControl.setValueListener(new StringControl.StringControlListener() {
				@Override
				public void onValueChanged(String v) {
					Bassline bs = getSelectedSynth();
					if (bs != null) {
						bs.state.name = v;
						/*
						if (bs.padButton != null) bs.padButton.invalidate();
						*/
					}
				}
			});
		}
/*
		if (padModeControl != null) {
			padModeControl.setAdapter(mwbp.getPadModeAdapter());
			padModeControl.setValueListener(new MenuControl.MenuControlListener() {
				@Override
				public void onValueChanged(int position) {
					Bassline bs = getSelectedSynth();
					if (bs != null) {
						MenuData<PadMode> d = mwbp.getPadModeAdapter().getItem(position);
						bs.state.padMode = d.getMode();
					}
				}
			});
		}
*/
		if (gainControl != null) gainControl.setPotListener(new TeaPot.TeaPotListener() {
			@Override
			public void onValueChanged(float v) {
				Bassline bs = getSelectedSynth();
				if (bs != null) {
					bs.setGain(v);
					mwbp.ccontrolUpdate(bs, CControl.GAIN, v, Controllable.Controller.INTERFACE);
				}
			}

		});

		if (panControl != null) panControl.setPotListener(new TeaPot.TeaPotListener() {
			@Override
			public void onValueChanged(float v) {
				Bassline bs = getSelectedSynth();
				if (bs != null) {
					bs.setPan(v);
					mwbp.ccontrolUpdate(bs, CControl.PAN, v, Controllable.Controller.INTERFACE);
				}
			}

		});

		/*
		if (loopCountControl != null) loopCountControl.setPotListener(new InfinIntControl.InfiniteLongListener() {
			@Override
			public void onValueChanged(long v) {
				Bassline bs = getSelectedSynth();
				if (bs != null) {
					bs.state.loopCount = (int) v;
				}
			}
		});*/

		if (tuneControl != null) {
			tuneControl.setValueBounds(-Controllable.TUNE_RANGE_SEMITONES, Controllable.TUNE_RANGE_SEMITONES);
			tuneControl.setPotListener(new TeaPot.TeaPotListener() {
				@Override
				public void onValueChanged(float v) {
					Bassline bs = getSelectedSynth();
					if (bs != null) {
						bs.setTune(v);
						mwbp.ccontrolUpdate(bs, CControl.TUNE, v, Controllable.Controller.INTERFACE);
					}
				}

			});
		}

		/*
		if (syncMasterSelect != null) {
			syncMasterSelect.setAdapter(null);
			syncMasterSelect.setValueListener(new MenuControl.MenuControlListener() {
				@Override
				public void onValueChanged(int position) {
					Bassline bs = getSelectedSynth();
					if (bs != null) {
						Object o=syncMasterSelect.getAdapter().getItem(position);
						Log.d("setup", String.format("%s adapter item %d", o.toString(), position));
						try {
							PadSample.Info s = (PadSample.Info) syncMasterSelect.getAdapter().getItem(position);
							bs.setSyncMaster(s.padSample);
						} catch (ClassCastException e) {
							syncMasterSelect.setSelection(0);
							bs.setSyncMaster(null);
						}
					}
				}
			});
		}
		*/

		if (filterSelect != null) {
			filterSelect.setAdapter(Bassline.filtersAdapter);
			filterSelect.setValueListener(new MenuControl.MenuControlListener() {
				@Override
				public void onValueChanged(int position) {
					Bassline bs = getSelectedSynth();
					if (bs != null) {
						Controllable.Filter d = Bassline.filtersAdapter.getItem(position);
						bs.setFilterType(d);
					}
				}
			});
		}

		if (filterCutoffControl != null) {
			filterCutoffControl.setPotListener(new TeaPot.TeaPotListener() {
				@Override
				public void onValueChanged(float v) {
					Bassline bs = getSelectedSynth();
					if (bs != null) {
						bs.setFilterFrequency(v);
						mwbp.ccontrolUpdate(bs, CControl.FLT_CUTOFF, v, Controllable.Controller.INTERFACE);
					}
				}

			});
		}

		if (filterEnvmodControl != null) {
			filterEnvmodControl.setPotListener(new TeaPot.TeaPotListener() {
				@Override
				public void onValueChanged(float v) {
					Bassline bs = getSelectedSynth();
					if (bs != null) {
						bs.setFilterEnvMod(v);
						mwbp.ccontrolUpdate(bs, CControl.FLT_ENVMOD, v, Controllable.Controller.INTERFACE);
					}
				}

			});
		}

		if (filterResonControl != null) {
			filterResonControl.setPotListener(new TeaPot.TeaPotListener() {
				@Override
				public void onValueChanged(float v) {
					Bassline bs = getSelectedSynth();
					if (bs != null) {
						bs.setFilterResonance(v);
						mwbp.ccontrolUpdate(bs, CControl.FLT_RESONANCE, v, Controllable.Controller.INTERFACE);
					}
				}

			});
		}
	}
}

package com.mayaswell.infernal;

import java.io.File;
import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;

import com.mayaswell.audio.AudioPlayer;
import com.mayaswell.audio.Bufferator;
import com.mayaswell.audio.file.AudioFile.Type;
import com.mayaswell.audio.AudioPlayer.State;
import com.mayaswell.audio.Bufferator.SampleChunkInfo;
import com.mayaswell.audio.Bufferator.SampleInfo;
import com.mayaswell.audio.SamplePlayer;
import com.mayaswell.audio.SamplePlayer.Manager;
import com.mayaswell.infernal.Pattern.Line;
import com.mayaswell.spacegun.BusMixer;
import com.mayaswell.spacegun.PadSample;

public class InfernalPlayer extends AudioPlayer implements SamplePlayer.Manager {
	public interface Listener {

	}
	
	protected Pattern pattern = null;

	protected ArrayList<IPadSample> padSample = null;
	protected ArrayList<Bassline> syn = null;

	protected int currentLoopFrame = 0;
	protected int currentStepFrame = 0;
	protected double currentBeat=0;
	protected int currentStep = 0;
	protected int currentLoop = 0;
	
	private InfernalEngine infernal;

	private boolean startExporting = false;
	private int exportCount = 0;
	private File exportFile = null;
	private boolean isExporting;

	public InfernalPlayer(InfernalEngine infernal, int maxDrum, int maxSynth) {
		playState = State.STOPPED;
		pattern = new Pattern();
		this.infernal = infernal;
		padSample = new ArrayList<IPadSample>();
		for (int i=0; i<maxDrum; i++) {
			padSample.add(new IPadSample(infernal, "drum "+i, i));
		}
		syn = new ArrayList<Bassline>();
		for (int i=0; i<maxSynth; i++) {
			syn.add(new Bassline("syn "+i, i));
		}
	}
	
	@Override
	public boolean isPlaying() {
		return playState == State.PLAYING;
	}
	
	public boolean fire() {
		if (playState != State.PLAYING) {
			resetPattern();
		}
		playState = State.PLAYING;
		return false;
	}

	@Override
	public boolean stop(boolean andTriggerEvent) {
		playState = State.PAUSED;
		return true;
	}
	
	/**
	 * lots of potential synchronization pitfalls
	 * @param p
	 */
	public void setPattern(Pattern p) {
		resetPattern();
	}
	
	public void resetPattern() {
		double framesPerBeat = infernal.getFramesPerBeat();
		double framesPerStep = framesPerBeat/pattern.stepsPerBeat;
		for (IPadSample p: padSample) {
			Pattern.Line pl = p.getPatternLine();
			p.stepFrames = 0;
			p.setStep(0, (int) framesPerStep);
			p.setLoopCount(0);
		}
		for (Bassline b: syn) {
			Pattern.Line pl = b.getPatternLine();
			b.stepFrames = 0;
			b.setStep(0, (int) framesPerStep);
			b.setLoopCount(0);
		}

	}

	/**
	 * current way of doing things ...
	 * @param busMixPointer
	 * @param nFrames
	 * @param nOutChannels
	 */
	public int playCumulative(long busMixPointer, int nFrames, short nOutChannels)
	{
		if (!isPlaying()) {
			return 0;
		}
		
		int nFramesOut = 0;
		int buffInd = 0;
		checkExportStart(0);
		
		int nActiveBus = 1; // lets keep bus 1 on for now
		while (nFramesOut < nFrames) {
			
			double framesPerBeat = infernal.getFramesPerBeat();
			double framesPerStep = framesPerBeat/pattern.stepsPerBeat;
			int nIterFrames = (int) (Math.ceil(framesPerStep))-currentStepFrame;
			if (nIterFrames > nFrames - nFramesOut) {
				nIterFrames = nFrames - nFramesOut;
			}
			
			for (IPadSample p: padSample) {
				if (p.isActive()) {
					Pattern.Line pl = p.getPatternLine();
					if (pl != null && pl.isRunningDecoupled()) {
						int nif = pl.framesPerStep(framesPerBeat)-p.stepFrames;
						if (nif > 0 && nif < nIterFrames) {
							nIterFrames = nif;
						}
					}
				}
			}
			if (nIterFrames == 0) {
				break;
			}

			for (IPadSample p: padSample) {
				if (p.isActive()) {
					int nfp = p.playCumulative(busMixPointer, buffInd, nIterFrames, nOutChannels);
					int pab = p.nActiveBus();
					if (pab > nActiveBus) {
						nActiveBus = pab;
					}

					Pattern.Line pl = p.getPatternLine();
					p.stepFrames += nfp;
					int sfb = pl.framesPerStep(framesPerBeat);
//					Log.d("player", String.format("nfp %d %d %d", p.stepFrames, sfb, nfp));
					if (p.stepFrames >= sfb) {
						int ns = p.getNextStep();
						Log.d("player", String.format("at step end %d", ns));
						if (ns == 0) {
							p.setLoopCount(p.getLoopCount()+1);
						}
						p.setStep(ns, (int) framesPerStep);
					} else {
//						Log.d("player", String.format("not at step end"));
					}

				}
			}
			
			for (Bassline b: syn) {
				if (b.isActive()) {
					int nfp = b.playCumulative(busMixPointer, buffInd, nIterFrames, nOutChannels);
					int pab = b.nActiveBus();
					if (pab > nActiveBus) {
						nActiveBus = pab;
					}

					Pattern.Line pl = b.getPatternLine();
					b.stepFrames += nfp;
					int sfb = pl.framesPerStep(framesPerBeat);
					Log.d("player", String.format("played: stepFrames %d framesPerStep %d nFramesPlayed %d", b.stepFrames, sfb, nfp));
					if (b.stepFrames >= sfb) {
						int ns = b.getNextStep();
						Log.d("player", String.format("at step end %d", ns));
						b.setStep(ns, (int) framesPerStep);
					} else {
//						Log.d("player", String.format("not at step end"));
					}

				}
			}

			currentStepFrame += nIterFrames;
			currentLoopFrame += nIterFrames;
			if (currentStepFrame >= framesPerStep) {
				currentStepFrame = 0;
				currentStep++;
				if (currentStep >= pattern.dfltNSteps()) {
					currentStep = 0;
					currentLoop++;
					
					for (IPadSample p: padSample) {
						if (p.isActive()) {
							if (needsSync(p.getPatternLine(), p.getLoopCount())) {
								p.setLoopCount(0);
			// we may be syncing twice here, though the cost isn't severe
								p.setStep(0, (int) framesPerStep);
							}
						}
					}
					for (Bassline b: syn) {
						if (b.isActive()) {
							if (needsSync(b.getPatternLine(), b.getLoopCount())) {
								b.setLoopCount(0);
								// we may be syncing twice here, though the cost isn't severe
								b.setStep(0, (int) framesPerStep);
							}
						}
					}


					if (isExporting) {
						if (--exportCount <= 0 || playState == State.STOPPED) {
							stopExport(buffInd/nOutChannels);
						}
					}		
//					onLoopEnd();
				}
			}
			currentBeat = (((float)currentStep)/pattern.stepsPerBeat)+(currentStepFrame/framesPerBeat)+currentLoop*pattern.nBeats;
		
			nFramesOut += nIterFrames;
			buffInd += nIterFrames * nOutChannels;
			
		}
		
		return nActiveBus;
	}
	
	/**
	 *  we need to sync if 
	 * @param pl
	 * @param loopCount
	 * @return
	 */
	private boolean needsSync(Pattern.Line pl, int loopCount) {
		if (pl.syncH == 0 || pl.syncL == 0 || pl.syncH == pl.syncL) {
			if (pl.nBeats() == pattern.nBeats) { // same tempo and length
				return true;
			}
			if (loopCount > 0 && (pl.nBeats() * currentLoop) % (pattern.nBeats * loopCount) == 0) {
				return true;
			}
			return false;
		}
		return loopCount > 0 && (pl.nBeats() * currentLoop * pl.syncH) % (pattern.nBeats * loopCount * pl.syncL) == 0;
	}

	public CharSequence getBeatCounterString() {
		return getBeatString(currentBeat+pattern.currentLoop*pattern.nBeats);
	}

	@SuppressLint("DefaultLocale")
	private String getBeatString(double p) {
		int nbar = (int) Math.floor(p / pattern.nBeats);
		double rbt = p % pattern.nBeats;
		int nbt = (int) Math.floor(rbt);
		rbt -= nbt;
		int bfr = (int) Math.floor(rbt*100);
		return String.format("%d:%d.%02d", nbar, nbt, bfr);
	}

	public void setBufSize(int size) {
		for (IPadSample p: padSample) {
			p.setBufsize(size);
		}
		for (Bassline p: syn) {
			p.setBufsize(size);
		}

	}

	public void setTempo(float t) {
		for (IPadSample ps: padSample) {
			ps.setTempo(t);
		}
		for (Bassline p: syn) {
			p.setTempo(t);
		}

	}
	
	protected void checkExportStart(int buffInd) {
		if (startExporting) {
			startExporting = false;
			startExport(0);
		}
	}

	private boolean startExport(int froffset) {
		isExporting = true;
//		orbital.getCurrentMixer().startExport(this, froffset, exportFile, AudioFileType.WAV_16_STEREO);
		return true;
	}
	
	private boolean stopExport(int froffset)
	{
		if (isExporting) {
//			orbital.getCurrentMixer().stopExport(this, froffset);
		}
		isExporting = false;
		return true;
	}
	
	public boolean exportLoops(File sff, int n) {
		if (isExporting) {
			return false;
		}
		isExporting = true;
		exportFile = sff;
		exportCount = n;
		startExporting = true;
		return true;
	}

	@Override
	public Context getContext() {
		return infernal;
	}

	@Override
	public int countSamplePlayers() {
		return padSample != null? padSample.size(): 0;
	}

	@Override
	public SamplePlayer getSamplePlayer(int i) {
		return padSample != null? padSample.get(i): null;
	}

	@Override
	public void startSamplePlayer(SamplePlayer p, boolean b) {
		if (p != null) {
//			startPadSample((PadSample)p, b);
		}
	}

	@Override
	public void stopSamplePlayer(SamplePlayer p) {
		if (p != null) {
//			stopPadSample((PadSample)p);
		}
	}

	@Override
	public float getSampleRate() {
		return infernal.getSampleRate();
	}

	public void setupControlsAdapter(InfernalEngine infernalEngine, int mwSpinnerItem, int mwSpinnerDropdownItem) {
		for (IPadSample p: padSample) {
			p.setupControlsAdapter(infernalEngine, mwSpinnerItem, mwSpinnerDropdownItem);
		}
	}

	public IPadSample getPad(int i) {
		return padSample != null && i>=0 && i<padSample.size()? padSample.get(i): null;
	}

	public Bassline getSynth(int i) {
		return syn != null && i>=0 && i<syn.size()? syn.get(i): null;
	}

	public int countPadSamples() {
		return padSample != null? padSample.size(): 0;
	}

	public int countSynths() {
		return syn != null? syn.size(): 0;
	}

	public void startPad(IPadSample p, boolean fromSync) {
	}

	public void stopPad(IPadSample selectedDrumPad) {
	}


}

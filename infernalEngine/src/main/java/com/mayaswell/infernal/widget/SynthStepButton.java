package com.mayaswell.infernal.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;

import com.mayaswell.infernal.R;
import com.mayaswell.widget.PadStepButton;

public class SynthStepButton extends PadStepButton {

	public static final int TIED = 3;
	private static final int[] STATE_TIED = {R.attr.state_tied};
	private boolean isTied = false;
	
	public SynthStepButton(Context context) {
		super(context);
		init(context, null, -1);
	}

	public SynthStepButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context, attrs, -1);
	}

	public SynthStepButton(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context, attrs, defStyle);
	}

	private void init(Context context, AttributeSet attrs, int defStyleAttr) {
		setBackgroundResource(R.drawable.mw_synstep_button);
		setLampResource(R.drawable.mw_synstep_lamp);
	}
	
	/** 
	 * @see android.widget.TextView#onCreateDrawableState(int)
	 */
	@Override
	protected int[] onCreateDrawableState(int extraSpace) {
		final int[] drawableState = super.onCreateDrawableState(extraSpace + 1);
		if (isTied) {
			mergeDrawableStates(drawableState, STATE_TIED);
		}
		return drawableState;
	}

	protected boolean onLongClickEvent(View v) {
		longClick = true;
		return false;
	}

	private boolean longClick = false;
	protected void touchResponse(MotionEvent me) {
		int action = me.getAction();
		if (action == MotionEvent.ACTION_DOWN) {
			longClick = false;
		} if(action == MotionEvent.ACTION_UP) {
			Log.d("button", String.format("up %b %b", isTied, isActive));
			if (longClick) {
				setTiedState(!isTied);
				isActive = isTied;
			} else {
				if (isTied) {
					isTied = false;
					isActive = true;
				} else if (isActive) {
					isActive = false;
				} else {
					isActive = true;
					isTied = false;
				}
			}
			if (isTied) {
				dispatchStateChange(TIED);
			} else if (isActive) {
				dispatchStateChange(ACTIVATED);
			} else {
				dispatchStateChange(DEACTIVATED);
			}
		}
	}
	
	/**
	 * @param s
	 */
	public void setTiedState(boolean s) {
		isTied = s;
		refreshDrawableState();
	}


}

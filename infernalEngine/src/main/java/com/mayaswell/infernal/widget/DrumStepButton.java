package com.mayaswell.infernal.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;

import com.mayaswell.infernal.R;
import com.mayaswell.widget.PadStepButton;

public class DrumStepButton extends PadStepButton {

	public DrumStepButton(Context context) {
		super(context);
		init(context, null, -1);
	}

	public DrumStepButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context, attrs, -1);
	}

	public DrumStepButton(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context, attrs, defStyle);
	}

	private void init(Context context, AttributeSet attrs, int defStyleAttr) {
		setBackgroundResource(R.drawable.mw_drumpad_button);
	}

}

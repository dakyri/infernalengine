LOCAL_PATH:= $(call my-dir)

CORE_AUDIO_BASE:=../../MayaswellCore/jni/audio
CORE_AUDIO_OBJ:=../../MayaswellCore/obj/local/armeabi
CPAD_BASE:=../../SpaceGunLib/jni
CPAD_OBJ:=../../SpaceGunLib/obj/local/armeabi

include $(CLEAR_VARS)
LOCAL_MODULE := cpad
LOCAL_SRC_FILES := $(CPAD_OBJ)/libcpad.so
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/$(CPAD_BASE)/include
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE            := audio
LOCAL_SRC_FILES         := $(CORE_AUDIO_OBJ)/libaudio.a
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)

LOCAL_MODULE    := cbass
LOCAL_CFLAGS    := -Werror -D__GXX_EXPERIMENTAL_CXX0X__
LOCAL_CPPFLAGS  := -std=c++11
LOCAL_SRC_FILES := cbass.cpp CBassline.cpp
LOCAL_LDLIBS    := -llog 
APP_STL := c++_static
LOCAL_STATIC_LIBRARIES := audio
LOCAL_SHARED_LIBRARIES := cpad
LOCAL_C_INCLUDES := $(LOCAL_PATH) $(LOCAL_PATH)/include $(LOCAL_PATH)/$(CORE_AUDIO_BASE)/include $(LOCAL_PATH)/$(CPAD_BASE)/ $(LOCAL_PATH)/$(CPAD_BASE)/include
#"C:/Android/android-ndk-r10/sources/cxx-stl/gnu-libstdc++/4.8/include"

include $(BUILD_SHARED_LIBRARY)
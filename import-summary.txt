ECLIPSE ANDROID PROJECT IMPORT SUMMARY
======================================

Manifest Merging:
-----------------
Your project uses libraries that provide manifests, and your Eclipse
project did not explicitly turn on manifest merging. In Android Gradle
projects, manifests are always merged (meaning that contents from your
libraries' manifests will be merged into the app manifest. If you had
manually copied contents from library manifests into your app manifest
you may need to remove these for the app to build correctly.

Ignored Files:
--------------
The following files were *not* copied into the new Gradle project; you
should evaluate whether these are still needed in your project and if
so manually move them:

From InfernalEngine:
* .gitignore
* ic_launcher-web.png
* proguard-project.txt
From MayaswellCore:
* .gitignore
From SpaceGunLib:
* .gitignore
* proguard-project.txt

Replaced Jars with Dependencies:
--------------------------------
The importer recognized the following .jar files as third party
libraries and replaced them with Gradle dependencies instead. This has
the advantage that more explicit version information is known, and the
libraries can be updated automatically. However, it is possible that
the .jar file in your project was of an older version than the
dependency we picked, which could render the project not compileable.
You can disable the jar replacement in the import wizard and try again:

android-support-v4.jar => com.android.support:support-v4:21.0.3

Moved Files:
------------
Android Gradle projects use a different directory structure than ADT
Eclipse projects. Here's how the projects were restructured:

In MayaswellCore:
* AndroidManifest.xml => mayaswellCore\src\main\AndroidManifest.xml
* jni\ => mayaswellCore\src\main\jni\
* res\ => mayaswellCore\src\main\res\
* src\ => mayaswellCore\src\main\java\
In SpaceGunLib:
* AndroidManifest.xml => spaceGunLib\src\main\AndroidManifest.xml
* assets\ => spaceGunLib\src\main\assets
* jni\ => spaceGunLib\src\main\jni\
* libs\armeabi\libcpad.so => spaceGunLib\src\main\jniLibs\armeabi\libcpad.so
* res\ => spaceGunLib\src\main\res\
* src\ => spaceGunLib\src\main\java\
In InfernalEngine:
* AndroidManifest.xml => infernalEngine\src\main\AndroidManifest.xml
* assets\ => infernalEngine\src\main\assets\
* jni\ => infernalEngine\src\main\jni\
* libs\armeabi\libcbass.so => infernalEngine\src\main\jniLibs\armeabi\libcbass.so
* res\ => infernalEngine\src\main\res\
* src\ => infernalEngine\src\main\java\

Next Steps:
-----------
You can now build the project. The Gradle project needs network
connectivity to download dependencies.

Bugs:
-----
If for some reason your project does not build, and you determine that
it is due to a bug or limitation of the Eclipse to Gradle importer,
please file a bug at http://b.android.com with category
Component-Tools.

(This import summary is for your information only, and can be deleted
after import once you are satisfied with the results.)

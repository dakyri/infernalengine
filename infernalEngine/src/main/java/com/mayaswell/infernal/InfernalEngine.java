package com.mayaswell.infernal;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.content.res.Resources.NotFoundException;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Environment;
import android.os.Looper;
import android.os.Message;
import android.preference.PreferenceManager;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnLongClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;

import com.mayaswell.audio.SamplePlayer;
import com.mayaswell.fragment.ModControlsFragment;
import com.mayaswell.infernal.Pattern.DrumLine;
import com.mayaswell.infernal.Pattern.Line;
import com.mayaswell.infernal.Pattern.SynthLine;
import com.mayaswell.infernal.R;
import com.mayaswell.infernal.fragment.PatternFragment;
import com.mayaswell.infernal.fragment.SynthControlsFragment;
import com.mayaswell.infernal.widget.DrumLineView;
import com.mayaswell.infernal.widget.LineView;
import com.mayaswell.infernal.widget.SynthLineView;
import com.mayaswell.audio.Bufferator.SampleInfo;
import com.mayaswell.audio.Controllable;
import com.mayaswell.audio.Controllable.ControllableInfo;
import com.mayaswell.audio.Controllable.ControllableManager;
import com.mayaswell.audio.Bufferator;
import com.mayaswell.audio.Envelope;
import com.mayaswell.audio.FX;
import com.mayaswell.audio.LFO;
import com.mayaswell.audio.Modulator.LFWave;
import com.mayaswell.audio.Modulator.ResetMode;
import com.mayaswell.audio.PadMode;
import com.mayaswell.util.ErrorLevel;
import com.mayaswell.util.MWBPActivity;
import com.mayaswell.widget.MenuData;
import com.mayaswell.widget.MultiModeButton;
import com.mayaswell.widget.PlayButton;
import com.mayaswell.spacegun.Bus;
import com.mayaswell.spacegun.BusMixer;
import com.mayaswell.spacegun.MWBPPadActivity;
import com.mayaswell.spacegun.PadSample;
import com.mayaswell.spacegun.PadSampleState;
import com.mayaswell.spacegun.fragment.BusControlsFragment;
import com.mayaswell.spacegun.fragment.FragmentHolder;
import com.mayaswell.spacegun.fragment.MainControlsFragment;
import com.mayaswell.spacegun.fragment.PadSampleEditorFragment;
import com.mayaswell.util.AbstractPatch.Action;
import com.mayaswell.util.EventID;
import com.mayaswell.util.SimpleFileChooser;
import com.mayaswell.widget.TeaPot;
import com.mayaswell.widget.TeaPot.TeaPotListener;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;

import rx.functions.Action1;

public class InfernalEngine extends MWBPPadActivity<Bank, Patch, CControl> implements ControllableManager {
	private Bufferator bufferator = null;

	public class Global {
		public static final int MAX_DRUM = 8;
		public static final int MAX_SYNTH = 4;
		public static final int LFO_PER_PAD = 4;
		public static final int MAX_LFO_TGT = 4;
		public static final int ENV_PER_PAD = 2;
		public static final int MAX_ENV_TGT = 4;
		public static final int MAX_BUS = 4;
		public static final int MAX_BUS_FX = 4;
		public static final int MAX_FX_PARAM = 6;
	}
	
	public static int[] regginbow = new int[8];

	protected RelativeLayout mainLayout = null;
	protected PlayButton playButton = null;
	protected TeaPot masterTempoControl = null;
	protected TextView beatCounter = null;

	protected PadSampleEditorFragment sampleEditorFragment = null;
	protected MainControlsFragment mainDrumControlsFragment = null;
	protected SynthControlsFragment mainSynthControlsFragment = null;
	protected ModControlsFragment<InfernalEngine> modControlsFragment = null;
	protected BusControlsFragment busControlsFragment = null;
	protected PatternFragment patternFragment = null;
	protected FragmentHolder fragmentHolder = null;
	

	protected IEAudioMixer mixer = null;
	private InfernalPlayer infernal = null;
	public BusMixer busMixer = null;

	private float masterTempo = 120;
	
	private SharedPreferences sgPreferences = null;
	private boolean keepScreenAlive = true;
	protected String defaultSampleSearchPath = null;

	private ArrayAdapter<ResetMode> rstModeAdapter;
	private ArrayAdapter<LFWave> lfoWaveAdapter;
	public static ArrayAdapter<FX> fxAdapter = null;
	public static ArrayList<FX> fxList = null;
	private static ArrayAdapter<ControllableInfo> controllableAdapter = null;

	public static MWBPPadActivity<?, ?, ?> ie;

	private Typeface digitalDream = null;

	protected boolean showSampleTimeBeats = false;
	protected IPadSample selectedDrumPad = null;
	protected Bassline selectedSynth = null;

	protected int currentPatternInd = -1;
	protected Pattern currentPattern = null;
	
	protected Spinner patternSelector = null;
	protected EditText patternNameEditView = null;
	protected ArrayAdapter<Action> patternSelectAdapter = null;

	public ArrayList<CControl> stepperCControls = null;

	public InfernalEngine() {
		super(".iex");
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Resources r = getResources();

		Drawable lamp;
		try {
			lamp = getResources().getDrawable(R.drawable.mw_synstep_lamp);
		} catch (NotFoundException e) {
			lamp = null;
		}

		regginbow[0] = r.getColor(R.color.ribbonBow1);
		regginbow[1] = r.getColor(R.color.ribbonBow2);
		regginbow[2] = r.getColor(R.color.ribbonBow3);
		regginbow[3] = r.getColor(R.color.ribbonBow4);
		regginbow[4] = r.getColor(R.color.ribbonBow5);
		regginbow[5] = r.getColor(R.color.ribbonBow6);
		regginbow[6] = r.getColor(R.color.ribbonBow7);
		regginbow[7] = r.getColor(R.color.ribbonBow8);		

		InfernalEngine.ie = this;
		
		infernal = new InfernalPlayer(this, Global.MAX_DRUM, 1/*Global.MAX_SYNTH*/);
//		infernal.setListener(new Listener() { });
		
		infernal.setupControlsAdapter(this, R.layout.mw_spinner_item, R.layout.mw_spinner_dropdown_item);
		PadSample.setupFiltersAdapter(this, R.layout.mw_spinner_item, R.layout.mw_spinner_dropdown_item);
		Bassline.setupFiltersAdapter(this, R.layout.mw_spinner_item, R.layout.mw_spinner_dropdown_item);

		Log.d("wrecker", "oncreate global adapters ..");
		rstModeAdapter = new ArrayAdapter<ResetMode>(this, R.layout.mw_spinner_item);
		rstModeAdapter.setDropDownViewResource(R.layout.mw_spinner_dropdown_item);
		rstModeAdapter.add(ResetMode.ONSLICE);		
		rstModeAdapter.add(ResetMode.ONLOOP);		
		rstModeAdapter.add(ResetMode.ONFIRE);		
		rstModeAdapter.add(ResetMode.ONATTACK);

		lfoWaveAdapter = new ArrayAdapter<LFWave>(this, R.layout.mw_spinner_item);
		lfoWaveAdapter.setDropDownViewResource(R.layout.mw_spinner_dropdown_item);
		lfoWaveAdapter.add(LFWave.SIN);
		lfoWaveAdapter.add(LFWave.SQUARE);
		lfoWaveAdapter.add(LFWave.SAW);
		lfoWaveAdapter.add(LFWave.EXP);

		/*
		playerSelectAdapter = new ArrayAdapter<MenuData<PlayMode>>(this, R.layout.mw_spinner_item);
		playerSelectAdapter.setDropDownViewResource(R.layout.mw_spinner_dropdown_item);

		viewSelectAdapter = new ArrayAdapter<ViewMode>(this, R.layout.mw_spinner_item);
		viewSelectAdapter.setDropDownViewResource(R.layout.mw_spinner_dropdown_item);
		viewSelectAdapter.add(ViewMode.MAIN);
		viewSelectAdapter.add(ViewMode.CHAOS);
		viewSelectAdapter.add(ViewMode.ALGORITHM);
*/
		fxList = defaultFX();
		if (fxAdapter == null) {
			fxAdapter = new ArrayAdapter<FX>(this, R.layout.mw_spinner_item);
			fxAdapter.setDropDownViewResource(R.layout.mw_spinner_dropdown_item);
			for (FX f: fxList) {
				fxAdapter.add(f);
			}
		}
		
		stepperCControls = new ArrayList<CControl>();
		stepperCControls.add(new CControl(CControl.NOTHING));
		stepperCControls.add(new CControl(CControl.GAIN));
		stepperCControls.add(new CControl(CControl.PAN));
		stepperCControls.add(new CControl(CControl.TUNE));
		stepperCControls.add(new CControl(CControl.LOOP_START));
		stepperCControls.add(new CControl(CControl.LOOP_LENGTH));
		stepperCControls.add(new CControl(CControl.FLT_CUTOFF));
		stepperCControls.add(new CControl(CControl.FLT_ENVMOD));
		stepperCControls.add(new CControl(CControl.FLT_RESONANCE));
		
		for (int i=0; i<CControl.maxBus; i++) {
			stepperCControls.add(new CControl(CControl.busSendId(i)));
		}
		
		for (int i=0; i<CControl.maxEnv; i++) {
			stepperCControls.add(new CControl(CControl.envAttackTimeId(i)));
			stepperCControls.add(new CControl(CControl.envDecayTimeId(i)));
			stepperCControls.add(new CControl(CControl.envSustainTimeId(i)));
			stepperCControls.add(new CControl(CControl.envSustainLevelId(i)));
			stepperCControls.add(new CControl(CControl.envReleaseTimeId(i)));
			for (int j=0; j<CControl.maxEnvTgt; j++) {
				stepperCControls.add(new CControl(CControl.envTargetDepthId(i, j)));
			}
		}
		for (int i=0; i<CControl.maxLFO; i++) {
			stepperCControls.add(new CControl(CControl.lfoRateId(i)));
//			stepperControlAdapter.add(new CControl(CControl.lfoPhaseId(i)));
			for (int j=0; j<CControl.maxLFOTgt; j++) {
//				stepperControlAdapter.add(new CControl(CControl.lfoTargetDepthId(i, j)));
			}
		}

		setContentView(R.layout.main);
		
		mainDrumControlsFragment = new MainControlsFragment();
		mainDrumControlsFragment.setLayoutResource(R.layout.main_drum_control_layout);
		mainSynthControlsFragment = new SynthControlsFragment();
		mainSynthControlsFragment.setLayoutResource(R.layout.main_synth_control_layout);
		sampleEditorFragment = new PadSampleEditorFragment();
		modControlsFragment = new ModControlsFragment<InfernalEngine>();
		busControlsFragment = new BusControlsFragment();
		patternFragment = (PatternFragment) getFragmentManager().findFragmentById(R.id.patternFragment);
		fragmentHolder  = (FragmentHolder) findViewById(R.id.operationalViews);

		digitalDream  =Typeface.createFromAsset(getAssets(), "fonts/DigitalDreamFatNarrow.ttf"); 
		
		Log.d("wrecker", "oncreate global ui componnents ..");
		patchSelector = (Spinner) findViewById(R.id.patchSelector);
		patchNameEditView = (EditText) findViewById(R.id.patchNameEditView);
		
		/*
		sampleEditorFragment = (SampleEditorFragment) getFragmentManager().findFragmentById(R.id.sampleEditorFragment);		
		sliceEditorFragment  = (SliceEditorFragment) getFragmentManager().findFragmentById(R.id.sliceEditorFragment);		
		modControlFragment   = (ModControlsFragment<OrbitalLaser>) getFragmentManager().findFragmentById(R.id.sliceModsFragment);		
		fxBusFragment = (FXBusFragment) getFragmentManager().findFragmentById(R.id.fxBusFragment);
		sequencerFragment = (SequencerFragment) getFragmentManager().findFragmentById(R.id.sequencerFragment);
//		playerFragment = (PlayerFragment) getFragmentManager().findFragmentById(R.id.playerFragment);
		*/
		mainLayout  = (RelativeLayout) findViewById(R.id.mainLayout);
		
		doPreferences();
		/*
		 *  create player and banks
		 */
		
		Log.d("wrecker", "oncreate global init ..");
		PadSample.globalCPadInit();
		Bassline.globalCBassInit();
		BusMixer.globalBmxInit();

		if (bufferator == null) {
			bufferator = Bufferator.init(infernal);
			bufferator.setSampleCacheSize(4);
			final MWBPActivity<?,?,?> activity = this;
			bufferator.monitorState(new Action1<Bufferator.BufferatorException>() {
				@Override
				public void call(Bufferator.BufferatorException e) {
					if (e.severity == ErrorLevel.FNF_ERROR_EVENT) {
						String si = e.getPath();
						/*
						bankSanitizer = BankSanitizer.getInstance();
						if (!BankSanitizer.isChecking()) {
							BankSanitizer.check(currentBank, activity, currentPatch, si);
						}*/
					} else {
						if (errorIsShowing) {
							Log.d("error", "skip formal display of error " + e.getMessage());
						} else {
							handleErrorMessage(e.getMessage(), e.severity);
						}
					}
				}
			});
			bufferator.monitorGfx(new Action1<SampleInfo>() {
				@Override
				public void call(SampleInfo sampleInfo) {
/*
					if (wreckage != null && wreckage.hasSample(sampleInfo)) {
						if (sampleEditorFragment != null) sampleEditorFragment.setSampleGfx4Pad(wreckage);
					}
					*/
				}

			});
		}

		bufferator.setSampleCacheSize(infernal.padSample.size());

		mixer = new IEAudioMixer();

		infernal.setBufSize(mixer.getOutBufsize());

		Log.d("creation", "done osc setup");
		busMixer = BusMixer.build(Global.MAX_BUS, Global.MAX_BUS_FX, Global.MAX_FX_PARAM, Global.LFO_PER_PAD, Global.MAX_LFO_TGT, mixer.getOutBufsize());
		
		Log.d("creation", "built bus global");
		controllableAdapter = new ArrayAdapter<ControllableInfo>(this, R.layout.mw_spinner_item);
		for (Bus b: busMixer.getBus()) {
			b.setupControlsAdapter(this, R.layout.mw_spinner_item, R.layout.mw_spinner_dropdown_item);
			controllableAdapter.add(new ControllableInfo(b, b.toString()));
		}
		busControlsFragment.setBusList(busMixer.getBus());
		
		/*
		 *  find all the main interface bits
		 */
		
		playButton  = (PlayButton) findViewById(R.id.playButton);
		playButton.setListener(new PlayButton.Listener() {

			@Override
			public void onStateChanged(MultiModeButton b, int mode) {
				PlayButton pb = null;
				try {
					pb = (PlayButton) b;
				} catch (ClassCastException e2) {
					
				}
				if (pb == null) {
					return;
				}
				switch (mode) {
				case MultiModeButton.ACTIVATED: {
					if (!infernal.isPlaying()) {
						startInfernal(pb);
					} 
					break;
				}

				case MultiModeButton.DEACTIVATED: {
					if (infernal.isPlaying()) {
						stopInfernal(pb);
					}
					break;
				}
				}
			}
			
		});
		
		masterTempoControl = (TeaPot) findViewById(R.id.masterTempoControl);
		masterTempoControl.setPotListener(new TeaPotListener() {
			@Override
			public void onValueChanged(float v) {
				Patch p;
				setTempo(v);
				if ((p=getCurrentPatch()) != null) {
//					p.setTempo(v);
				}
			}
		});
		masterTempoControl.setValue(masterTempo);
		
		beatCounter = (TextView) findViewById(R.id.beatCounter);
		if (beatCounter != null) {
			beatCounter.setText("0:0.00");
			if (digitalDream != null) {
				beatCounter.setTypeface(digitalDream);
			}
		}
		
		/*
		 * create default banks and stuff
		 */
		defaultBankFileName = r.getString(R.string.default_bank);	
		currentBank = loadBank(defaultBankFileName, true, false);
		if (currentBank == null) {
			currentBank = new Bank();
			saveBank(currentBank, defaultBankFileName, true);
		} else {
		}
		if (currentBank.patch.size() == 0) {
			currentBank.add(currentBank.newPatch());
		}
		setupPatchControls();
		
		if (currentPatternInd < 0) {
			setCurrentPatternInd(0);
		}
		if (currentPatchInd < 0) {
			setCurrentPatchInd(0);
		}
		if (currentPattern != null && currentPattern.row != null) {
			ArrayList<Line> r1 = currentPattern.row;
			if (r1.get(0) != null) {
				Log.d("crate", "gto "+r1.get(0).vel.length);
			}
		}
		View current = getCurrentFocus();
		if (current != null) current.clearFocus();
	}

	/**
	 * The activity is about to become visible.
	 */
	@Override
	protected void onStart()
	{		
//		if (fragmentSelector.countSelected() == 0) fragmentSelector.selectTab(0);
		mixer.addInfernal(infernal);
		mixer.play();
		Bufferator.run();
		super.onStart();
		startPeriodicUiUpdate();
	}
	
	/** 
	 * The activity has become visible (it is now "resumed").
	 */
   @Override
   protected void onResume()
   {
		super.onResume();
//		resumeSensors();
		startPeriodicUiUpdate();
   }
	
	/**
	 *  Another activity is taking focus (this activity is about to be "paused").
	 */
	@Override
	protected void onPause()
	{
		super.onPause();
//		pauseSensors();
		stopPeriodicUiUpdate();
	}
	
	/**
	 * The activity is no longer visible (it is now "stopped")
	 */
	@Override
	protected void onStop()
	{
		super.onStop();
//		pauseSensors();
		stopPeriodicUiUpdate();
	}
	
	/**
	 * The activity is about to be destroyed.
	 */
	@Override
	protected void onDestroy()
	{
		mixer.stop();// which will close it and force all the cleanups
		if (mixer.isRecording()) {
			mixer.stopRecording();
		}
//		sensorManager.unregisterListener(this);
		PadSample.globalCPadCleanup();
		BusMixer.globalBmxCleanup();
		Bufferator.stop();
		Bufferator.cleanup();
		Looper l = getMainLooper();
		super.onDestroy();
	}

	/**
	 * @see android.app.Activity#onBackPressed()
	 */
	@Override
	public void onBackPressed()
	{
		bailAlog("Save current bank before exit?");
//		super.onBackPressed();
	}
	

/**
 * other activity overrides
 */

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main, menu);
		return true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean onPrepareOptionsMenu (Menu menu)
	{
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		switch (item.getItemId()) {
		case R.id.ie_menu_new_patch: {
			newPattern();
//			if (busControlsFragment != null) {
//				Log.d("new pattern", "get select "+busControlsFragment.getSelectedId());
//			}
			return true;
		}
		case R.id.ie_menu_save_patch: {
			setCurrentPatchState(currentPatch);
			savePatch(currentPatchInd, currentPatch);
			return true;
		}
		case R.id.ie_menu_clone_patch: {
			branchPatch();
			return true;
		}
		case R.id.ie_menu_reload_patch: {
			reloadCurrentPatch();
			return true;
		}

		case R.id.ie_menu_del_patch: {
			delPatch();
			return true;
		}
		case  R.id.ie_menu_ren_patch: {
			renPatch();
			return true;
		}

		case R.id.ie_menu_new_pattern: {
			newPattern();
//			if (busControlsFragment != null) {
//				Log.d("new Pattern", "get select "+busControlsFragment.getSelectedId());
//			}
			return true;
		}
		case R.id.ie_menu_save_pattern: {
			setCurrentPatternState(currentPattern);
			savePattern(currentPatternInd, currentPattern);
			return true;
		}
		case R.id.ie_menu_branch_pattern: {
			branchPattern();
			return true;
		}
		case R.id.ie_menu_reload_pattern: {
			reloadCurrentPattern();
			return true;
		}

		case R.id.ie_menu_del_pattern: {
			delPattern();
			return true;
		}
		case  R.id.ie_menu_ren_pattern: {
			renPattern();
			return true;
		}

		case R.id.ie_menu_load_bank: {
			String [] fileFilter = {".*\\.sgx"};
			String startDir = getCurrentBankBaseDir();
			Intent request =new Intent(InfernalEngine.this, SimpleFileChooser.class);

			request.putExtra("startDir", startDir);
			request.putExtra("fileFilter", fileFilter);
			request.putExtra("showHidden", false);
			startActivityForResult(request,  R.id.ie_menu_load_bank);
			return true;
		}
		case R.id.ie_menu_new_bank: {
			currentBank = new Bank();
			if (currentBank.patch.size() == 0) {
				currentBank.add(currentBank.newPatch());
			}
			currentBankFile = null;
			setCurrentPatchInd(0);
			loadPatchDisplayUpdate();
			setCurrentPatternInd(0);
			return true;
		}
		case R.id.ie_menu_save_bank_and_state: {
			if (currentBankFile == null) {
				saveFialog("bank.sgx", null, false, true);
			} else {
				saveCurrentBank(false, true);
			}
			return true;
		}
		case R.id.ie_menu_save_bank: {
			if (currentBankFile == null) {
				saveFialog("bank.sgx", null, false, false);
			} else {
				saveCurrentBank(false, false);
			}
			return true;
		}
		case R.id.ie_menu_save_bank_as: {
			String dnm = null;
			if (currentBankFile != null) {
				dnm = currentBankFile.getName();
			} else {
				dnm = "bank.sgx";
			}
			saveFialog(dnm, null, false, false);
			return true;
		}
		case R.id.ie_menu_save_bank_2dflt: {
			if (currentBank != null) {
				setCurrentPatchState(getCurrentPatch());
				if (currentPatchInd >= 0 && currentPatchInd < currentBank.patch.size()) {
					currentBank.patch.set(currentPatchInd, getCurrentPatch());
				}
				saveBank(currentBank, defaultBankFileName, true);
			}
			return true;
		}
		case R.id.ie_menu_help: {
			showHelp();
			return true;
		}
		case R.id.ie_menu_about: {
			showAbout();
			return true;
		}
		case R.id.ie_menu_preferences: {
			Intent i = new Intent(this, IEPreferenceActivity.class);
			startActivityForResult(i, R.id.ie_menu_preferences);
			return true;
		}
		case R.id.ie_menu_exit: {
			bailAlog("Save current bank before exit?");
			return true;
		}
		default: {
//			Log.d("option", "Unhandled menu option"+Integer.toString(item.getItemId()));
		}
		}
		return super.onOptionsItemSelected(item);
	}


	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
//		Log.d("result", "got actity "+Integer.toString(requestCode));
		switch(requestCode) {
		
		case EventID.CHOOSE_FINDER: {
			if(resultCode == RESULT_OK) {
				String filePath = data.getExtras().getString("filePath");
				if (selectedDrumPad != null && filePath != null) {
//					Log.d("result", "got actity and all ok "+Integer.toString(editPadButton.getId()));
					selectedDrumPad.setSamplePath(filePath, true);
					if (sampleEditorFragment != null) {
						sampleEditorFragment.setSampleGfx4Pad(selectedDrumPad);
					}
					File fp = new File(filePath);
					if (fp.getParent() != null) {
						lastSampleFolder = fp.getParent();
					}
				}

			}
			break;
		}
		
/*		case SANITIZE_FINDER: {
			if(resultCode == RESULT_OK) {
				String filePath = data.getExtras().getString("filePath");
				if (filePath != null) {
					BankSanitizer.resume(filePath);
				}
			}
			break;
		}
*/	
		case R.id.ie_menu_load_bank: {
			if(resultCode == RESULT_OK) {
				String filePath = data.getExtras().getString("filePath");
				if (filePath != null) {
					currentBankFile = new File(filePath);
					currentBank = loadBank(currentBankFile);
					setCurrentPatchInd(0);
					loadPatchDisplayUpdate();
					setCurrentPatternInd(0);
				}
			}
			break;
		}
		
		case R.id.ie_menu_preferences: {
			doPreferences();
			break;
		}
		}
		
	}

	private void doPreferences()
	{
		sgPreferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
		keepScreenAlive  = sgPreferences.getBoolean("screenKeepAlive",true);
		defaultSampleSearchPath = sgPreferences.getString("defaultSampleSearchPath",null);
		if (keepScreenAlive) {
			getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		} else {
			getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onLowMemory ()
	{
		super.onLowMemory();
		Log.d("SpaceGun", String.format("on low memory"));
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onTrimMemory(int level)
	{
		super.onTrimMemory(level);
		Log.d("SpaceGun", String.format("on trim memory %d", level));
	}
	
	/**************************
	 * mwactivity overrides
	 **************************/
	@Override
	public Patch clonePatch(Patch p) {
		return p != null? p.clone():null;
	}

	@Override
	public ArrayAdapter<ResetMode> getLfoRstModeAdapter(LFO.Host h) {
		return rstModeAdapter;
	}

	@Override
	public ArrayAdapter<ResetMode> getEnvRstModeAdapter(Envelope.Host h) {
		return rstModeAdapter;
	}

	@Override
	public ArrayAdapter<LFWave> getLfoWaveAdapter(LFO.Host h) {
		return lfoWaveAdapter;
	}

	@Override
	public ArrayAdapter<FX> getFXAdapter(FX.Host h) {
		return fxAdapter;
	}

	@Override
	public ArrayAdapter<ControllableInfo> getControllableAdapter() {
		return null;
	}

	@Override
	public int lfoRateId(int xyci) {
		return CControl.lfoRateId(xyci);
	}

	@Override
	public int lfoPhaseId(int xyci) {
		return CControl.lfoPhaseId(xyci);
	}

	@Override
	public int lfoTargetDepthId(int xyci, int tid) {
		return CControl.lfoTargetDepthId(xyci, tid);
	}

	@Override
	public int envAttackTimeId(int xyci) {
		return CControl.envAttackTimeId(xyci);
	}

	@Override
	public int envDecayTimeId(int xyci) {
		return CControl.envDecayTimeId(xyci);
	}

	@Override
	public int envReleaseTimeId(int xyci) {
		return CControl.envReleaseTimeId(xyci);
	}

	@Override
	public int envSustainTimeId(int xyci) {
		return CControl.envSustainTimeId(xyci);
	}

	@Override
	public int envSustainLevelId(int xyci) {
		return CControl.envSustainLevelId(xyci);
	}

	@Override
	public int envTargetDepthId(int xyci, int tid) {
		return CControl.envTargetDepthId(xyci, tid);
	}

	@Override
	public boolean alwaysTempoLock() {
		return false;
	}

	/***************************************
	 * GETTERS, SETTTERS, FORGETTERS
	 ***************************************/
	
	public void setTempo(float t)
	{
		masterTempo = t;
		infernal.setTempo(t);
		BusMixer.setTempo(t);
	}
	
	/***************************************
	 * EVENT HANDLING
	 ***************************************/
	public void onPeriodicUiUpdate() {
//		if (sampleEditorFragment.isVisible() && wreckage != null && wreckage.isPlaying()) {
//			sampleEditorFragment.updateSampleCursor();
//		}
		
		if (beatCounter != null && infernal != null && infernal.isPlaying()) {
			beatCounter.setText(infernal.getBeatCounterString());
		}
	}
	
	/***************************************
	 * PATCH AND BANK, STORE AND LOAD
	 ***************************************/

	protected void setPatch(Patch p)
	{
		int i = 0;
		int j = 0;
//		Log.d("setPatch", String.format("pads %d %d name %s", padSample.size(), p.padState.size(), p.name));
		if (p == null) {
			return;
		}
		for (i=0; i<infernal.countPadSamples() && i<p.padState.size(); i++) {
			IPadSample ps = infernal.getPad(i);
			PadSampleState pss = p.padState.get(i);
			if (pss == null) break;
			ps.setState(pss);
//			PatchTempoControl.setValue(p.tempo);
			if (selectedDrumPad == ps) {
				setSubPanelsToPad(ps);
			}
		}
		for (j=0; j<infernal.countSynths() && j<p.synState.size(); j++) {
			Bassline bs = infernal.getSynth(j);
			BasslineState bss = p.synState.get(j);
			if (bss == null) break;
			bs.setState(bss);
//			PatchTempoControl.setValue(p.tempo);
			if (selectedSynth == bs) {
				setSubPanelsToSynth(bs);
			}
		}
		checkCurrentPatternLines();
		busMixer.setBusStates(p.busState);
//		setTempo(p.tempo);
		for (;i<infernal.countPadSamples(); i++) {
			IPadSample ps = infernal.getPad(i);
			ps.setState(new PadSampleState());
		}
		for (;j<infernal.countSynths(); j++) {
			Bassline bs = infernal.getSynth(j);
			bs.setState(new BasslineState());
		}
		if (busControlsFragment != null) {
			busControlsFragment.refreshDisplayedBusState();
		}
		loadPatchDisplayUpdate();
	}

	private boolean checkCurrentPatternLines() {
		if (currentPattern == null) {
			return false;
		}
		int i;
		int nps = infernal.countPadSamples();
		boolean ext = false;
		for (i=0; i<nps; i++) {
			DrumLine dl;
			dl = currentPattern.getDrumLine(i);
			if (dl == null) {
				dl = currentPattern.addDrumLine();
				ext = true;
			}
			IPadSample ps = infernal.getPad(i);
			ps.setPatternLine(dl);
		}
		int nsy = infernal.countSynths();
		for (i=0; i<nsy; i++) {
			SynthLine dl;
			dl = currentPattern.getSynthLine(i);
			if (dl == null) {
				dl = currentPattern.addSynthLine();
				ext = true;
			}
			Bassline ps = infernal.getSynth(i);
			ps.setPatternLine(dl);
		}
		if (currentPattern.countDrumLine() == 0) {
			currentPattern.addDrumLine();
			ext = true;
		}
		if (ext) {
			setSubPanelsToPattern(currentPatch, currentPattern);
		}
		return true;
	}

	private void setSubPanelsToPad(IPadSample pbs) {
		if (sampleEditorFragment != null) {
			sampleEditorFragment.setToPad(pbs);
		}
		if (mainDrumControlsFragment != null) {
			mainDrumControlsFragment.setToPad(pbs);
		}
		if (modControlsFragment != null) {
			modControlsFragment.setEnvHost(pbs);
			modControlsFragment.setLFOHost(pbs);
		}
		if (busControlsFragment != null) {
			busControlsFragment.setToPad(pbs);
		}
	}

	private void setSubPanelsToSynth(Bassline pbs) {
		if (sampleEditorFragment != null) {
//			sampleEditorFragment.setToPad(null);
		}
		if (mainSynthControlsFragment != null) {
			mainSynthControlsFragment.setToSynth(pbs);
		}
		if (modControlsFragment != null) {
			modControlsFragment.setEnvHost(pbs);
			modControlsFragment.setLFOHost(pbs);
		}
		if (busControlsFragment != null) {
			busControlsFragment.setToPad(pbs);
		}
	}

	private void setSubPanelsToPattern(Patch p, Pattern pat) {
		if (patternFragment != null) {
			patternFragment.setToPattern(p, pat);
		}
	}

	/**
	 *   this just updates the display in the spinner
	 * @param cpInd
	 */
	protected void setPatchSelector(int cpInd)
	{
		if (cpInd < 0 || cpInd >= currentBank.numPatches()) {
			cpInd = 0;
		}
		for (int i=0; i<patchSelectAdapter.getCount(); i++) {
			Action p = patchSelectAdapter.getItem(i);
			if (p.getOp() == cpInd) {
				patchSelector.setSelection(i);
				break;
			}
		}
	}
	
	protected void setCurrentPatchState(Patch p)
	{
		if (p == null) return;
		int i = 0;
		IPadSample ps;
		while ((ps=infernal.getPad(i)) != null) {
			if (i>=p.padState.size()) {
				p.padState.add(ps.state);
			} else {
				if (p.padState.get(i) != ps.state) {
					p.padState.remove(i);
					p.padState.add(i, ps.state);
				}
			}
			i++;
		}
		Bus b;
		i=0;
		while ((b=BusMixer.getBus(i)) != null) {
			Log.d("set Patch state", String.format("bus %d", i));
			if (i>=p.busState.size()) {
				p.busState.add(b.state);
			} else {
				if (p.busState.get(i) != b.state) {
					p.busState.remove(i);
					p.busState.add(i, b.state);
				}
			}
			i++;
		}
	}

	@Override
	protected File getStorageBaseDir() {
		File f = new File(Environment.getExternalStorageDirectory()+"/"+"mayaswell", "infernal");
		if (!f.exists()) {
			f.mkdirs();
			// also check noe and see if we should migrate
			File orig = getExternalFilesDir(null);
			Collection<File> existingContent = FileUtils.listFiles(orig, TrueFileFilter.INSTANCE, TrueFileFilter.INSTANCE);
			for (File oldFile: existingContent) {
				try {
					FileUtils.moveToDirectory(oldFile, f, false);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return f;
	}

	public Bank loadBank(String fnm, boolean internal, boolean qq)
	{
		FileInputStream fp;
		Bank b = null;
		try {
			if (internal) {
				fp = openFileInput(fnm);
				b = new Bank();
				b.load(fp);
				fp.close();
				b.setPatchAdapterItems(patchSelectAdapter);
//				if (automaticBankSanitize) BankSanitizer.check(b, this);
			} else {
			}
		} catch (FileNotFoundException e) {
			if (qq) {
				e.printStackTrace();
				mafFalog("File exception while loading bank "+e.getMessage());
			}
		} catch (IOException e) {
			if (qq) {
				e.printStackTrace();
				mafFalog("IO exception while loading bank "+e.getMessage());
			}
		}
		return b;
	}
	
	public Bank loadBank(File file)
	{
		FileInputStream fp;
		Bank b = currentBank;
		try {
			fp = new FileInputStream(file);
			b = new Bank();
			b.load(fp);
			b.setPatchAdapterItems(patchSelectAdapter);
			fp.close();
//			if (automaticBankSanitize) BankSanitizer.check(b, this);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			mafFalog("File exception while loading bank "+e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
			mafFalog("IO exception while loading bank "+e.getMessage());
		}
		return b;
	}
	
	
	private void loadPatchDisplayUpdate()
	{
		/*
		if (padButton != null) {
			for (PadButton pb: padButton) {
				pb.invalidate();
			}
		}
		if (ribbonController != null) {
			ribbonController.invalidate();
		}
		refreshTouchPad();
		if (selectedPadButton != null) {
			selectPad(selectedPadButton);
		}
		checkStartStopSensors();
		*/
	}

	protected boolean cleanVerifyCutAndRun(boolean andState)
	{
		if (currentBankFile != null) {
			saveCurrentBank(true, andState);
		} else {
			saveFialog("bank.sgx", null, true, andState);
		}
		return false;
	}


	protected void saveCurrentBank(final boolean andQuit, final boolean andPatch)
	{
		if (currentBankFile != null && currentBank != null) {
			if (andPatch && currentPatchInd >= 0 && currentPatchInd < currentBank.patch.size()) {
				setCurrentPatchState(currentBank.patch.get(currentPatchInd));
			}
			saveBank(currentBank, currentBankFile);
			if (andQuit) {
				cleanExit();
			}
		}
	}

	protected void cleanExit() {
		finish();
	}

	/**
	 *   selects a Patch, shove the info at the pads and assorted bits and force display updates
	 * @param cpInd
	 */
	public void setCurrentPatternInd(int cpInd)
	{

		if (currentBank == null || currentBank.numPatterns() == 0) {
			Log.d("MWBPActivity", "scpi: null or empty bank");
			currentPatternInd = -1;
			currentPattern = null;
			return;
		}
		Log.d("MWBPActivity", "set current pattern ind "+cpInd+", "+Integer.toString(currentBank.numPatterns()));
		if (cpInd < 0 || cpInd >= currentBank.numPatterns()) {
			cpInd = 0;
		}
		currentPattern  = currentBank.clonePattern(cpInd);
		currentPatternInd = cpInd;
		setPattern(currentPattern);
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				setPatternSelector(currentPatternInd);
			}
		});
	}
	
	protected void setPattern(Pattern p)
	{
		if (p == null) {
			Log.d("infernal", String.format("set pattern null"));
			setSubPanelsToPattern(null, null);
			return;
		}
		infernal.setPattern(p);
//		setTempo(p.tempo);
		final Pattern fp = p;
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				setSubPanelsToPattern(currentPatch, fp);
//				masterTempoControl.setValue(fp.tempo);
				loadPatternDisplayUpdate();
			}
		});
		
	}

	
	protected void newPattern() {
		if (currentBank != null) {
			int npInd = currentBank.add(new Pattern());
			if (npInd >= 0) {
				setCurrentPatternItems(currentBank);
				savePattern(currentPatternInd, currentPattern);
				setCurrentPatternInd(npInd);
			} else {
				setPatternSelector(currentPatternInd);
			}
		}
	}

	protected void delPattern() {
		if (currentBank != null) {
			if (currentBank.numPatterns() > 1) {
				if (currentPatternInd < 0) {
					currentPatternInd = 0;
				} else if (currentPatternInd >= currentBank.numPatterns()) {
					currentPatternInd = currentBank.numPatterns()-1;
				}
				int npInd = currentBank.deletePattern(currentPatternInd);
				if (npInd >= 0) {
					setCurrentPatternItems(currentBank);
					setCurrentPatternInd(npInd);
				} else {
					setPatternSelector(0);
				}
			} else {
				setPatternSelector(0);
			}
		}
	}

	protected void renPattern() {
		setPatternSelector(currentPatternInd);
		if (currentBank != null && currentBank.numPatterns() > 0) {
			try {
				Pattern p = currentBank.pattern.get(currentPatternInd);
				if (p != null) {
					patternNameEditView.setText(p.name);
					patternNameEditView.setVisibility(View.VISIBLE);
					patternNameEditView.requestFocus();
					InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.showSoftInput(patternNameEditView, 0);//InputMethodManager.SHOW_FORCED
				}
			} catch (IndexOutOfBoundsException e) {
				
			}

		}
	}


/**
 * clone: copies the working pattern, saves the current one, and selects the new one
 * !! the working Pattern is cloned from the bank, not a reference to it
 */
	protected void branchPattern() {
		if (currentBank != null) {
			if (currentPattern != null) {
				Log.d("branch", "cpi "+currentPattern.name+" "+currentPatternInd+currentBank.pattern.size());
				Pattern p = currentBank.clonePattern(currentPatternInd);
				p.name = currentBank.makeIncrementalPatternName(currentPattern.name);
				setCurrentPatternState(p);
				int npInd = currentBank.add(p);
				setCurrentPatternItems(currentBank);
//				savePattern(currentPatternInd, currentPattern);
				setCurrentPatternInd(npInd);
			}
		}
	}
	
	protected void reloadCurrentPattern() {
		if (currentBank != null) {
			setCurrentPatternInd(currentPatternInd);
		}
	}
	
	protected void setPatternSelector(int cpInd) {
		if (cpInd < 0 || cpInd >= currentBank.numPatterns()) {
			cpInd = 0;
		}
		for (int i=0; i<patternSelectAdapter.getCount(); i++) {
			Action p = patternSelectAdapter.getItem(i);
			if (p.getOp() == cpInd) {
				patternSelector.setSelection(i);
				break;
			}
		}
	}
	
	protected void setCurrentPatternItems(Bank b) {
		if (b != null && patchSelectAdapter != null) {
			b.setPatternAdapterItems(patchSelectAdapter);
		}
//		setSequenceStepAdapter();
	}
	public void savePattern(int ind, Pattern p)
	{
		if (currentBank != null && p != null && ind >= 0 && ind < currentBank.numPatches()) {
			currentBank.saveCopy(ind, p);
		}
	}

	public void setupPatchControls()
	{
		super.setupPatchControls();
		patternSelectAdapter = new ArrayAdapter<Action>(this, R.layout.mw_spinner_item);
		patternSelectAdapter.setDropDownViewResource(R.layout.mw_spinner_dropdown_item);
		if (currentBank != null) {
			setCurrentPatternItems(currentBank);
		}
		if (patternSelector != null) {
			patternSelector.setAdapter(patternSelectAdapter);
			patternSelector.setOnLongClickListener(new OnLongClickListener() {
	
				@Override
				public boolean onLongClick(View v) {
					renPatch();
					return true;
				}
				
			});
			
			patternSelector.setOnItemSelectedListener(new OnItemSelectedListener() {
				@Override
				public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
					Action b = patternSelectAdapter.getItem(position);
					if (currentBank != null) {
						int cbi = b.getOp(); // previously included things that are now menu ops
						if (cbi >= 0 && cbi < currentBank.numPatches()) {
							setCurrentPatchInd(cbi);
						}
					}
				}

				@Override
				public void onNothingSelected(AdapterView<?> parentView) {
				}
			});
		}

	}

	/**
	 * all the various display bits we should check when we load a new pattern
	 */
	private void loadPatternDisplayUpdate()
	{
		/*
		if (padButton != null) {
			for (PadButton pb: padButton) {
				pb.invalidate();
			}
		}
		if (ribbonController != null) {
			ribbonController.invalidate();
		}
		refreshTouchPad();
		if (selectedPadButton != null) {
			selectPad(selectedPadButton);
		}
		checkStartStopSensors();
		*/
	}

	/**
	 * set the given pattern to the current local active pattern
	 * @param p
	 */
	protected void setCurrentPatternState(Pattern p)
	{
		/*
		if (p == null) return;
		int i = 0;
		for (i=0; i<padSample.size(); i++) {
			PadSample ps = padSample.get(i);
			if (i>=p.padState.size()) {
				p.padState.add(ps.state);
			} else {
				if (p.padState.get(i) != ps.state) {
					p.padState.remove(i);
					p.padState.add(i, ps.state);
				}
			}
		}
		for (i=0; i<busMixer.bus.size(); i++) {
			Log.d("set patch state", String.format("bus %d", i));
			Bus b = busMixer.bus.get(i);
			if (i>=p.busState.size()) {
				p.busState.add(b.state);
			} else {
				if (p.busState.get(i) != b.state) {
					p.busState.remove(i);
					p.busState.add(i, b.state);
				}
			}
		}*/
	}
	
	/********************************************
	 * INTERFACE AND DIALOG HOOKS AND WRAPPERS
	 ********************************************/
	private void showHelp()
	{
		Builder d = new AlertDialog.Builder(this);
		d.setTitle(getResources().getString(R.string.help_title_general));
		d.setMessage(Html.fromHtml(getResources().getString(R.string.help_text_general)));
		d.setPositiveButton(aboutButtonText(), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				dialog.dismiss();
			}
		});
		d.show(); 
	}

	private void showAbout()
	{
		int pbi = (int) Math.floor(Math.random()*aboutButtonText.length);
		PackageInfo pInfo;
		String versnm = "0.0.1";
		try {
			pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
			versnm = pInfo.versionName;
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
		String appnm = getResources().getString(R.string.release_name);
//		String appniknm = getResources().getString(R.string.release_nickname);
		
		Builder d = new AlertDialog.Builder(this);
		d.setTitle(appnm+", "+versnm);
		d.setMessage(Html.fromHtml(getResources().getString(R.string.about_text)));
		d.setPositiveButton(aboutButtonText[pbi], new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				dialog.dismiss();
			}
		});
		d.show(); 
	}

	protected boolean startInfernal(PlayButton pb)
	{
//		Wreckage p = pb.getSample();
//		if (p.shouldStartPaused()) {
//			p.firePaused();
//			pb.setPausedState(p.isPaused());
//			return false;
//		} else {
			Log.d("infernal", "starting player");
			mixer.startInfernal(infernal);
			pb.setPlayState(infernal.isPlaying());
			return true;
//		}
	}
	
	protected boolean stopInfernal(PlayButton pb)
	{
		mixer.stopInfernal(infernal);
		pb.setPlayState(false);
		return true;
	}

	public double getFramesPerBeat() {
		return 60.0*IEAudioMixer.sampleRate/masterTempo;
	}

	@Override
	public Controllable findControllable(String dt, int di) {
		Log.d("spacegun", String.format("controllable %s %d", dt, di));
		if (dt.equals("pad")) {
			for (PadSample ps: infernal.padSample) {
				if (ps != null && ps.id == di) {
					return ps;
				}
			}
			if (di <= infernal.padSample.size() && di > 0) {
				return infernal.padSample.get(di-1);
			}
		} else if (dt.equals("bus")) {
			for (Bus ps: busMixer.getBus()) {
				if (ps != null && ps.getBusId() == di) {
					return ps;
				}
			}
			if (di <= busMixer.getBus().size() && di > 0) {
				return busMixer.getBus().get(di-1);
			}
		}
		return null;
	}

	@Override
	public PadSample getPadSample(int i) {
		if (i < infernal.padSample.size() && i >= 0)  {
			return infernal.padSample.get(i);
		}
		return null;
	}
	
	@Override
	public int countPadSamples() {
		return infernal.padSample.size();
	}

	@Override
	public float getSampleRate() {
		return IEAudioMixer.dfltSampleRate;
	}

	@Override
	public PadSample getSelectedPad() {
		return selectedDrumPad;
	}

	public Bassline getSelectedSynth() {
		return selectedSynth;
	}

	@Override
	public ArrayAdapter<MenuData<PadMode>> getPadModeAdapter() {
		return null;
	}

	@Override
	public float getTempo()
	{
		return masterTempo;
	}

	@Override
	public boolean getShowSampleTimeBeats() {
		return showSampleTimeBeats ;
	}

	@Override
	public void setShowSampleTimeBeats(boolean b) {
		showSampleTimeBeats = b;
	}

	@Override
	public BusMixer getBusMixer() {
		return busMixer;
	}

	@Override
	public PadSample padForId(int i) {
		return infernal != null? infernal.getPad(i): null;
	}

	public void selectLineView(LineView v) {
		if (v instanceof DrumLineView) {
			fragmentHolder.showFragment(sampleEditorFragment);
			fragmentHolder.showFragment(mainDrumControlsFragment);
			fragmentHolder.hideFragment(mainSynthControlsFragment);
			DrumLineView dlv = (DrumLineView) v;
			int id = dlv.getPadId();
			IPadSample s = infernal.getPad(id);
			selectedDrumPad = s;
			setSubPanelsToPad(s);
		} else if (v instanceof SynthLineView) {
			fragmentHolder.hideFragment(sampleEditorFragment);
			fragmentHolder.hideFragment(mainDrumControlsFragment);
			fragmentHolder.showFragment(mainSynthControlsFragment);
			SynthLineView slv = (SynthLineView) v;
			int id = slv.getSynthId();
			Bassline b = infernal.getSynth(id);
			selectedSynth = b;
			setSubPanelsToSynth(b);
		}
	}

	@Override
	public void startSamplePlayer(SamplePlayer p, boolean fromSync) {
		infernal.startPad((IPadSample)p, fromSync);
	}

	@Override
	public void stopSamplePlayer(SamplePlayer p) {
		infernal.stopPad((IPadSample)p);
	}

	@Override
	public Context getContext() {
		return this;
	}


	@Override
	public int countSamplePlayers() {
		return 1;
	}

	@Override
	public SamplePlayer getSamplePlayer(int i) {
		return infernal.getSamplePlayer(i) ;
	}



}

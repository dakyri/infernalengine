package com.mayaswell.infernal.widget;

import java.util.ArrayList;

import com.mayaswell.audio.Control;
import com.mayaswell.audio.Controllable.XYControlAssign;
import com.mayaswell.infernal.Pattern.Stepper;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.util.AttributeSet;
import android.util.Log;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;

public class StepperValuesView extends View {

	final float densityMultiplier = getContext().getResources().getDisplayMetrics().density;
	final float textScaledPx = 10 * densityMultiplier;
	private int padW = 0;
	private int padH = 0;
	
	private int nSteps = 1;
	private Paint textBrush = null;
	private Paint gridBrush = null;
	private Paint valBrush = null;
	private Stepper currentStepper = null;

	public StepperValuesView(Context context) {
		super(context);
		init(context, null, -1);
	}

	public StepperValuesView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context, attrs, -1);
	}

	public StepperValuesView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		init(context, attrs, defStyleAttr);
	}

	private void init(Context context, AttributeSet attrs, int defStyleAttr) {
		padW = getWidth(); 
		padH = getHeight();

		textBrush = new Paint(Paint.ANTI_ALIAS_FLAG);
		textBrush.setColor(0xdd4285F4);
		textBrush.setTextSize(textScaledPx);
		textBrush.setTextAlign(Align.CENTER);
		
		gridBrush = new Paint(Paint.ANTI_ALIAS_FLAG);
		gridBrush.setColor(0xaa333333);

		valBrush = new Paint(Paint.ANTI_ALIAS_FLAG);
		valBrush.setColor(0xaaaa4444);
	}

	public void setStepper(Stepper s) {
		currentStepper  = s;
		invalidate();
	}

	public void setLine(LineView lv) {
		if (lv == null) {
			nSteps = 1;
		} else {
			padW = lv.lineStepButtons.getMeasuredWidth();
			getLayoutParams().width = padW;
			nSteps = lv.countButtons();
			if (nSteps < 1) nSteps = 1;
		}
		invalidate();
	}

	@Override
	public boolean onDragEvent (DragEvent event)
	{
		return super.onDragEvent(event);
	}
	
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		int widthMode = MeasureSpec.getMode(widthMeasureSpec);
		int widthSize = MeasureSpec.getSize(widthMeasureSpec);
		int heightMode = MeasureSpec.getMode(heightMeasureSpec);
		int heightSize = MeasureSpec.getSize(heightMeasureSpec);
//		Log.d("touch", String.format("onMeaure %s %s", View.MeasureSpec.toString(widthMeasureSpec), View.MeasureSpec.toString(heightMeasureSpec)));
		int h = heightSize;
		int w = widthSize;
//		if (h > w) {
//			h = w;
//		} else if (w > h) {
//			w = h;
//		}
		setMeasuredDimension(w, h);
	}
	
	@Override
	public boolean onTouchEvent (MotionEvent e)
	{
		if (currentStepper == null) {
			return false;
		}
		/*
		if (SpaceGun.sg != null && SpaceGun.sg.saveSelectButton != null && (SpaceGun.sg.saveSelectButton.isPressed() || SpaceGun.sg.saveSelectButton.getPrimedState())) {
			SpaceGun.sg.startTouchpadEdit();
			clearXYCATrack(null);
			return false;
		}*/
		int action = e.getActionMasked();
		ArrayList<XYControlAssign> tracked = new ArrayList<XYControlAssign>();
		int sx = getScrollX();
		for (int i=0; i<e.getPointerCount(); i++) {
			float x = e.getX(i);
			float y = e.getY(i);
			
			float stepW = padW/nSteps;
			short stepInd = (short) ((sx+x)/stepW);
			float pny = 1-(y/padH);
			if (pny < 0) pny = 0; else if (pny > 1) pny = 1;
			if (stepInd >= 0 && stepInd < nSteps) {
				currentStepper.setNormalizedVal(stepInd, pny);
				invalidate((int)(stepInd*stepW), 0, (int)((stepInd+1)*stepW), padH);
			}
/*
			XYControlAssign xyp = findXYCA(e.getPointerId(i), pnx, pny);
			if (xyp != null) {
				invalidate(xyp.l, xyp.t, xyp.r, xyp.b);
				xyp.setNormalizedValue(pnx, pny);
				SpaceGun.sg.ccontrolUpdate(xyp.controllable, xyp.xp.ccontrolId(), xyp.x, Controllable.Controller.TOUCHPAD);
				SpaceGun.sg.ccontrolUpdate(xyp.controllable, xyp.yp.ccontrolId(), xyp.y, Controllable.Controller.TOUCHPAD);
				tracked.add(xyp);
			}
*/
		}

		if (action == MotionEvent.ACTION_UP) {
//			clearXYCATrack(null);
		} else {
//			clearXYCATrack(tracked);
		}
//		invalidate();
		return true;
	}
	
	protected void onDraw (Canvas canvas)
	{
/** TODO still not sure if these should be view width or canvas width */
		if (padW == 0) {
			padW = getMeasuredWidth();//canvas.getWidth();
		}
		if (padH == 0) {
			padH = canvas.getHeight();
		}
		
		float geW = ((float)padW) / nSteps;
		float geH = ((float)padH);
		float gridX = geW;
		float gridY = geH;
		if (currentStepper != null) {
			for (int i=0; i<nSteps; i++) {
				float v = currentStepper.getNormalizedVal(i);
				canvas.drawRect(gridX-geW, (1-v)*geH, gridX, geH, valBrush);
				gridX += geW;
			}
		}
		gridX = geW;
		for (int i=0; i<nSteps-1; i++) {
			canvas.drawLine(gridX, 0, gridX, padH, gridBrush);
			gridX += geW;
		}
		/*
		int cl = 15;
		Patch p = null;
		if (SpaceGun.sg != null) p = SpaceGun.sg.getCurrentPatch();
		if (p != null) {
			for (XYControlAssign xyp: p.touchAssign) {
				if (xyp != null && xyp.isValid()) {
					xyp.refreshNormalizedValue();
					float dnx = xyp.nx*padW;
					float dny = (1-xyp.ny)*padH;
					canvas.drawLine(dnx-cl, dny, dnx+cl, dny, pointBrush);
					canvas.drawLine(dnx, dny-cl, dnx, dny+cl, pointBrush);
					String xls = xyp.labelString();
					String xvs = xyp.valueString();
//					Log.d("touch", xls+" "+xvs);
					float xlsl = textBrush.measureText(xls)/2;
					float xvsl = textBrush.measureText(xvs)/2;
					float mlvsl = (xlsl>xvsl?xlsl:xvsl);
					
					xyp.l = (int) (dnx - xlsl);
					xyp.r = (int) (dnx + xlsl);
					xyp.t = (int) (dny - xlsl);
					xyp.b = (int) (dny + xvsl);

					float ylsl = textBrush.descent()-textBrush.ascent();
					float yvsl = textBrush.getFontSpacing();
					
					if (dnx-mlvsl<0)
						dnx = mlvsl;
					else if (dnx+mlvsl>padW)
						dnx = padW-mlvsl;
					if (dny-ylsl < 0)
						dny = ylsl;
					else if (dny + yvsl > padH)
						dny = padH - yvsl;
					canvas.drawText(xls, dnx, dny-textBrush.descent(), textBrush);
					dny += textBrush.getFontSpacing();
					canvas.drawText(xvs, dnx, dny, textBrush);
				}
			}
		}*/
	}

	public Stepper getStepper() {
		return currentStepper;
	}
	

}

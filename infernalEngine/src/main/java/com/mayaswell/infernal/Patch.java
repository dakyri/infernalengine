package com.mayaswell.infernal;
import java.util.ArrayList;

import com.mayaswell.spacegun.BusState;
import com.mayaswell.spacegun.PadSampleState;
import com.mayaswell.util.AbstractPatch;


public class Patch extends AbstractPatch {
	public ArrayList<PadSampleState> padState = null;
	public ArrayList<BusState> busState = null;
	public ArrayList<BasslineState> synState = null;
	
	public Patch() {
		this("", false);
	}
	
	public Patch(String name, boolean andInit) {
		this.name = name;
		padState = new ArrayList<PadSampleState>();
		busState = new ArrayList<BusState>();		
		synState = new ArrayList<BasslineState>();		
	}

	public Patch clone() {
		Patch p = new Patch(name, false);
		for (PadSampleState pss: padState) {
			if (pss != null) {
				p.padState.add(pss.clone());
			}
		}
		for (BusState pss: busState) {
			if (pss != null) {
				p.busState.add(pss.clone());
			}
		}
		for (BasslineState pss: synState) {
			if (pss != null) {
				p.synState.add(pss.clone());
			}
		}
		return p;
	}

	public boolean add(PadSampleState pss)
	{
		return padState.add(pss);
	}
	
	public boolean add(BusState pss)
	{
		return busState.add(pss);
	}

	public boolean add(BasslineState pss)
	{
		return synState.add(pss);
	}


}

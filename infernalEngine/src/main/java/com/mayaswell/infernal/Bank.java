package com.mayaswell.infernal;

import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Matcher;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

import android.util.Log;
import android.util.Xml;
import android.widget.ArrayAdapter;

import com.mayaswell.audio.FX;
import com.mayaswell.spacegun.BusState;
import com.mayaswell.spacegun.PadBank;
import com.mayaswell.spacegun.PadSampleState;
import com.mayaswell.util.AbstractPatch.Action;
import com.mayaswell.util.NamedPatch;

public class Bank extends PadBank<Patch, CControl> {

	protected ArrayList<Pattern> pattern;
	
	public Bank() {
		this(true);
	}
	
	public Bank(boolean andInit) {
		super();
		pattern = new ArrayList<Pattern>();
		if (andInit) {
			Pattern p = new Pattern();
			p.addDrumLine();
			p.addSynthLine();
			pattern.add(p);
		}
	}
	
	@Override
	public Patch newPatch() {
		return new Patch();
	}

	@Override
	public boolean save(FileOutputStream fp) {
		XmlSerializer xml = Xml.newSerializer();
		try {
			FileWriter fw = new FileWriter(fp.getFD());
			xml.setOutput(fw);
			xml.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true);
			xml.startDocument("UTF-8", true);
			xs(xml, "bank");
			sa(xml, "source", "infernal");
			la(xml, "version", 1);
			for (Patch p: patch) {
				xs(xml, "patch");
				sa(xml, "name", p.name);
				for (PadSampleState pss: p.padState) {
					putPad(xml, pss);
				}
				for (BusState b: p.busState) {
					putBus(xml, b);
				}
				xe(xml, "patch");
			}
			xe(xml, "bank");
			xml.endDocument();
			fw.close();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
			return false;
		} catch (IllegalStateException e) {
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	protected boolean doBank(XmlPullParser xpp) throws XmlPullParserException, IOException {
		String tag = xpp.getName();
		if (tag == null || !tag.equals("bank")) return false;
		
		if (xpp.isEmptyElementTag()) {
			return true;
		}
		
		int na=xpp.getAttributeCount();
		String source = null;
		for (int i=0; i<na; i++) {
			String n = xpp.getAttributeName(i);
			String v = xpp.getAttributeValue(i);
			if (n.equals("source")){
				source = v;
			}
		}
		if (source == null || !source.equals("spacegun")) {
			return false;
		}
		Patch p = null;
		int eventType = xpp.getEventType();
		while (eventType != XmlPullParser.END_DOCUMENT) {
			if(eventType == XmlPullParser.START_TAG) {
				if ((p=doPatch(xpp)) != null) {
					patch.add(p);
				}
			} else if(eventType == XmlPullParser.END_TAG) {
				tag = xpp.getName();
				if (tag.equals("bank")) { // this will be consumed by the 'next()' call in the current iteration of the loop of load() 
					return true;
				}
			} else if(eventType == XmlPullParser.TEXT) {
			}
			eventType = xpp.next();
		}
		throw new XmlPullParserException("Unexpected document end in xml for 'bank'");
	}

	/**
	 * @param xpp
	 * @return
	 * @throws XmlPullParserException
	 * @throws IOException
	 */
	private Patch doPatch(XmlPullParser xpp) throws XmlPullParserException, IOException
	{
		String tag = xpp.getName();
		if (tag == null || !tag.equals("patch")) return null;
		
		Patch p = new Patch();
		PadSampleState pss = null;
		BusState bss = null;
		int na=xpp.getAttributeCount();
		for (int i=0; i<na; i++) {
			String n = xpp.getAttributeName(i);
			String v = xpp.getAttributeValue(i);
			if (n.equals("name")){
				p.name = v;
			} else {
//				Log.d("bank error", "Unknown attribute in 'patch' element, "+n);
//				throw new XmlPullParserException("Unknown attribute in patch element, "+n);
			}
		}
		
//		Log.d("do patch", String.format("got patch name %s", p.name));
		
		if (xpp.isEmptyElementTag()) {
			return p;
		}
		
		int eventType = xpp.getEventType();
		while (eventType != XmlPullParser.END_DOCUMENT) {
			if(eventType == XmlPullParser.START_TAG) {
				if ((pss=doPad(xpp, InfernalEngine.ie)) != null) {
					p.add(pss);
				} else if ((bss=doBus(xpp)) != null) {
					p.add(bss);
				} else {
					Log.d("bank", String.format("unexpected start tag %s", xpp.getName()));
				}
			} else if(eventType == XmlPullParser.END_TAG) {
				tag = xpp.getName();
				if (tag.equals("patch")) { // tag consumed in loop of doBank()
					return p;
				}
			} else if(eventType == XmlPullParser.TEXT) {
			}
			eventType = xpp.next();
		}
		throw new XmlPullParserException("Unexpected document end in xml for 'patch'");
	}
	
	@Override
	protected CControl createControl(String s) {
		return new CControl(s);
	}

	@Override
	protected ArrayList<FX> fxList() {
		return InfernalEngine.fxList;
	}

	@Override
	public Patch clonePatch(int i) {
		Patch p = get(i);
		return (p!=null)? p.clone(): null;
	}

	@Override
	public Patch saveCopy(int i, Patch p) {
		return set(i, p.clone());
	}

	public int numPatterns() {
		return pattern != null? pattern.size(): 0;
	}

	public String makeIncrementalPatternName(String name) {
		java.util.regex.Pattern p = java.util.regex.Pattern.compile("(\\D*)(\\d*)");
		Matcher m = p.matcher(name);
		if (m.matches()) {
			String base = "default";
			int n = 0;
			if (m.group(1) != null && m.group(1).length() > 0) {
				base = m.group(1);
			}
			if (m.group(2) != null && m.group(2).length() > 0) {
				n = Integer.parseInt(m.group(2));
			}
			boolean found;
			String nnm;
			do {
				n++;
				nnm = base+Integer.toString(n);
				found = false;
				for (NamedPatch pp: pattern) {
					if (pp.name.equals(nnm)) {
						found = true;
						break;
					}
				}
			} while (found);
			return nnm;
		} else {
			return "default";
		}
	}

	/**
	 * adds the pattern and returns it's index
	 * @param p
	 * @return
	 */
	public int add(Pattern p) {
		pattern.add(p);
		return pattern.size()-1;
	}
	
	public Pattern clonePattern(int i) {
		Pattern p = pattern.get(i);
		return (p!=null)? p.clone(): null;
	}

	
	public int deletePattern(int i) {
		if (i >= 0 && i<pattern.size()) {
			pattern.remove(i);
			return i < pattern.size()? i: pattern.size()-1;
		}
		return -1;
	}

	public Pattern saveCopy(int i, Pattern p) {
		if (i >= 0 && i < pattern.size()) {
			return pattern.set(i,  p.clone());
		}
		return p;
	}

	public boolean setPatternAdapterItems(ArrayAdapter<Action> a) {
		if (a == null) return false;
		a.clear();
		int i=0;
		for (Pattern p: pattern) {
			a.add(new Action(i++, p.name));
		}
		return true;
	}

}

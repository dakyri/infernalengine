/*
 * CBassline.h
 *
 *  Created on: Nov 10, 2015
 *      Author: dak
 */

#ifndef CBASSLINE_H_
#define CBASSLINE_H_

#include <TimeSource.h>
#include <LFO.h>
#include <Envelope.h>
#include <ExpEnvelope.h>

#include "Oscilator.h"
#include "Filter.h"
#include "Send.h"

#include "com_mayaswell_spacegun_CControl.h"
#include "com_mayaswell_infernal_CControl.h"

#include "ControlDefs.h"
#include "BusMixer.h"

#undef __cplusplus
#define __cplusplus 201103L

#include <vector>
#include <array>

class CBassline: public TimeSource, public LFOHost, public EnvelopeHost, public FilterHost {
public:
	CBassline(short _id, int maxEnv, int maxEnvTgt, int maxLfo, int maxLfoTgt);
	virtual ~CBassline();

	void setBufsize(int bufsize);

	void setGain(float g);
	void setPan(float p);
	void setTune(float t, float e=0, float l=0);
	void setWaveform(int wf);
	void setWaveShape(float d);

	void setFltType(int t);
	void setFltFrequency(float f);
	void setFltResonance(float r);
	void setFltEnvMod(float e);
	void setFilter(int t, float f, float r, float e);

	void setFilterEnvEnable(bool e);
	void setFilterEnvAttT(float a);
	void setFilterEnvDecT(float a);
	void setFilterEnvHoldT(float a);
	void setFilterEnvAttack(float a);
	void setFilterEnvDecay(float a);
	void setFilterEnvelope(float a, float d, float at, float ht, float dt);

	bool setNSend(int n);
	bool setSend(Bus *b, float g, bool m);
	bool setSendGain(Bus *b, float g);
	bool setSendMute(Bus *b, bool m);

	void setGlide(float g);

	void setTempo(float t);

	bool setLFOTarget(int which, int whichn, int cc, float amt);
	bool setEnvelopeTarget(int which, int whichn, int cc, float amt);

	void setPitchTable(std::vector<float> &pt);

	void noteOn(short note, short velocity);
	void noteOff();
	void reloop();

	void resetModulators(int mode);
	void resetModulators();

	int playChunkMixer(BusMixer *mixer, const int buffInd, int nIterFrames, const int nGateFrames, const short nOutChannels);

	short id;
	float gain;
	float pan;

protected:
	int playChunkReplacing(float *buff, const int nIterFrames, int nGateFrames, const short nOutChannels);

	void calculateControlsMono(float &l);
	void calculateControlsStereo(float &l, float &r);

	int countNActiveSend(int lastAdded);

	Filter filter;
	Oscilator oscilator;
	ExpEnvelope filterEnvelope;

	unsigned int controlFrame;
	float *buffer;
	float *cycleBuffer;

	float lAmp;
	float rAmp;

	float tune;
	float realTune;
	bool hasTuneMod;

	float glide;
	float srcLagFactor;
	float dstLagFactor;
	float oscTargetFrequency;
	float oscFrequency;
	short oscMidiNote;
	short oscVelocity;

	std::vector<float> &pitchTable;

	std::array<float, lfoIdBase(com_mayaswell_spacegun_SpaceGun_Global_LFO_PER_PAD)> lfoOffset;
	std::array<float, lfoIdBase(com_mayaswell_spacegun_SpaceGun_Global_LFO_PER_PAD)> envOffset;

	bool enableFilterEnvelope;

	std::vector<Send> send;
public:
	int nActiveSend;
};

extern BoundedLookup pow2_6;


#endif /* CBASSLINE_H_ */

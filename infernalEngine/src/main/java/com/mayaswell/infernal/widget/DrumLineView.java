package com.mayaswell.infernal.widget;

import java.util.ArrayList;

import com.mayaswell.infernal.Pattern.DrumLine;
import com.mayaswell.infernal.Pattern.Line;
import com.mayaswell.infernal.R;
import com.mayaswell.spacegun.PadSampleState;
import com.mayaswell.widget.MultiModeButton;
import com.mayaswell.widget.PadStepButton;
import com.mayaswell.widget.XorizontalScrollView;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class DrumLineView extends LineView {
	
	public interface Listener {
		void onPadHit(DrumLineView v, DrumLine dl, int drid, int stepid, boolean hit);
		void onScrollChanged(DrumLineView v, int l, int t);
	}
	
	protected ArrayList<DrumStepButton> dsb = new ArrayList<DrumStepButton>();
	protected DrumStepButton lastDSB = null;
	private DrumLine currentLine = null;
	private int currentId = -1;
	private XorizontalScrollView scroller;

	public DrumLineView(Context context) {
		super(context);
		init(context, null, -1);
	}

	public DrumLineView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context, attrs, -1);
	}

	public DrumLineView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		init(context, attrs, defStyleAttr);
	}

	private void init(Context context, AttributeSet attrs, int defStyleAttr) {
		LayoutInflater li = (LayoutInflater)context.getSystemService (Context.LAYOUT_INFLATER_SERVICE);
		li.inflate (R.layout.drum_line_view, this, true);
		lineStepButtons  = (StepButtonsView) findViewById(R.id.drumStepButtons);
		lineDestLabel  = (TextView) findViewById(R.id.lineDestLabel);
		lineSyncLabel  = (TextView) findViewById(R.id.lineSyncLabel);
		scroller = (XorizontalScrollView) findViewById(R.id.drumStepButtonsScroller);
		final DrumLineView v = this;
		scroller.setListener(new XorizontalScrollView.Listener() {
			@Override
			public void onScrollChanged(int l, int t, int oldl, int oldt) {
				if (listener != null) {
					listener.onScrollChanged(v, l,t);
				}
			}
		});
	}

	public void setTo(int id, PadSampleState pss, DrumLine dl) {
		lineStepButtons.removeAllViews();
		currentLine  = dl;
		currentId  = id;
		lastDSB = null;
		if (pss != null && !pss.name.equals("")) {
			lineDestLabel.setText(id+" "+pss.name);
		} else {
			lineDestLabel.setText("Drum "+id);
		}
		for (int i=0; i<dl.getLength(); i++) {
			DrumStepButton db = getStepButton(i);
			db.setId(i+1);
			db.invalidate();
			RelativeLayout.LayoutParams rlp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			if (lastDSB == null) {
				rlp.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
			} else {
				rlp.addRule(RelativeLayout.RIGHT_OF, lastDSB.getId());
			}
			rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP);
			rlp.rightMargin = 1;
			lastDSB = db;
			lineStepButtons.addView(db, rlp);
		}
	}
	
	protected DrumStepButton getStepButton(int bi) {
		DrumStepButton b = null;
		for (DrumStepButton pv: dsb) {
			if (pv.getParent() == null) {
				b = pv;
				break;
			}
		}
		if (b == null) {
			b = new DrumStepButton(getContext());
			b.setLampResource(R.drawable.mw_synstep_lamp);
			dsb.add(b);
		}
		b.setStepId(bi);
		final DrumLineView v = this;
		b.setListener(new DrumStepButton.Listener() {

			@Override
			public void onStateChanged(PadStepButton b, int mode) {
				Log.d("drumlineview", String.format("drum step %d %d", b.id, mode));
				if (listener != null) {
					listener.onPadHit(v, currentLine, currentId, b.id, mode == PadStepButton.ACTIVATED);
				}
			}
			
		});
		return b;
	}
	
	private Listener listener = null;
	
	public void setListener(Listener l) {
		listener = l;
	}

	public int getPadId() {
		return currentId;
	}

	@Override
	public Line getLine() {
		return currentLine;
	}

}

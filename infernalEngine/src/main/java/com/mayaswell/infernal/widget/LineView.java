package com.mayaswell.infernal.widget;

import com.mayaswell.infernal.Pattern.Line;
import com.mayaswell.infernal.R;
import com.mayaswell.infernal.widget.DrumLineView.Listener;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewParent;
import android.view.View.OnClickListener;
import android.widget.RelativeLayout;
import android.widget.TextView;


public abstract class LineView extends RelativeLayout {
	
	protected StepButtonsView lineStepButtons;
	protected TextView lineDestLabel;
	protected TextView lineSyncLabel;
	
	private static final int[] STATE_MINIMIZED = {R.attr.state_minimized};
	
	public enum ViewMode {
		NORMAL, MINIMIZED;
	}
	
	protected ViewMode viewMode;

	public LineView(Context context) {
		super(context);
		setup(context, null, -1);
	}

	public LineView(Context context, AttributeSet attrs) {
		super(context, attrs);
		setup(context, attrs, -1);
	}

	public LineView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		setup(context, attrs, defStyleAttr);
	}

	private void setup(Context context, AttributeSet attrs, int defStyleAttr) {
		lineStepButtons = null;
		lineDestLabel = null;
		lineSyncLabel = null;

		setPadding(5, 5, 5, 5);
		setBackgroundResource(R.drawable.line_view_bg);

		setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (v.getParent() instanceof PatternLinesView && v instanceof LineView) {
					LineView lv = (LineView) v;
					if (!lv.isMinimized()) {
						((PatternLinesView)v.getParent()).select((LineView)v);
					} else {
						((PatternLinesView)v.getParent()).open((LineView)v);
					}
				}
			}
			
		});
		
		this.setLongClickable(true);
		this.setOnLongClickListener(new OnLongClickListener() {
			@Override
			public boolean onLongClick(View v) {
				setViewMode(ViewMode.MINIMIZED, true);
				return true;
			}	
		});

	}
	
	protected boolean isMinimized() {
		return viewMode == ViewMode.MINIMIZED;
	}

	public void setViewMode(ViewMode m, boolean doUpdate) {
		if (m == viewMode) {
			return;
		}
		viewMode = m;
		boolean updateLayout = false;
		switch(viewMode) {
		case MINIMIZED:
			if (lineStepButtons != null) {
				lineStepButtons.setVisibility(INVISIBLE);
				updateLayout = true;
			}
			break;
		case NORMAL:
			if (lineStepButtons != null) {
				lineStepButtons.setVisibility(VISIBLE);
				updateLayout = true;
			}
			break;
		}
		if (updateLayout) {
			ViewParent v = getParent();
			if (v instanceof PatternLinesView) {
				((PatternLinesView)v).layoutChildren();
			}
		}
		refreshDrawableState();
	}
	public ViewMode getViewMode() {
		return viewMode;
	}
	/** 
	 * @see android.widget.TextView#onCreateDrawableState(int)
	 */
	@Override
	protected int[] onCreateDrawableState(int extraSpace) {
		final int[] drawableState = super.onCreateDrawableState(extraSpace + 1);
		if (viewMode == ViewMode.MINIMIZED) {
			mergeDrawableStates(drawableState, STATE_MINIMIZED);
		}
		return drawableState;
	}

	/** 
	 * @see android.view.View#drawableStateChanged(int)
	 */
	@Override
	protected void drawableStateChanged() {
		super.drawableStateChanged();
	}
	
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		/*

		Log.d("line view", String.format("onMeaure %s %d -> %s %s", lineDestLabel.getText(), getMeasuredWidth(), View.MeasureSpec.toString(widthMeasureSpec), View.MeasureSpec.toString(heightMeasureSpec)));
		int wm = MeasureSpec.getMode(widthMeasureSpec);
		int w = MeasureSpec.getSize(widthMeasureSpec);
		int hm = MeasureSpec.getMode(heightMeasureSpec);
		int h = MeasureSpec.getSize(heightMeasureSpec);

	
		if (w < getChildCount()*200) {
			w = getChildCount()*200;
		}
		setMeasuredDimension(w, h);
		*/
	}

	public int getPreferredWidth() { /* wrap_content is preferable but doesn't work dynamically */
		int w;
		if (viewMode == ViewMode.MINIMIZED) {
			if ((w = lineDestLabel.getRight()) == 0) {
				return /* LayoutParams.WRAP_CONTENT*/ getContext().getResources().getDimensionPixelSize(R.dimen.minimizedLineViewWidth);
			}
		} else {
			if ((w = lineStepButtons.getRight()) == 0) {
				return LayoutParams.WRAP_CONTENT;
			}
		}
		RelativeLayout.LayoutParams rko;
		try {
			rko = (LayoutParams) getLayoutParams();
		} catch (ClassCastException e) {
			return w+4;
		}
		return w+rko.leftMargin+rko.rightMargin+4;
	}

	public abstract Line getLine();

	public int countButtons() {
		return lineStepButtons.getChildCount();
	}
}

package com.mayaswell.infernal.widget;

import java.util.ArrayList;

import com.mayaswell.infernal.BasslineState;
import com.mayaswell.infernal.R;
import com.mayaswell.infernal.Pattern.DrumLine;
import com.mayaswell.infernal.Pattern.Line;
import com.mayaswell.infernal.Pattern.SynthLine;
import com.mayaswell.infernal.widget.DrumLineView.Listener;
import com.mayaswell.spacegun.PadSampleState;
import com.mayaswell.widget.PadStepButton;
import com.mayaswell.widget.XorizontalScrollView;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.RelativeLayout.LayoutParams;

public class SynthLineView extends LineView {

	public interface Listener {
		void onPadHit(SynthLineView v, SynthLine dl, int syid, int stepid, int mode);
		void onScrollChanged(SynthLineView v, int l, int t);
	}
	
	protected ArrayList<SynthStepButton> ssb = new ArrayList<SynthStepButton>();
	private SynthLine currentLine = null;
	private XorizontalScrollView scroller;
	private int currentId = -1;
	private SynthStepButton lastDSB = null;
	
	public SynthLineView(Context context) {
		super(context);
		init(context, null, -1);
	}

	public SynthLineView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context, attrs, -1);
	}

	public SynthLineView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		init(context, attrs, defStyleAttr);
	}
	
	private void init(Context context, AttributeSet attrs, int defStyleAttr) {
		LayoutInflater li = (LayoutInflater)context.getSystemService (Context.LAYOUT_INFLATER_SERVICE);
		li.inflate (R.layout.synth_line_view, this, true);
		lineStepButtons  = (StepButtonsView) findViewById(R.id.synthStepButtons);
		lineDestLabel  = (TextView) findViewById(R.id.lineDestLabel);
		lineSyncLabel  = (TextView) findViewById(R.id.lineSyncLabel);
		scroller = (XorizontalScrollView) findViewById(R.id.synthStepButtonsScroller);
		final SynthLineView v = this;
		if (scroller != null) {
			scroller.setListener(new XorizontalScrollView.Listener() {
				@Override
				public void onScrollChanged(int l, int t, int oldl, int oldt) {
					if (listener != null) {
						listener.onScrollChanged(v, l,t);
					}
				}
			});
		}
	}

	public void setTo(int id, BasslineState pss, SynthLine dl) {
		lineStepButtons.removeAllViews();
		currentLine  = dl;
		currentId  = id;
		lastDSB = null;
		if (pss != null && !pss.name.equals("")) {
			lineDestLabel.setText(id+" "+pss.name);
		} else {
			lineDestLabel.setText("Synth "+id);
		}
		for (int i=0; i<dl.getLength(); i++) {
			SynthStepButton db = getStepButton(i);
			db.setId(i+1);
			db.invalidate();
			RelativeLayout.LayoutParams rlp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			if (lastDSB == null) {
				rlp.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
			} else {
				rlp.addRule(RelativeLayout.RIGHT_OF, lastDSB.getId());
			}
			rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP);
			rlp.rightMargin = 1;
			lastDSB  = db;
			lineStepButtons.addView(db, rlp);
		}
	}
	
	protected SynthStepButton getStepButton(int bi) {
		SynthStepButton b = null;
		for (SynthStepButton pv: ssb) {
			if (pv.getParent() == null) {
				b = pv;
				break;
			}
		}
		if (b == null) {
			b = new SynthStepButton(getContext());
			b.setLampResource(R.drawable.mw_synstep_lamp);
			ssb.add(b);
		}
		b.setStepId(bi);
		final SynthLineView v = this;
		b.setListener(new SynthStepButton.Listener() {

			@Override
			public void onStateChanged(PadStepButton b, int mode) {
				Log.d("synthlineview", String.format("drum step %d %d", b.id, mode));
				if (listener != null) {
					listener.onPadHit(v, currentLine, currentId, b.id, mode);
				}
			}
			
		});
		return b;
	}
	
	private Listener listener = null;
	
	public void setListener(Listener l) {
		listener = l;
	}

	@Override
	public Line getLine() {
		return currentLine;
	}

	public int getSynthId() {
		return currentId;
	}


}
